#!/usr/bin/env bash

#REM set the class path,
#REM assumes the build was executed with maven copy-dependencies
SET EFACTORY_CP=lapr4.app.spm.console\target\app.spm.console-2.0.0.jar;lapr4.app.spm.console\target\dependency\*;

#REM call the java VM, e.g, 
java -cp %EFACTORY_CP% lapr4.app.spm.console.EFactorySPM


