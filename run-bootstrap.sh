#!/usr/bin/env bash

#REM set the class path,
#REM assumes the build was executed with maven copy-dependencies
export EFACTORY_CP=lapr4.app.bootstrap/target/app.bootstrap-2.0.0.jar:lapr4.app.bootstrap/target/dependency/*;

#REM call the java VM, e.g,
java -cp $EFACTORY_CP lapr4.app.bootstrap.EFactoryBootstrap -bootstrap:demo 
