
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.domain;

import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author migue
 */
public class DepositTest {
    
    Map<Product,Integer> prods = new HashMap();
    String desc = "d1";
    DepositType type = DepositType.ENTRY;   
    
     @Test
    public void ensureDepositWithCompleteConstructor() {
        new Deposit("sss", desc,type,prods);
        assertTrue(true);
    }
    
    
      public void ensureDepositWithDescriptionAndType() {
        new Deposit("sss", desc,type);
        assertTrue(true);
    }
    
        public void ensureDepositWithType() {
        new Deposit("sss", null,type);
        assertTrue(true);
    }
        
            @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveProducts() {
        new Deposit("sss", desc,type,null);
    }
 
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeProductToNull() {
        final Deposit m = new Deposit("sss", desc,type,prods);
        m.setProducts(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeIDToNull() {
        final Deposit m = new Deposit("sss", desc,type,prods);
        m.setId(null);
    }

    
}
