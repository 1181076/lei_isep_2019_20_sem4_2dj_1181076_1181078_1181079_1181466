/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.domain;
    
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Leticia
 */
public class RawMateriaTest {

    private static final RawMateriaDescription LINHA = new RawMateriaDescription("aço");
    private static final RawMateriaCategory CATEGORY_FERRO = new RawMateriaCategory("ferro");
    private static final InternalRawMateriaCode INTERNALCODE = new InternalRawMateriaCode("rm1");
    private static final TechnicalSheet SHEET = new TechnicalSheet("sheetrm1");

    public RawMateriaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

   

    @Test(expected = IllegalArgumentException.class)
    public void addRawMateriaWithoutCode() {
        new RawMateria(LINHA, CATEGORY_FERRO, null, SHEET);

    }

    @Test
    public void addRawMateriaWithCompleteConstructor() {
        new RawMateria(LINHA, CATEGORY_FERRO, INTERNALCODE, SHEET);
        assertTrue(true);
    }

     @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeCodeToNull() {
        final RawMateria m = new RawMateria(LINHA, CATEGORY_FERRO, INTERNALCODE, SHEET);
        m.setCode(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeIDToNull() {
        final RawMateria m = new RawMateria(LINHA, CATEGORY_FERRO, INTERNALCODE, SHEET);
        m.setId(null);
    }

}


