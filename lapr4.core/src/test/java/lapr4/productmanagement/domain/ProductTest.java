/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.domain;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author migue
 */
public class ProductTest {
    
    @Test
    public void ensureProductWithRawMateriaManCodeAndComCode() {
        new Product(new ProductManufactureCode(""), new ProductCommercialCode(""), new ProductDescriptionBrief(""), new ProductDescriptionComplete(""), new ProductionSheet(), new RawMateria());
        assertTrue(true);
    }

//    @Test(expected = IllegalArgumentException.class)
//    public void ensureMustHaveRawMateria() {
//        new Product(new ProductManufactureCode(""), new ProductCommercialCode(""), new ProductDescriptionBrief(""), new ProductDescriptionComplete(""), null, null);
//        
//    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveRawManCode() {
        new Product(null, new ProductCommercialCode(""), new ProductDescriptionBrief(""), new ProductDescriptionComplete(""), new ProductionSheet(), new RawMateria());
       }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveRawMateria() {
        new Product(new ProductManufactureCode(""), null, new ProductDescriptionBrief(""), new ProductDescriptionComplete(""), new ProductionSheet(), new RawMateria());
    }

//    @Test(expected = IllegalArgumentException.class)
//    public void ensureCannotChangeRawMateriaToNull() {
//        Product p = new Product(new ProductManufactureCode(""), new ProductCommercialCode(""), new ProductDescriptionBrief(""), new ProductDescriptionComplete(""), null, new RawMateria());
//        p.setRawMaterial(null);
//    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeManCodeToNull() {
        Product p = new Product(new ProductManufactureCode(""), new ProductCommercialCode(""), new ProductDescriptionBrief(""), new ProductDescriptionComplete(""), new ProductionSheet(), new RawMateria());
        p.setProductManufactureCode(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeComCodeToNull() {
        Product p = new Product(new ProductManufactureCode(""), new ProductCommercialCode(""), new ProductDescriptionBrief(""), new ProductDescriptionComplete(""), new ProductionSheet(), new RawMateria());
        p.setProductCommercialCode(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeIDToNull() {
        Product p = new Product(new ProductManufactureCode(""), new ProductCommercialCode(""), new ProductDescriptionBrief(""), new ProductDescriptionComplete(""), new ProductionSheet(), new RawMateria());
        p.setId(null);
    }
}
