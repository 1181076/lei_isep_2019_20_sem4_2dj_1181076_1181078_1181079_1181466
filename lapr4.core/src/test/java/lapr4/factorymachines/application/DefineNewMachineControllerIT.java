/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.factorymachines.application;

import java.util.Date;
import java.util.HashSet;
import javax.persistence.PersistenceContext;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.MachineBrand;
import lapr4.factorymachines.domain.MachineDescript;
import lapr4.factorymachines.domain.MachineModel;
import lapr4.factorymachines.domain.Manufacturer;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.domain.Protocol;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author migue
 */
public class DefineNewMachineControllerIT {

    private final static ProductionLineRepository plRepository = lapr4.infrastructure.persistence.PersistenceContext.repositories().productionLines();
    private final MachineRepository machineRepository = lapr4.infrastructure.persistence.PersistenceContext.repositories().machines();
    private static ProductionLine pl;

    public DefineNewMachineControllerIT() {
    }

    @BeforeClass
    public static void setUpClass() {
        pl = new ProductionLine(12L, new HashSet(), new HashSet());
        plRepository.save(pl);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addMachine method, of class DefineNewMachineController.
     */
    @org.junit.Test
    public void testAddMachine() {
        System.out.println("addMachine");
        String numSerie = "bb6";
        Protocol prot = new Protocol("a");
        MachineDescript desc = new MachineDescript("b");
        Date installationDate = new Date();
        MachineBrand brand = new MachineBrand("c");
        MachineModel model = new MachineModel("d");
        Manufacturer man = new Manufacturer("e");
        DefineNewMachineController instance = new DefineNewMachineController();
        Machine expResult = new Machine(numSerie, prot, desc, installationDate, brand, model, man);
        instance.addMachine(numSerie, prot, desc, installationDate, brand, model, man, pl);
        Machine result = machineRepository.findByNumSerie(numSerie).get();
        assertEquals(expResult, result);
    }

}
