package lapr4.factoryusermanagement.domain;

import lapr4.factoryusermanagement.domain.FactoryUserBuilder;
import lapr4.factoryusermanagement.domain.FactoryUser;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import lapr4.usermanagement.domain.FactoryRoles;
import eapli.framework.infrastructure.authz.domain.model.NilPasswordPolicy;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.infrastructure.authz.domain.model.Role;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.infrastructure.authz.domain.model.SystemUserBuilder;

public class FactoryUserTest {

    private final String aMecanographicNumber = "abc";
    private final String anotherMecanographicNumber = "xyz";

    public static SystemUser dummyUser(final String username, final Role... roles) {
        // should we load from spring context?
        final SystemUserBuilder userBuilder = new SystemUserBuilder(new NilPasswordPolicy(),
                new PlainTextEncoder());
        return userBuilder.with(username, "duMMy1", "dummy", "dummy", "a@b.ro").withRoles(roles)
                .build();
    }

    private SystemUser getNewDummyUser() {
        return dummyUser("dummy", FactoryRoles.ADMIN);
    }

    @Test
    public void ensureFactoryUserEqualsPassesForTheSameMecanographicNumber() throws Exception {

        final FactoryUser aFactoryUser = new FactoryUserBuilder()
                .withMecanographicNumber("DUMMY").withSystemUser(getNewDummyUser()).build();

        final FactoryUser anotherFactoryUser = new FactoryUserBuilder()
                .withMecanographicNumber("DUMMY").withSystemUser(getNewDummyUser()).build();

        final boolean expected = aFactoryUser.equals(anotherFactoryUser);

        assertTrue(expected);
    }

    @Test
    public void ensureFactoryUserEqualsFailsForDifferenteMecanographicNumber() throws Exception {
        final Set<Role> roles = new HashSet<>();
        roles.add(FactoryRoles.ADMIN);

        final FactoryUser aFactoryUser = new FactoryUserBuilder()
                .withMecanographicNumber(aMecanographicNumber).withSystemUser(getNewDummyUser())
                .build();

        final FactoryUser anotherFactoryUser = new FactoryUserBuilder()
                .withMecanographicNumber(anotherMecanographicNumber)
                .withSystemUser(getNewDummyUser()).build();

        final boolean expected = aFactoryUser.equals(anotherFactoryUser);

        assertFalse(expected);
    }

    @Test
    public void ensureFactoryUserEqualsAreTheSameForTheSameInstance() throws Exception {
        final FactoryUser aFactoryUser = new FactoryUser(1);

        final boolean expected = aFactoryUser.equals(aFactoryUser);

        assertTrue(expected);
    }

    @Test
    public void ensureFactoryUserEqualsFailsForDifferenteObjectTypes() throws Exception {
        final Set<Role> roles = new HashSet<>();
        roles.add(FactoryRoles.ADMIN);

        final FactoryUser aFactoryUser = new FactoryUserBuilder()
                .withMecanographicNumber("DUMMY").withSystemUser(getNewDummyUser()).build();

        final boolean expected = aFactoryUser.equals(getNewDummyUser());

        assertFalse(expected);
    }

    @Test
    public void ensureFactoryUserIsTheSameAsItsInstance() throws Exception {
        final FactoryUser aFactoryUser = new FactoryUserBuilder()
                .withMecanographicNumber("DUMMY").withSystemUser(getNewDummyUser()).build();

        final boolean expected = aFactoryUser.sameAs(aFactoryUser);

        assertTrue(expected);
    }

    @Test
    public void ensureTwoFactoryUserWithDifferentMecanographicNumbersAreNotTheSame()
            throws Exception {
        final Set<Role> roles = new HashSet<>();
        roles.add(FactoryRoles.ADMIN);
        final FactoryUser aFactoryUser = new FactoryUserBuilder()
                .withMecanographicNumber(aMecanographicNumber).withSystemUser(getNewDummyUser())
                .build();

        final FactoryUser anotherFactoryUser = new FactoryUserBuilder()
                .withMecanographicNumber(anotherMecanographicNumber)
                .withSystemUser(getNewDummyUser()).build();

        final boolean expected = aFactoryUser.sameAs(anotherFactoryUser);

        assertFalse(expected);
    }
}
