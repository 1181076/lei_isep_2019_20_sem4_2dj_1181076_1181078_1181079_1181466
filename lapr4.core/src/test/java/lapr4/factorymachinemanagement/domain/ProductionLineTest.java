/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.factorymachinemanagement.domain;

import java.util.HashSet;
import java.util.Set;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.messagemanagement.domain.Message;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author migue
 */
public class ProductionLineTest {

    Long code = 253L;
    Set<Machine> ma = new HashSet<>();
    Set<Message> me = new HashSet<>();
    
     @Test
    public void ensureProductionLineWithCompleteConstructor() {
        new ProductionLine(code, ma, me);
        assertTrue(true);
    }


    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveMessagens() {
       new ProductionLine(code, ma, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveMachinesIds() {
       new ProductionLine(code, null, me);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveCode() {
        new ProductionLine(null, ma, me);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeCodeToNull() {
        final ProductionLine pl = new ProductionLine(code, ma, me);
        pl.setCode(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeMachinesIdsToNull() {
        final ProductionLine pl = new ProductionLine(code, ma, me);
        pl.setMachines(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeMessagesToNull() {
      final ProductionLine pl = new ProductionLine(code, ma, me);
        pl.setMessages(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeIDToNull() {
        final ProductionLine pl = new ProductionLine(code, ma, me);
        pl.setId(null);
    }
}
