/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.factorymachinemanagement.domain;

import java.util.Date;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.MachineBrand;
import lapr4.factorymachines.domain.MachineDescript;
import lapr4.factorymachines.domain.MachineModel;
import lapr4.factorymachines.domain.Manufacturer;
import lapr4.factorymachines.domain.Protocol;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author migue
 */
public class MachineTest {

    String numSerie1 = "254";
    String numSerie2 = "0";
    String numSerie3 = "-5";
    Protocol prot = new Protocol("p1");
    MachineDescript desc = new MachineDescript("m1");
    Date installationDate = new Date(2020, 05, 10);
    MachineBrand brand = new MachineBrand("brand1");
    MachineModel model = new MachineModel("model1");
    Manufacturer man = new Manufacturer("ma1");

    @Test
    public void ensureMachineWithCompleteConstructor() {
        new Machine(numSerie1, prot, desc, installationDate, brand, model, man);
        assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustNumSerieNotNull() {
        new Machine(null, prot, desc, installationDate, brand, model, man);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveInstallationDate() {
        new Machine(numSerie1, prot, desc, null, brand, model, man);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveManufacturer() {
        new Machine(numSerie1, prot, desc, installationDate, brand, model, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveModel() {
        new Machine(numSerie1, prot, desc, installationDate, brand, null, man);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveBrand() {
        new Machine(numSerie1, prot, desc, installationDate, null, model, man);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeBrandToNull() {
        final Machine m = new Machine(numSerie1, prot, desc, installationDate, brand, model, man);
        m.setBrand(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeModelToNull() {
        final Machine m = new Machine(numSerie1, prot, desc, installationDate, brand, model, man);
        m.setModel(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeManToNull() {
        final Machine m = new Machine(numSerie1, prot, desc, installationDate, brand, model, man);
        m.setMan(null);
    }
    
        @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeInstallationDateToNull() {
        final Machine m = new Machine(numSerie1, prot, desc, installationDate, brand, model, man);
        m.setInstallationDate(null);
    }
    
         @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeNumSerieToNull() {
        final Machine m = new Machine(numSerie1, prot, desc, installationDate, brand, model, man);
        m.setNumSerie(null);
    }
    
        @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeMan() {
        final Machine m = new Machine(numSerie1, prot, desc, installationDate, brand, model, man);
        m.setMan(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeIDToNull() {
        final Machine m = new Machine();
        m.setId(null);
    }

}
