package lapr4.usermanagement.domain;

import eapli.framework.infrastructure.authz.domain.model.Role;

/**
 * @author Paulo Gandra Sousa
 *
 */
public final class FactoryRoles {
    /**
     * poweruser
     */
    public static final Role POWER_USER = Role.valueOf("POWER_USER");
    /**
     * Utente
     */
    public static final Role CAFETERIA_USER = Role.valueOf("CAFETERIA_USER");
    /**
     * Cafeteria Administrator
     */
    public static final Role ADMIN = Role.valueOf("ADMIN");
    /**
     *
     */
    public static final Role GESTOR_CHAO = Role.valueOf("GESTOR_CHAO");
    /**
     *
     */
    public static final Role GESTOR_PRODUCAO = Role.valueOf("GESTOR_PRODUCAO");


    /**
     * get available role types for adding new users
     *
     * @return
     */
    public static Role[] nonUserValues() {
        return new Role[] { ADMIN, GESTOR_CHAO, GESTOR_PRODUCAO};
    }

    public boolean isCollaborator(final Role role) {
        return role != CAFETERIA_USER;
    }
}
