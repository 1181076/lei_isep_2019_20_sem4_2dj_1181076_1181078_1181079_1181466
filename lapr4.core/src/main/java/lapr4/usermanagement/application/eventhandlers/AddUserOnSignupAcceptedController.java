package lapr4.usermanagement.application.eventhandlers;

import lapr4.factoryusermanagement.domain.events.NewUserRegisteredFromSignupEvent;
import lapr4.factoryusermanagement.domain.events.SignupAcceptedEvent;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.usermanagement.domain.FactoryRoles;
import lapr4.usermanagement.domain.UserBuilderHelper;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.events.DomainEvent;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.infrastructure.authz.domain.model.SystemUserBuilder;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.infrastructure.eventpubsub.EventPublisher;
import eapli.framework.infrastructure.eventpubsub.impl.inprocess.InProcessPubSub;

@UseCaseController
/* package */ class AddUserOnSignupAcceptedController {
    private final UserRepository userRepository = PersistenceContext.repositories().users();
    private final EventPublisher dispatcher = InProcessPubSub.publisher();

    /**
     * @param theSignupRequest
     * @return
     * @throws ConcurrencyException
     * @throws IntegrityViolationException
     */
    public SystemUser addUser(final SignupAcceptedEvent theSignupRequest) {

        final SystemUserBuilder userBuilder = UserBuilderHelper.builder();
        userBuilder.withUsername(theSignupRequest.username())
                .withPassword(theSignupRequest.password())
                .withName(theSignupRequest.name())
                .withEmail(theSignupRequest.email())
                .withRoles(FactoryRoles.CAFETERIA_USER);
        final SystemUser newUser = userRepository.save(userBuilder.build());

        // notify interested parties
        final DomainEvent event = new NewUserRegisteredFromSignupEvent(
                theSignupRequest.mecanographicNumber(), newUser.username());
        dispatcher.publish(event);

        return newUser;
    }
}
