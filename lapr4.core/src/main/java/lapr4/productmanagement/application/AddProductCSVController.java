/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.InternalRawMateriaCode;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductCommercialCode;
import lapr4.productmanagement.domain.ProductDescriptionBrief;
import lapr4.productmanagement.domain.ProductDescriptionComplete;
import lapr4.productmanagement.domain.ProductManufactureCode;
import lapr4.productmanagement.domain.RawMateria;
import lapr4.productmanagement.domain.RawMateriaCategory;
import lapr4.productmanagement.domain.RawMateriaDescription;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaCategoryRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author mxlsa
 */
public class AddProductCSVController {

    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ";";
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductRepository pdRepo = PersistenceContext.repositories().products();

    private final RawMateriaRepository rMRepo = PersistenceContext.repositories().rawMaterias();
    private final RawMateriaCategoryRepository cRepo = PersistenceContext.repositories().rawMateriaCategories();

    public void read(String fileName) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO);
        this.readHeader(fileName);
        try {

            br = new BufferedReader(new FileReader(fileName));
            br.readLine(); //Ignore Header
            while ((line = br.readLine()) != null) {

                String[] pd = line.split(cvsSplitBy);
                this.addProduct(pd[0], pd[1], pd[2], pd[3], pd[5]);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void readHeader(String fileName) {
        try {

            br = new BufferedReader(new FileReader(fileName));

            // use comma as separator
            String[] header = line.split(cvsSplitBy);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Product addProduct(String pManufacturerCode, String pComercialCode, String pDescriptionBrief, String pDescriptionComplete, String Category) {
        DefineRawMateriaCategoryController cont = new DefineRawMateriaCategoryController();
        RawMateriaCategory cat = cont.defineRawMateriaCategory(Category);

        final RawMateria rM = new RawMateria(new RawMateriaDescription(pDescriptionComplete), cat, new InternalRawMateriaCode(pManufacturerCode), null);

        rMRepo.save(rM);

        Product newProduct = new Product(new ProductManufactureCode(pManufacturerCode), new ProductCommercialCode(pComercialCode), new ProductDescriptionBrief(pDescriptionBrief), new ProductDescriptionComplete(pDescriptionComplete), null, rM);
        
        newProduct.setId(Long.parseLong(pManufacturerCode));
        return this.pdRepo.save(newProduct);
    }

}
