/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;


import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.InternalRawMateriaCode;
import lapr4.productmanagement.domain.RawMateria;
import lapr4.productmanagement.domain.RawMateriaCategory;
import lapr4.productmanagement.domain.RawMateriaDescription;
import lapr4.productmanagement.domain.TechnicalSheet;
import lapr4.productmanagement.domain.repositories.RawMateriaRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author Leticia
 */
public class AddRawMateriaController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ListRawMateriaCategoryService svc = new ListRawMateriaCategoryService();
    private final RawMateriaRepository rawMateriaRepository = PersistenceContext.repositories().rawMaterias();

    public RawMateria addRawMateria(final RawMateriaDescription desc, final RawMateriaCategory cat, final InternalRawMateriaCode code, final TechnicalSheet technicalSheet) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER, FactoryRoles.ADMIN);

        final RawMateria newRawMateria = new RawMateria(desc, cat, code, technicalSheet);

        return rawMateriaRepository.save(newRawMateria);
    }
    
    

    public Iterable<RawMateriaCategory> RawMateriaCategories() {
        return this.svc.allRawMateriaCategories();
    }
}
