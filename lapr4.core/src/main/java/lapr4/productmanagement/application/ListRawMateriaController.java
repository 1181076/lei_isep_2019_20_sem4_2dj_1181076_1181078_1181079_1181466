/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import lapr4.productmanagement.domain.RawMateria;

/**
 *
 * @author Leticia
 */
public class ListRawMateriaController {

    private final ListRawMateriaService svc = new ListRawMateriaService();

    public Iterable<RawMateria> listRawMaterias() {
        return this.svc.allRawMaterias();
    }
}
