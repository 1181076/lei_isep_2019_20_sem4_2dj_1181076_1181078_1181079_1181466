/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductCommercialCode;
import lapr4.productmanagement.domain.ProductDescriptionBrief;
import lapr4.productmanagement.domain.ProductDescriptionComplete;
import lapr4.productmanagement.domain.ProductManufactureCode;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.RawMateria;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author mxlsa
 */
public class AddProductController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionSheetRepository psRepo = PersistenceContext.repositories().productionSheets();
    private final RawMateriaRepository rawRepo = PersistenceContext.repositories().rawMaterias();
    private final ProductRepository pdRepo = PersistenceContext.repositories().products();
    
    public Iterable<ProductionSheet> productionSheets() {
        return psRepo.findAll();
    }

    public Iterable<RawMateria> rawMaterials() {
        return rawRepo.findAll();
    }

    public Iterable<RawMateria> rawMaterialsWithoutProducts() {
        Iterator<RawMateria> it = rawRepo.findAll().iterator();
        Iterator<Product> pR = pdRepo.findAll().iterator();
        List<RawMateria> ls = new ArrayList();
        List<RawMateria> lst = new ArrayList();

        while (pR.hasNext()) {
            ls.add(pR.next().getRawMaterial());
        }

        RawMateria rm;

        while (it.hasNext()) {
            rm = it.next();
            if (!ls.contains(rm)) {
                lst.add(rm);
            }
        }
        
        return lst;

    }

    public Product addProduct(ProductManufactureCode pManufacturerCode, ProductCommercialCode pComercialCode, ProductDescriptionBrief pDescriptionBrief, ProductDescriptionComplete pDescriptionComplete, ProductionSheet pdSheet, RawMateria rawMat) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO);

        final Product newProduct = new Product((pManufacturerCode), (pComercialCode), (pDescriptionBrief), (pDescriptionComplete), pdSheet, rawMat);
        return this.pdRepo.save(newProduct);
    }

    public Product addProduct(Product pd) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO);

        
        return this.pdRepo.save(pd);
    }

}
