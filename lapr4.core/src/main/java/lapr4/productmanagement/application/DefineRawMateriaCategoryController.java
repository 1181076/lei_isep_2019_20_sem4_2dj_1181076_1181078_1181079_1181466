/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.RawMateriaCategory;
import lapr4.productmanagement.domain.repositories.RawMateriaCategoryRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author Leticia
 */
public class DefineRawMateriaCategoryController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final RawMateriaCategoryRepository repository = PersistenceContext.repositories().rawMateriaCategories();

    public RawMateriaCategory defineRawMateriaCategory(final String cat) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO);

        final RawMateriaCategory newRawMateriaCategory = new RawMateriaCategory(cat);
        return this.repository.save(newRawMateriaCategory);
    }
}
