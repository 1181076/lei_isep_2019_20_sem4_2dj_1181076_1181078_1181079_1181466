/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author migue
 */
public class RemoveProductController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductRepository productRepository = PersistenceContext.repositories().products();

    public void removeProduct(final long id) {
        
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER, FactoryRoles.ADMIN, FactoryRoles.GESTOR_PRODUCAO);
        
        Product rmc = productRepository.findByID(id).get();
        
        productRepository.remove(rmc);
        
    }
}
