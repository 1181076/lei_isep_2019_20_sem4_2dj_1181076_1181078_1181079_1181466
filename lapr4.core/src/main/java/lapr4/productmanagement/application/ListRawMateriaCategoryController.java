/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import lapr4.productmanagement.domain.RawMateriaCategory;

/**
 *
 * @author Leticia
 */
public class ListRawMateriaCategoryController {
      private final ListRawMateriaCategoryService svc = new ListRawMateriaCategoryService();

    public Iterable<RawMateriaCategory> listRawMateriaCategories() {
        return this.svc.allRawMateriaCategories();
    }  
}
