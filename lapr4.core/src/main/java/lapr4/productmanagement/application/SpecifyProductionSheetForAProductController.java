/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Optional;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductManufactureCode;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author vrmpe
 */
public class SpecifyProductionSheetForAProductController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionSheetRepository productRepository = PersistenceContext.repositories().productionSheets();

    public ProductionSheet specifyProductionSheetForAProduct(ProductionSheet sheet) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO);

        return this.productRepository.save(sheet);
    }

}
