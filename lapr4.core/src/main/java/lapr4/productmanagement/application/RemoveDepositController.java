/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Optional;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.Deposit;
import lapr4.productmanagement.domain.repositories.DepositRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author Leticia
 */
public class RemoveDepositController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final DepositRepository depositRepository = PersistenceContext.repositories().deposits();

    public void removeDeposit(final String id) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER, FactoryRoles.GESTOR_CHAO);
        Optional<Deposit> dp = depositRepository.findByID(id);
        depositRepository.remove(dp.get());
    }
}
