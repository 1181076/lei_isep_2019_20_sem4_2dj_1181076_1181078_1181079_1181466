/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.RawMateriaCategory;
import lapr4.productmanagement.domain.repositories.RawMateriaCategoryRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author Leticia
 */
public class ListRawMateriaCategoryService {
        private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final RawMateriaCategoryRepository rawMateriaCategoryRepository = PersistenceContext.repositories().rawMateriaCategories();

    public Iterable<RawMateriaCategory> allRawMateriaCategories() {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO);

        return this.rawMateriaCategoryRepository.findAll();
    }

    public Iterable<RawMateriaCategory> activeRawMateriaCategories() {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO);

        return this.rawMateriaCategoryRepository.findAllActive();
    }
}
