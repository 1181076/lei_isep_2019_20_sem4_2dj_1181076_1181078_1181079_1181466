/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author vrmpe
 */
public class ConsultProductsWithoutProductionSheetController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductRepository productRepository = PersistenceContext.repositories().products();

    public Iterable<Product> productsWithoutProductionSheet() {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_PRODUCAO, FactoryRoles.POWER_USER);

        Iterable<Product> all = this.productRepository.findAllWithoutPS();

        return all;
    }
}
