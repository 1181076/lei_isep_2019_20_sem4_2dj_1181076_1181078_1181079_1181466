/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author migue
 */
public class ListProductionSheetsController  {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionSheetRepository productionSheetsRepository = PersistenceContext.repositories().productionSheets();

    public Iterable<ProductionSheet> allSheets() {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO, FactoryRoles.GESTOR_CHAO);

        return this.productionSheetsRepository.findAll();
    }
}
