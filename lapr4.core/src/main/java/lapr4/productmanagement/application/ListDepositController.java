/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import lapr4.productmanagement.domain.Deposit;

/**
 *
 * @author Leticia
 */
public class ListDepositController {
    private final ListDepositService svc = new ListDepositService();

    public Iterable<Deposit> listDeposit() {
        return this.svc.allDeposits();
    }
}
