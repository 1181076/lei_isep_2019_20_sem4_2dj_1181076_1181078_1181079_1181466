/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.RawMateria;
import lapr4.productmanagement.domain.repositories.RawMateriaRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author Leticia
 */
public class ListRawMateriaService {
        private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final RawMateriaRepository rawMateriaRepository = PersistenceContext.repositories().rawMaterias();

    public Iterable<RawMateria> allRawMaterias() {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO);

        return this.rawMateriaRepository.findAll();
    }
}
