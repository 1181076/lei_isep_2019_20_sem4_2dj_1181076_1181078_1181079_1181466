/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Map;
import java.util.Set;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.Deposit;
import lapr4.productmanagement.domain.DepositType;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.repositories.DepositRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author Leticia
 */
public class AddDepositController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final DepositRepository depositRepository = PersistenceContext.repositories().deposits();

    public Deposit addDeposit(final String id, final String desc, final DepositType depositType, Map<Product,Integer> prods) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_CHAO);

        final Deposit newDeposit = new Deposit(id, desc, depositType, prods);

        return this.depositRepository.save(newDeposit);
    }

    public DepositType DepositTypes() {

        for (DepositType dp : DepositType.values()) {
            System.out.println(dp);
        }
        return null;
    }

}
