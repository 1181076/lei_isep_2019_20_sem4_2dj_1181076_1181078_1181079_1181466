package lapr4.productmanagement.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Embeddable
public class ProductDescriptionBrief implements ValueObject{
    private static final long serialVersionUID = 1L;
    
    private String descBrief;

    public ProductDescriptionBrief(String desc) {
        Preconditions.nonNull(desc);
        this.descBrief = desc;
    }

    public String getDesc() {
        return descBrief;
    }

    public ProductDescriptionBrief() {
    }

    public void setDesc(String desc) {
        this.descBrief = desc;
    }

    @Override
    public int hashCode() {
        return descBrief.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductDescriptionBrief)) {
            return false;
        }
        ProductDescriptionBrief other = (ProductDescriptionBrief) object;
        if (this.descBrief.equals(other.descBrief)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "description: "+ descBrief + "\n" ;
    }
    
    public String toXml() {
        return "<description>" + descBrief + "</description>";
    }
}
