package lapr4.productmanagement.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Embeddable
public class RawMateriaDescription implements ValueObject{

    public RawMateriaDescription() {
    }
    
    private static final long serialVersionUID = 1L;
    
    private String desc;

    public RawMateriaDescription(String desc) {
        Preconditions.nonNull(desc);
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    @Override
    public int hashCode() {
        return desc.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RawMateriaDescription)) {
            return false;
        }
        RawMateriaDescription other = (RawMateriaDescription) object;
        if (this.desc.equals(other.desc)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "description: "+ desc + "\n" ;
    }
    
    public String toXml() {
        return "<rawMateriaDescription>" + desc + "</rawMateriaDescription>";
    }
}

