/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author migue
 */
@Entity
public class Deposit implements Serializable, AggregateRoot<String> {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    private String desc;

    @Enumerated(EnumType.STRING)
    private DepositType type;

    @ElementCollection
    private Map<Product, Integer> products = new HashMap();

    public Deposit(String id, String desc, DepositType type, Map<Product, Integer> prods) {
        Preconditions.nonNull(id);
        Preconditions.nonNull(prods);
        Preconditions.nonNull(desc);
        Preconditions.nonNull(type);
        this.id = id;
        this.desc = desc;
        this.type = type;
        this.products = prods;
    }

    public Deposit(String id, String desc, DepositType type) {
        Preconditions.nonNull(id);
        Preconditions.nonNull(desc);
        Preconditions.nonNull(type);
        this.id = id;
        this.desc = desc;
        this.type = type;
    }

    public Deposit(DepositType type) {
        Preconditions.nonNull(products);
        this.desc = null;
        this.type = type;
    }

    public Deposit() {
    }

    public Map<Product, Integer> getProducts() {
        return products;
    }

    public void setProducts(Map<Product, Integer> products) {
        Preconditions.nonNull(products);
        this.products = products;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public DepositType getType() {
        return type;
    }

    public void setType(DepositType type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        Preconditions.nonNull(id);
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Deposit)) {
            return false;
        }
        Deposit other = (Deposit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lapr4.productmanagement.domain.Deposit[ id=" + id + " ]";
    }

    @Override
    public boolean sameAs(Object object) {
        Deposit other = (Deposit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String identity() {
        return id;
    }

    public String toXml() {
        String a = "<deposit idDeposit= \"" + id + "\">";

        if (type != null) {
            a = a + "<type>" + type.toString() + "</type>";
        }

        if (desc != null) {
            a = a + "<desc>" + desc + "</desc>";
        }

        Iterator<Product> itt = products.keySet().iterator();
        Product pro;

        if (!products.keySet().isEmpty()) {

            a = a + "<products>";

            for (int i = 0; i < products.size(); i++) {
                a = a + "<product_amount>";
                pro = itt.next();
                a = a + pro.toXml() + "<amount>" + products.get(pro) + "</amount>"+ "</product_amount>";;
            }

            a = a + "</products>";
        }
        a = a + "</deposit>";

        return a;
    }
}
