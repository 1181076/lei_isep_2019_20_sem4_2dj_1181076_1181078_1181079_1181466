/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author migue
 */
@Entity
public class ProductionSheet implements Serializable, AggregateRoot<String> {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    @ElementCollection
    private Map<Product, Integer> materialAmount = new HashMap();

    public ProductionSheet(String id, Map<Product, Integer> amount) {
        Preconditions.nonNull(amount);
        Preconditions.nonNull(id);
        this.materialAmount = amount;
        this.id = id;
    }

    public ProductionSheet() {
    }

    public Map<Product, Integer> getAmount() {
        return materialAmount;
    }

    public void setAmount(Map<Product, Integer> amount) {
        this.materialAmount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductionSheet)) {
            return false;
        }
        ProductionSheet other = (ProductionSheet) object;
        if (!(this.materialAmount.equals(other.materialAmount))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return materialAmount.toString();
    }

    @Override
    public boolean sameAs(Object object) {
        ProductionSheet other = (ProductionSheet) object;
        if (!(this.materialAmount.equals(other.materialAmount))) {
            return false;
        }
        return true;
    }

    @Override
    public String identity() {
        return id;
    }

    public String toXml() {

        String a = "";

        Iterator<Product> it = materialAmount.keySet().iterator();
        Product ma;

        if (!materialAmount.isEmpty()) {
            a = "<productionSheet idProductionSheet= \"" + id + "\">";
            a = a + "<materials>";

            for (int i = 0; i < materialAmount.size(); i++) {
                a = a + "<material_amount>";
                ma = it.next();
                a = a + ma.toXml() + "<amount>" + materialAmount.get(ma) + "</amount>" + "</material_amount>";;
            }

            a = a + "</materials>";
            a = a + "</productionSheet>";

        }

        return a;
    }
}
