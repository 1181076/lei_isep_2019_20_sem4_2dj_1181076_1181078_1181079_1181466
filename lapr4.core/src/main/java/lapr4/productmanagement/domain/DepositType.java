package lapr4.productmanagement.domain;

import eapli.framework.domain.model.ValueObject;

public enum DepositType implements ValueObject{
    ENTRY, EXIT;
}