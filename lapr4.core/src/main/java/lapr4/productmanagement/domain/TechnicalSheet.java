package lapr4.productmanagement.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Embeddable
public class TechnicalSheet implements ValueObject {

    public TechnicalSheet() {
    }

    private static final long serialVersionUID = 1L;

    private String sheet;

    public TechnicalSheet(String desc) {
        Preconditions.nonNull(desc);
        this.sheet = desc;
    }

    @Override
    public int hashCode() {
        return this.sheet.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TechnicalSheet)) {
            return false;
        }
        TechnicalSheet other = (TechnicalSheet) object;
        if (this.sheet.equals(other.sheet)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "description: " + sheet + "\n";
    }

    public String toXml() {
        return "<sheet>" + sheet + "</sheet>";
    }
}
