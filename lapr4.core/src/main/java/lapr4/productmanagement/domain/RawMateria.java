/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author migue
 */
@Entity
public class RawMateria implements Serializable, AggregateRoot<Long> {

    public RawMateriaDescription getDesc() {
        return desc;
    }

    public void setDesc(RawMateriaDescription desc) {
        this.desc = desc;
    }

    public RawMateriaCategory getCat() {
        return cat;
    }

    public void setCat(RawMateriaCategory cat) {
        this.cat = cat;
    }

    public InternalRawMateriaCode getCode() {
        return code;
    }

    public void setCode(InternalRawMateriaCode code) {
        Preconditions.nonNull(code);
        this.code = code;
    }

    public TechnicalSheet getTechnicalSheet() {
        return technicalSheet;
    }

    public void setTechnicalSheet(TechnicalSheet technicalSheet) {
        this.technicalSheet = technicalSheet;
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private RawMateriaDescription desc;

    @ManyToOne(cascade = {CascadeType.ALL})
    private RawMateriaCategory cat;

    private InternalRawMateriaCode code;

    private TechnicalSheet technicalSheet;

    public RawMateria(RawMateriaDescription desc, RawMateriaCategory cat, InternalRawMateriaCode code, TechnicalSheet technicalSheet) {
        Preconditions.nonNull(code);
        this.desc = desc;
        this.cat = cat;
        this.code = code;
        this.technicalSheet = technicalSheet;
    }

    public RawMateria() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Preconditions.nonNull(id);
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RawMateria)) {
            return false;
        }
        RawMateria other = (RawMateria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lapr4.productmanagement.domain.PrimaryMateria[ id=" + id + " ]";
    }

    @Override
    public boolean sameAs(Object object) {

        RawMateria other = (RawMateria) object;

        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public Long identity() {
        return this.id;
    }

    public String toXml() {
        String a = "<rawMateria idRawMateria= \"" + id + "\">";

        if (desc != null) {
            a = a + "<description>" + desc.toXml() + "</description>";
        }

        if (cat != null) {
            a = a + "<category>" + cat.toXml() + "</category>";
        }

        a = a + "<internalCode>" + code.toXml() + "</internalCode>";

        if (technicalSheet != null) {
            a = a + "<technicalSheet>" + technicalSheet.toXml() + "</technicalSheet>";
        }

        a = a + "</rawMateria>";

        return a;
    }

}
