/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author migue
 */
@Entity
public class Product implements Serializable, AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private ProductManufactureCode productManufactureCode;

    private ProductCommercialCode productCommercialCode;
    private ProductDescriptionBrief productDescriptionBrief;
    private ProductDescriptionComplete productDescriptionComplete;

    @OneToOne(cascade = {CascadeType.ALL})
    private ProductionSheet productSheet;

    @OneToOne(cascade = {CascadeType.ALL})
    private RawMateria rawMaterial;

    public Product() {
    }

    public Product(ProductManufactureCode productManufactureCode, ProductCommercialCode productCommercialCode, ProductDescriptionBrief productDescriptionBrief, ProductDescriptionComplete productDescriptionComplete, ProductionSheet productSheet, RawMateria rawMaterial) {
        if (productDescriptionBrief.getDesc().length() > 30) {
            throw new IllegalArgumentException();
        }
        Preconditions.nonNull(productManufactureCode);
        Preconditions.nonNull(productCommercialCode);
        this.productManufactureCode = productManufactureCode;
        this.productCommercialCode = productCommercialCode;
        this.productDescriptionBrief = productDescriptionBrief;
        this.productDescriptionComplete = productDescriptionComplete;
        this.productSheet = productSheet;
        this.rawMaterial = rawMaterial;
    }

    public Product(ProductManufactureCode productManufactureCode, ProductCommercialCode productCommercialCode, ProductDescriptionBrief productDescriptionBrief, ProductDescriptionComplete productDescriptionComplete, RawMateria rawMaterial) {
        if (productDescriptionBrief.getDesc().length() > 30) {
            throw new IllegalArgumentException();
        }
        Preconditions.nonNull(productManufactureCode);
        Preconditions.nonNull(productCommercialCode);
        this.productManufactureCode = productManufactureCode;
        this.productCommercialCode = productCommercialCode;
        this.productDescriptionBrief = productDescriptionBrief;
        this.productDescriptionComplete = productDescriptionComplete;
        this.productSheet = null;
        this.rawMaterial = rawMaterial;
    }

    public Product(ProductManufactureCode productManufactureCode, ProductCommercialCode productCommercialCode, ProductionSheet productSheet, RawMateria rawMaterial) {

        Preconditions.nonNull(productManufactureCode);
        Preconditions.nonNull(productCommercialCode);
        this.productManufactureCode = productManufactureCode;
        this.productCommercialCode = productCommercialCode;
        this.productSheet = productSheet;
        this.rawMaterial = rawMaterial;
        this.productDescriptionBrief = new ProductDescriptionBrief();
        this.productDescriptionComplete = new ProductDescriptionComplete();
    }

    public ProductManufactureCode getProductManufactureCode() {
        return productManufactureCode;
    }

    public void setProductManufactureCode(ProductManufactureCode productManufactureCode) {

        Preconditions.nonNull(productManufactureCode);
        this.productManufactureCode = productManufactureCode;
    }

    public ProductCommercialCode getProductCommercialCode() {
        return productCommercialCode;
    }

    public void setProductCommercialCode(ProductCommercialCode productCommercialCode) {

        Preconditions.nonNull(productCommercialCode);
        this.productCommercialCode = productCommercialCode;
    }

    public ProductDescriptionBrief getProductDescriptionBrief() {
        return productDescriptionBrief;
    }

    public void setProductDescriptionBrief(ProductDescriptionBrief productDescriptionBrief) {
        this.productDescriptionBrief = productDescriptionBrief;
    }

    public ProductDescriptionComplete getProductDescriptionComplete() {
        return productDescriptionComplete;
    }

    public void setProductDescriptionComplete(ProductDescriptionComplete productDescriptionComplete) {
        this.productDescriptionComplete = productDescriptionComplete;
    }

    public ProductionSheet getProductSheet() {
        return productSheet;
    }

    public void setProductSheet(ProductionSheet productSheet) {
        this.productSheet = productSheet;
    }

    public RawMateria getRawMaterial() {
        return rawMaterial;
    }

    public void setRawMaterial(RawMateria rawMaterial) {
        this.rawMaterial = rawMaterial;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Preconditions.nonNull(id);
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if (this.productCommercialCode.equals(other.productCommercialCode) && this.productManufactureCode.equals(other.productManufactureCode)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lapr4.productmanagement.domain.Product[ id=" + id + " ]";
    }

    @Override
    public boolean sameAs(Object object) {
        Product other = (Product) object;
        if (this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public Long identity() {
        return id;
    }

    public String toXml() {

        String a = "<product idProduct= \"" + id + "\">" + "<manufactureCode>" + productManufactureCode.toXml() + "</manufactureCode>"
                + "<commercialCode>" + productCommercialCode.toXml() + "</commercialCode>";

        if (productDescriptionBrief != null) {
            a = a + "<briefDescription>" + productDescriptionBrief.toXml() + "</briefDescription>";
        }

        if (productDescriptionComplete != null) {
            a = a + "<completeDescription>" + productDescriptionComplete.toXml() + "</completeDescription>";
        }

        if (productSheet != null) {
            a = a + productSheet.toXml();
        }

        if (rawMaterial != null) {
            a = a + rawMaterial.toXml() + "</product>";
        }

        return a;

    }

}
