package lapr4.productmanagement.domain.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.RawMateria;
import lapr4.productmanagement.domain.RawMateriaCategory;

public interface RawMateriaRepository extends DomainRepository<Long, RawMateria > {

    default Optional<RawMateria> findByCode(
            final Long id) {
        return ofIdentity(id);
    }

    Iterable<RawMateria> findAllActive();
        
}