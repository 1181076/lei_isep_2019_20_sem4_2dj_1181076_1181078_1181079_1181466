package lapr4.productmanagement.domain.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.ProductLot;

public interface ProductLotRepository extends DomainRepository<String, ProductLot > {

    default Optional<ProductLot> findByCode(
            final String code) {
        return ofIdentity(code);
    }

    Iterable<ProductLot> findAllActive();
}