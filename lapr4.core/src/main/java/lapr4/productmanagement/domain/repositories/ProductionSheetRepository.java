package lapr4.productmanagement.domain.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.ProductionSheet;

public interface ProductionSheetRepository extends DomainRepository<String, ProductionSheet > {

    default Optional<ProductionSheet> findByCode(
            final String id) {
        return ofIdentity(id);
    }

    Iterable<ProductionSheet> findAllActive();
}