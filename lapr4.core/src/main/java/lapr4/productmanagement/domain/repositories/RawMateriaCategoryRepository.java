package lapr4.productmanagement.domain.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.RawMateriaCategory;


public interface RawMateriaCategoryRepository extends DomainRepository<Long, RawMateriaCategory > {

    default Optional<RawMateriaCategory> findByCode(
            final Long id) {
        return ofIdentity(id);
    }

    Iterable<RawMateriaCategory> findAllActive();
}