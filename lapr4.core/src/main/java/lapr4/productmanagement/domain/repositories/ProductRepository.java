package lapr4.productmanagement.domain.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductManufactureCode;

public interface ProductRepository extends DomainRepository<Long, Product > {

    default Optional<Product> findByID(
            final Long Id) {
        return ofIdentity(Id);
    }

    Iterable<Product> findAllActive();
    
    Iterable<Product> findAllWithoutPS();
    
    Optional<Product> findByCode(String code);
 
}
