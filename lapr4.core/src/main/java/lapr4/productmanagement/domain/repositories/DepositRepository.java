package lapr4.productmanagement.domain.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.Deposit;

public interface DepositRepository extends DomainRepository<String, Deposit > {

    default Optional<Deposit> findByID(
            final String Id) {
        return ofIdentity(Id);
    }

    Iterable<Deposit> findAllActive();
}