package lapr4.productmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RawMateriaCategory implements Serializable, AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    private String categ;

    public String getCateg() {
        return categ;
    }

    public void setCateg(String categ) {
        this.categ = categ;
    }

    public RawMateriaCategory() {
    }

    public RawMateriaCategory(String categ) {
        Preconditions.nonNull(categ);
        this.categ = categ;
    }

    @Override
    public int hashCode() {
        return categ.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RawMateriaCategory)) {
            return false;
        }
        RawMateriaCategory other = (RawMateriaCategory) object;
        if (!this.categ.equals(other.categ)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Category:" + categ + "\n";
    }

    @Override
    public boolean sameAs(Object object) {
        RawMateriaCategory other = (RawMateriaCategory) object;
        if (!this.categ.equals(other.categ)) {
            return false;
        }
        return true;
    }

    @Override
    public Long identity() {
        return id;
    }
    
    public String toXml() {
        return "<rawMateriaCategory idRawMateriaCategory= \"" + id + "\">" + "<categoryName>" + categ + "</categoryName>" + "</rawMateriaCategory>" ;
    }

}
