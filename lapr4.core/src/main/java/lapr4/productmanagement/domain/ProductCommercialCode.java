/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productmanagement.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Embeddable
public class ProductCommercialCode implements ValueObject{
    private static final long serialVersionUID = 1L;

    private String comCode;

    public ProductCommercialCode() {
    }

    public ProductCommercialCode(String code) {
        Preconditions.nonNull(code);
        this.comCode = code;
    }

    @Override
    public int hashCode() {
        return comCode.hashCode();
    }

    public String getCode() {
        return comCode;
    }

    public void setCode(String code) {
        this.comCode = code;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductCommercialCode)) {
            return false;
        }
        ProductCommercialCode other = (ProductCommercialCode) object;
        if (this.comCode.equals(other.comCode)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "code: "+ comCode + "\n" ;
    }
    
    public String toXml() {
        return "<code>" + comCode + "</code>";
    }
}
