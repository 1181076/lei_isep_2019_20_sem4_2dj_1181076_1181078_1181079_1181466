package lapr4.productmanagement.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Embeddable
public class InternalRawMateriaCode implements ValueObject{

    public InternalRawMateriaCode() {
    }
    
    private static final long serialVersionUID = 1L;

    private String code;

    public InternalRawMateriaCode(String code) {
        Preconditions.nonNull(code);
        this.code = code;
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InternalRawMateriaCode)) {
            return false;
        }
        InternalRawMateriaCode other = (InternalRawMateriaCode) object;
        if (this.code.equals(other.code)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "code: "+ code + "\n" ;
    }
    
    public String toXml() {
        return "<code>" + code + "</code>";
    }
}