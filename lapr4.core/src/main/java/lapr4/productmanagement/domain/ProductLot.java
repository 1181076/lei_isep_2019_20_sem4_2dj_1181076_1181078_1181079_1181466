package lapr4.productmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ProductLot implements AggregateRoot<String>, Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;
    
    @ElementCollection
    private Map<Product, Integer> amount = new HashMap();

    public ProductLot(String id, Map<Product, Integer> amount) {
        this.id = id;
        this.amount = amount;
    }

    public ProductLot() {
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<Product, Integer> getAmount() {
        return amount;
    }

    public void setAmount(Map<Product, Integer> amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductLot)) {
            return false;
        }
        ProductLot other = (ProductLot) object;
        if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public boolean sameAs(Object object) {
        
        ProductLot other = (ProductLot) object;
        
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String identity() {
        return this.id;
    }

    public String toXml() {
        String a = "<productlot idlot= \"" + id + "\">";
        
        Iterator<Product> itt = amount.keySet().iterator();
        Product pro;

        if (!amount.keySet().isEmpty()) {

            a = a + "<products>";

            for (int i = 0; i < amount.size(); i++) {
                a = a + "<product_amount>";
                pro = itt.next();
                a = a + pro.toXml() + "<amount>" + amount.get(pro) + "</amount>" + "</product_amount>";
            }

            a = a + "</products>";
        }
        a = a + "</productlot>";

        return a;
    }
    
}
