package lapr4.productmanagement.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;

@Embeddable
public class ProductManufactureCode implements ValueObject, Comparable<ProductManufactureCode>{
    private static final long serialVersionUID = 1L;

    private String code;

    public ProductManufactureCode(String code) {
        Preconditions.nonNull(code);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ProductManufactureCode() {
    }
 

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductManufactureCode)) {
            return false;
        }
        ProductManufactureCode other = (ProductManufactureCode) object;
        if (this.code.equals(other.code)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "code: "+ code + "\n" ;
    }

    @Override
    public int compareTo(ProductManufactureCode arg0) {
        return this.code.compareTo(arg0.code);
    }
    
    public String toXml() {
        return "<code>" + code + "</code>";
    }
}
