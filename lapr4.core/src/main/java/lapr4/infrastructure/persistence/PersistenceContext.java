package lapr4.infrastructure.persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lapr4.Application;
import eapli.framework.util.Utility;

@Utility
public final class PersistenceContext {
    private static final Logger LOGGER = LogManager.getLogger(PersistenceContext.class);

    private static RepositoryFactory theFactory;

    private PersistenceContext() {
        // ensure utility
    }

    
    public static RepositoryFactory repositories() {
        if (theFactory == null) {
            final String factoryClassName = Application.settings().getRepositoryFactory();
            System.out.println(factoryClassName);
            try {
                theFactory = (RepositoryFactory) Class.forName(factoryClassName).newInstance();
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
                LOGGER.error("Unable to dynamically load the Repository Factory", ex);
                throw new IllegalStateException(
                        "Unable to dynamically load the Repository Factory: " + factoryClassName,
                        ex);
            }
        }
        return theFactory;
    }
}
