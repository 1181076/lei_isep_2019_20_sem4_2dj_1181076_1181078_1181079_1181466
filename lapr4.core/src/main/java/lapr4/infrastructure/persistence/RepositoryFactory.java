package lapr4.infrastructure.persistence;

import lapr4.factoryusermanagement.repositories.SignupRequestRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.factoryusermanagement.repositories.FactoryUserRepository;
import lapr4.messagemanagement.repositories.MessageRepository;
import lapr4.productionmanagement.repositories.NotificationRepository;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaCategoryRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaRepository;
import lapr4.productmanagement.domain.repositories.DepositRepository;
import lapr4.productmanagement.domain.repositories.ProductLotRepository;

/**
 * The interface for the repository factory of eCafeteria.
 *
 *
 * @author Paulo Gandra Sousa
 *
 */
public interface RepositoryFactory {

    /**
     * Factory method to create a transactional context to use in the repositories
     *
     * @return
     */
    TransactionalContext newTransactionalContext();

    /**
     *
     * @param autoTx
     *            the transactional context to enroll
     * @return
     */
    UserRepository users(TransactionalContext autoTx);

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    UserRepository users();

    /**
     *
     * @param autoTx
     *            the transactional context to enroll
     * @return
     */
    FactoryUserRepository factoryUsers(TransactionalContext autoTx);

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    FactoryUserRepository factoryUsers();

    /**
     *
     * @param autoTx
     *            the transactional context to enroll
     * @return
     */
    SignupRequestRepository signupRequests(TransactionalContext autoTx);

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    SignupRequestRepository signupRequests();

    DepositRepository deposits();
    
    DepositRepository deposits(TransactionalContext autoTx);
    
    ProductRepository products();
    
    ProductRepository products(TransactionalContext autoTx);
    
    ProductionSheetRepository productionSheets();
    
    ProductionSheetRepository productionSheets(TransactionalContext autoTx);
    
    RawMateriaCategoryRepository rawMateriaCategories();
    
    RawMateriaCategoryRepository rawMateriaCategories(TransactionalContext autoTx);
    
    RawMateriaRepository rawMaterias(); 
    
    RawMateriaRepository rawMaterias(TransactionalContext autoTx); 
    
    ProductLotRepository productLots(); 
    
    ProductLotRepository productLots(TransactionalContext autoTx); 
    
    NotificationRepository notifications(); 
    
    NotificationRepository notifications(TransactionalContext autoTx); 
    
    ProductionOrderRepository productionOrders(); 
    
    ProductionOrderRepository productionOrders(TransactionalContext autoTx); 
    
    MachineRepository machines(); 
    
    MachineRepository machines(TransactionalContext autoTx); 
    
    ProductionLineRepository productionLines(); 
    
    ProductionLineRepository productionLines(TransactionalContext autoTx);
    
    MessageRepository messages(); 
    
    MessageRepository messages(TransactionalContext autoTx); 
}
