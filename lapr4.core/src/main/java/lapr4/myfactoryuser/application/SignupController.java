package lapr4.myfactoryuser.application;

import java.util.Calendar;

import lapr4.factoryusermanagement.domain.SignupRequest;
import lapr4.factoryusermanagement.domain.SignupRequestBuilder;
import lapr4.factoryusermanagement.repositories.SignupRequestRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.usermanagement.domain.UserBuilderHelper;
import eapli.framework.application.UseCaseController;
import eapli.framework.time.util.Calendars;

@UseCaseController
public class SignupController {

    private final SignupRequestRepository signupRequestRepository = PersistenceContext
            .repositories().signupRequests();

    public SignupRequest signup(final String username, final String password,
            final String firstName, final String lastName,
            final String email,
            String mecanographicNumber,
            final Calendar createdOn) {

        
        final SignupRequestBuilder signupRequestBuilder = UserBuilderHelper.signupBuilder();
        signupRequestBuilder.withUsername(username).withPassword(password)
                .withName(firstName, lastName).withEmail(email)
                .createdOn(createdOn)
                .withMecanographicNumber(mecanographicNumber);

        final SignupRequest newSignupRequest = signupRequestBuilder.build();
        return this.signupRequestRepository.save(newSignupRequest);
    }

    public SignupRequest signup(final String username, final String password,
            final String firstName, final String lastName,
            final String email,
            String mecanographicNumber) {

        return signup(username, password, firstName, lastName, email,
                mecanographicNumber,
                Calendars.now());
    }
}
