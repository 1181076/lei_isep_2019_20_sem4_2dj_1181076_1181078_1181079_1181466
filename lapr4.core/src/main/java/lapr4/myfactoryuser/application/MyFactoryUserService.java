package lapr4.myfactoryuser.application;

import java.util.Optional;

import lapr4.factoryusermanagement.domain.FactoryUser;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.usermanagement.domain.FactoryRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.infrastructure.authz.application.UserSession;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import lapr4.factoryusermanagement.repositories.FactoryUserRepository;

/**
 *
 * @author Paulo Gandra de Sousa
 */
public class MyFactoryUserService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final FactoryUserRepository repo = PersistenceContext.repositories().factoryUsers();

    public FactoryUser me() {
        final UserSession s = authz.session().orElseThrow(IllegalStateException::new);
        final SystemUser myUser = s.authenticatedUser();
        // TODO cache the cafeteria user object
        final Optional<FactoryUser> me = repo.findByUsername(myUser.identity());
        return me.orElseThrow(IllegalStateException::new);
    }

    public FactoryUser myUser() {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.CAFETERIA_USER);
        final UserSession s = authz.session().orElseThrow(IllegalStateException::new);
        final SystemUser me = s.authenticatedUser();
        return repo.findByUsername(me.identity()).orElseThrow(IllegalStateException::new);
    }

}
