/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.factorymachines.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author vrmpe
 */
public class ListMachineController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MachineRepository machineRepository = PersistenceContext.repositories().machines();
    
    
    public Iterable<Machine> allMachines() {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_CHAO);

        return this.machineRepository.findAll();
    }
}
