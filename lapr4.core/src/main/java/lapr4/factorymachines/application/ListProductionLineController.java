/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.factorymachines.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author mxlsa
 */
public class ListProductionLineController {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionLineRepository pdRepository = PersistenceContext.repositories().productionLines();
    
    public Iterable<ProductionLine> allProductionLines() {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_CHAO);

        return this.pdRepository.findAll();
    }
}
