/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.factorymachines.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Date;
import java.util.Set;
import lapr4.productionmanagement.domain.Notification;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.MachineBrand;
import lapr4.factorymachines.domain.MachineDescript;
import lapr4.factorymachines.domain.MachineModel;
import lapr4.factorymachines.domain.Manufacturer;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.domain.Protocol;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author mxlsa
 */
public class RegisterProductionLineController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionLineRepository productionLineRepository = PersistenceContext.repositories().productionLines();
    
    public ProductionLine addProductionLine(final Long code, Set<Machine> ma, Set<Message> me){
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_CHAO, FactoryRoles.POWER_USER);
       
        final ProductionLine pl= new ProductionLine(code,ma,me);
        
        return this.productionLineRepository.save(pl);
    }
    
    public void addMachine(final Machine mac, final ProductionLine pl){
        ProductionLine p =productionLineRepository.findByID(pl.getId()).get();
        p.getMachines().add(mac);
        
        productionLineRepository.save(p);
    }
    
}

