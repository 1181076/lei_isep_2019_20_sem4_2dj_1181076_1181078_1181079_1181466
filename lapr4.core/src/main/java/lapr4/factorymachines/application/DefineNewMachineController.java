/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.factorymachines.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.factorymachines.repositories.MachineRepository;
import java.util.Date;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.MachineBrand;
import lapr4.factorymachines.domain.MachineDescript;
import lapr4.factorymachines.domain.MachineModel;
import lapr4.factorymachines.domain.Manufacturer;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.domain.Protocol;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author vrmpe
 */
public class DefineNewMachineController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MachineRepository machineRepository = PersistenceContext.repositories().machines();
    private final RegisterProductionLineController pController = new RegisterProductionLineController();
    
    public Machine addMachine(final String numSerie, final Protocol prot, final MachineDescript desc, final Date installationDate, final MachineBrand brand, final MachineModel model, final Manufacturer man, ProductionLine pl){
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_CHAO, FactoryRoles.POWER_USER);
        
        final Machine mach= new Machine(numSerie, prot, desc, installationDate, brand, model, man);
        
        pController.addMachine(mach, pl);
        
        return mach;
    }
    
}
