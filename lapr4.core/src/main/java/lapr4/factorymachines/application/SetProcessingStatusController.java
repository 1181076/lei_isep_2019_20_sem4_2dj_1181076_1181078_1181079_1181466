/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.factorymachines.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author Leticia
 */
public class SetProcessingStatusController {
      private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionLineRepository plRepository = PersistenceContext.repositories().productionLines();
    
    public ProductionLine setProcessingStatus(ProductionLine pl, String element){
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_CHAO, FactoryRoles.POWER_USER);
        if (element.equalsIgnoreCase("active"))
            pl.setProcesseable(true);
        else
            pl.setProcesseable(false);
        
        plRepository.save(pl);
        
        return pl;
    }
      
}
