package lapr4.factorymachines.domain;

import eapli.framework.domain.model.ValueObject;

public enum MachineStatus implements ValueObject{
    ONLINE, RUNNING, OFFLINE, REBOOTING;
    
}
