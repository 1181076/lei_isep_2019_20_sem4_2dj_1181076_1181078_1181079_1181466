package lapr4.factorymachines.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;


@Embeddable
public class MachineModel implements ValueObject{

    public MachineModel() {
    }

    private static final long serialVersionUID = 1L;
    private String model;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public MachineModel(String model) {
        Preconditions.nonNull(model);
        this.model = model;
    }

    @Override
    public int hashCode() {
        return model.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MachineModel)) {
            return false;
        }
        MachineModel other = (MachineModel) object;
        if (!this.model.equals(other.model)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model:" + model + "\n";
    }
    
    public String toXml() {
        return "<name>" + model + "</name>";
    }
}