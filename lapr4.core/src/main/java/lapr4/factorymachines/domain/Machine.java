package lapr4.factorymachines.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

;

@Entity
public class Machine implements Serializable, AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String numSerie;

    private Protocol prot;

    private MachineDescript desc;

    @Temporal(TemporalType.DATE)
    private Date installationDate;

    private MachineBrand brand;

    private MachineModel model;

    private Manufacturer man;

    private ConfigurationFile file;
        
    @Enumerated(EnumType.STRING)
    private MachineStatus status;

    public MachineStatus getStatus() {
        return status;
    }

    public void setStatus(MachineStatus status) {
        this.status = status;
    }
    

    public Machine() {
    }

    public Machine(String numSerie, Protocol prot, MachineDescript desc, Date installationDate, MachineBrand brand, MachineModel model, Manufacturer man) {
        Preconditions.nonNull(numSerie);
        Preconditions.nonNull(installationDate);
        Preconditions.nonNull(man);
        Preconditions.nonNull(model);
        Preconditions.nonNull(brand);
        this.numSerie = numSerie;
        this.prot = prot;
        this.desc = desc;
        this.installationDate = installationDate;
        this.brand = brand;
        this.model = model;
        this.man = man;
        this.file = null;
        this.status = MachineStatus.OFFLINE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Preconditions.nonNull(id);
        this.id = id;
    }

    public ConfigurationFile getFile() {
        return file;
    }

    public void setFile(ConfigurationFile file) {
        this.file = file;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        Preconditions.nonNull(numSerie);
        this.numSerie = numSerie;
    }

    public Protocol getProt() {
        return prot;
    }

    public void setProt(Protocol prot) {
        this.prot = prot;
    }

    public MachineDescript getDesc() {
        return desc;
    }

    public void setDesc(MachineDescript desc) {
        this.desc = desc;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        Preconditions.nonNull(installationDate);
        this.installationDate = installationDate;
    }

    public MachineBrand getBrand() {
        return brand;
    }

    public void setBrand(MachineBrand brand) {
        Preconditions.nonNull(brand);
        this.brand = brand;
    }

    public MachineModel getModel() {
        return model;
    }

    public void setModel(MachineModel model) {
        Preconditions.nonNull(model);
        this.model = model;
    }

    public Manufacturer getMan() {
        return man;
    }

    public void setMan(Manufacturer man) {
        Preconditions.nonNull(man);
        this.man = man;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Machine)) {
            return false;
        }
        Machine other = (Machine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lapr4.factorymachines.domain.Machine[ id=" + id + " ]";
    }

    @Override
    public boolean sameAs(Object object) {
        Machine other = (Machine) object;
        if (this.numSerie.equals(other.numSerie)) {
            return true;
        }
        return false;
    }

    @Override
    public Long identity() {
        return id;
    }

    public String toXml() {
        String s = "<machine idMachine= \"" + id + "\">"
                + "<numSerie>" + numSerie + "</numSerie>";
        if (prot != null) {
                s= s + "<protocol>" + prot.toXml() + "</protocol>";
        }
        
        if (desc != null) {
                s = s + "<description>" + desc.toXml() + "</description>";
        }
                s = s + "<installationDate>" + installationDate.toString() + "</installationDate>"
                + "<brand>" + brand.toXml() + "</brand>"
                + "<model>" + model.toXml() + "</model>"
                + "<manufacturer>" + man.toXml() + "</manufacturer>";
        if (file != null) {
            s = s + "<configurationFile>" + file.toXml() + "</configurationFile>";
        }
        s = s + "<status>" + status + "</status></machine>";
        return s;

    }
}


