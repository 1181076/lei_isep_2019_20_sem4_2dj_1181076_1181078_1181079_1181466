/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.factorymachines.domain;

import eapli.framework.domain.model.ValueObject;

/**
 *
 * @author migue
 */
public class ConfigurationFile implements ValueObject{

    private static final long serialVersionUID = 1L;
    private String file;

    public String getFile() {
        return file;
    }

    public void setFile(String desc) {
        this.file = desc;
    }

    public ConfigurationFile(String desc) {
        this.file = desc;
    }

    public ConfigurationFile() {
    }

    @Override
    public int hashCode() {
        return file.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MachineDescript)) {
            return false;
        }
        ConfigurationFile other = (ConfigurationFile) object;
        if (!this.file.equals(other.file)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "File:" + file + "\n";
    }
    
    public String toXml() {
        return "<filename>" + file + "</filename>";
    }
}
