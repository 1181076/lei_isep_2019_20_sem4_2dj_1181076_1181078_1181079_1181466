package lapr4.factorymachines.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;

@Embeddable
public class MachineDescript implements ValueObject{

    private static final long serialVersionUID = 1L;
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public MachineDescript(String desc) {
        this.desc = desc;
    }

    public MachineDescript() {
    }

    @Override
    public int hashCode() {
        return desc.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MachineDescript)) {
            return false;
        }
        MachineDescript other = (MachineDescript) object;
        if (!this.desc.equals(other.desc)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Description:" + desc + "\n";
    }
    
    public String toXml() {
        return "<machineDescription>" + desc + "</machineDescription>";
    }
    
}