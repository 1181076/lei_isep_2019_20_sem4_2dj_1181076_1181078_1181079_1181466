package lapr4.factorymachines.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;


@Embeddable
public class Protocol implements ValueObject{

    public Protocol() {
    }

    private static final long serialVersionUID = 1L;
    
    private String protocol;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Protocol(String protocol) {
        Preconditions.nonNull(protocol);
        this.protocol = protocol;
    }

    @Override
    public int hashCode() {
        return protocol.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Protocol)) {
            return false;
        }
        Protocol other = (Protocol) object;
        if (!this.protocol.equals(other.protocol)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Protocol:" + protocol + "\n";
    }
    
    public String toXml() {
        return "<machineProtocol>" + protocol + "</machineProtocol>";
    }
}