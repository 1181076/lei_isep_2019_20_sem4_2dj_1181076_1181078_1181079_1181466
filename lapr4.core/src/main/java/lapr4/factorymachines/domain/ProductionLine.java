package lapr4.factorymachines.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lapr4.messagemanagement.domain.Message;
import lapr4.productmanagement.domain.Product;

@Entity
public class ProductionLine implements Serializable, AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Machine> machines = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Message> messages = new HashSet<>();

    private boolean availability;
    
    private boolean processable;
    
    @Temporal(TemporalType.DATE)
    private Date lastProcessed;

    public Date getLastProcessed() {
        return lastProcessed;
    }

    public void setLastProcessed(Date lastProcessed) {
        this.lastProcessed = lastProcessed;
    }

    public boolean isProcesseable() {
        return processable;
    }

    public void setProcesseable(boolean processeable) {
        this.processable = processeable;
    }

    public ProductionLine() {
    }

    private Long code;

    public ProductionLine(Long code, Set<Machine> ma, Set<Message> me) {
        Preconditions.nonNull(me);
        Preconditions.nonNull(ma);
        Preconditions.nonNull(code);
        this.code = code;
        this.machines = ma;
        this.messages = me;
        this.availability = true;
        this.processable = true;
        this.lastProcessed = null;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        Preconditions.nonNull(code);
        this.code = code;
    }

    public Set<Machine> getMachines() {
        return machines;
    }

    public void setMachines(Set<Machine> machines) {
        Preconditions.nonNull(machines);
        this.machines = machines;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        Preconditions.nonNull(messages);
        this.messages = messages;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Preconditions.nonNull(id);
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductionLine)) {
            return false;
        }
        ProductionLine other = (ProductionLine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lapr4.factorymachines.domain.ProductionLine[ id=" + id + " ]";
    }

    @Override
    public boolean sameAs(Object object) {
        ProductionLine other = (ProductionLine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public Long identity() {
        return id;
    }

    public String toXml() {
        String a = "<productionLine idProductionLine = \"" + id + "\">" + "<code>" + code + "</code>"  + "<availability>" + availability + "</availability>"  + "<processable>" + processable + "</processable>" ;

        if(lastProcessed!=null){
            a = a + "<lastprocessed>" + lastProcessed.toString() + "</lastprocessed>";
        }
        
        Iterator<Machine> it = machines.iterator();
        Machine ma;

        if (!machines.isEmpty()) {

            a = a + "<machines>";
            for (int i = 0; i < machines.size(); i++) {
                ma = it.next();
                a = a + ma.toXml();
            }

            a = a + "</machines>";

        }

        Iterator<Message> itt = messages.iterator();
        Message sms;

        if (!messages.isEmpty()) {
            a = a + "<messages>";

            for (int i = 0; i < messages.size(); i++) {
                sms = itt.next();
                a = a + sms.toXml();
            }

            a = a + "</messages>";
        }

        a = a + "</productionLine>";

        return a;
    }

}
