package lapr4.factorymachines.domain;

import eapli.framework.domain.model.ValueObject;
import javax.persistence.Embeddable;


@Embeddable
public class MachineBrand implements ValueObject{

    public MachineBrand() {
    }

    private static final long serialVersionUID = 1L;

    private String brandName;

    public MachineBrand(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

  
   

    @Override
    public int hashCode() {
        return brandName.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MachineBrand)) {
            return false;
        }
        MachineBrand other = (MachineBrand) object;
        if (!this.brandName.equals(other.brandName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Brand Name:" + brandName + "\n";
    }
    
    public String toXml() {
        return "<name>" + brandName + "</name>";
    }
}