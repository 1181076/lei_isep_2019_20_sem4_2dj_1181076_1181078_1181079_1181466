package lapr4.factorymachines.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.factorymachines.domain.ProductionLine;

public interface ProductionLineRepository extends DomainRepository<Long, ProductionLine > {

    default Optional<ProductionLine> findByID(
            final Long Id) {
        return ofIdentity(Id);
    }

    Iterable<ProductionLine> findAllActive();
    
    
    Iterable<ProductionLine> findToBeProcessed();
    
    int numTotal();
}

