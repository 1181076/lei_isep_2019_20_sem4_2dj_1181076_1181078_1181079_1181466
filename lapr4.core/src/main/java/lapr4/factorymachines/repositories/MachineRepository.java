package lapr4.factorymachines.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.factorymachines.domain.Machine;

public interface MachineRepository extends DomainRepository<Long, Machine > {

    default Optional<Machine> findByID(
            final Long Id) {
        return ofIdentity(Id);
    }

    Iterable<Machine> findAllActive();
    Optional<Machine> findByNumSerie(String s);
}
