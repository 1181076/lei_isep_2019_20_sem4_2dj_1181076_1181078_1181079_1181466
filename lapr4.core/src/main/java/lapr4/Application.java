package lapr4;

import eapli.framework.util.Utility;

@Utility
public class Application {

    public static final String VERSION = "1.0.0";
    public static final String COPYRIGHT = "(C) 2020";

    private static final AppSettings SETTINGS = new AppSettings();

    public static AppSettings settings() {
        return SETTINGS;
    }

    private Application() {
        // private visibility to ensure singleton & utility
    }
}
