package lapr4.factoryusermanagement.repositories;

import lapr4.factoryusermanagement.domain.FactoryUser;
import lapr4.factoryusermanagement.domain.MecanographicNumber;
import eapli.framework.domain.repositories.DomainRepository;
import eapli.framework.infrastructure.authz.domain.model.Username;

import java.util.Optional;

/**
 * @author Jorge Santos ajs@isep.ipp.pt 02/04/2016
 */
public interface FactoryUserRepository
        extends DomainRepository<MecanographicNumber, FactoryUser> {

    /**
     * returns the cafeteria user (utente) whose username is given
     *
     * @param name the username to search for
     * @return
     */
    Optional<FactoryUser> findByUsername(Username name);

    /**
     * returns the cafeteria user (utente) with the given mecanographic number
     *
     * @param number
     * @return
     */
    default Optional<FactoryUser> findByMecanographicNumber(
            final MecanographicNumber number) {
        return ofIdentity(number);
    }

    Iterable<FactoryUser> findAllActive();
}
