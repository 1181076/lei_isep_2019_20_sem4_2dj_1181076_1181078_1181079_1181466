package lapr4.factoryusermanagement.application;

import lapr4.factoryusermanagement.domain.SignupRequest;

public interface AcceptRefuseSignupRequestController {

    SignupRequest acceptSignupRequest(SignupRequest theSignupRequest);

    SignupRequest refuseSignupRequest(SignupRequest theSignupRequest);

    Iterable<SignupRequest> listPendingSignupRequests();
}