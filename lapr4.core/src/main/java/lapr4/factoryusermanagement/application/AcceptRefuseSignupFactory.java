package lapr4.factoryusermanagement.application;

import lapr4.Application;

public final class AcceptRefuseSignupFactory {
    private AcceptRefuseSignupFactory() {
        // ensure utility
    }

    public static AcceptRefuseSignupRequestController build() {
        // decide and try
        if (Application.settings().getUseEventfulControllers()) {
            return new AcceptRefuseSignupRequestControllerEventfulImpl();
        } else {
            return new AcceptRefuseSignupRequestControllerTxImpl();
        }
    }
}
