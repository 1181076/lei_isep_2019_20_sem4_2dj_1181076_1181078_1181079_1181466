package lapr4.factoryusermanagement.application;

import java.util.Optional;

import lapr4.factoryusermanagement.domain.FactoryUser;
import lapr4.factoryusermanagement.domain.MecanographicNumber;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.usermanagement.domain.FactoryRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.infrastructure.authz.domain.model.Username;
import lapr4.factoryusermanagement.repositories.FactoryUserRepository;

/**
 *
 * @author mcn
 */
public class FactoryUserService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final FactoryUserRepository repo = PersistenceContext.repositories().factoryUsers();

    public Optional<FactoryUser> findCafeteriaUserByMecNumber(final String mecNumber) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER, FactoryRoles.ADMIN);
        return repo.ofIdentity(MecanographicNumber.valueOf(mecNumber));
    }

    public Optional<FactoryUser> findCafeteriaUserByUsername(final Username user) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER, FactoryRoles.ADMIN);
        return repo.findByUsername(user);
    }

}
