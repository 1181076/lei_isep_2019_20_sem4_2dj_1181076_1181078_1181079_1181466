package lapr4.productionmanagement.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.productionmanagement.domain.ErrorType;
import lapr4.productionmanagement.domain.Notification;

public interface NotificationRepository extends DomainRepository<Long, Notification > {

    default Optional<Notification> findByID(
            final Long Id) {
        return ofIdentity(Id);
    }

    Iterable<Notification> findAllActive();

    public Iterable<Notification> findAllWithoutTreatment();
    
    public Iterable<Notification> findAllWithTreatment();
    
    public Iterable<Notification> findAllWithTreatment(ErrorType t);
    
    public Iterable<Notification> findAllWithoutTreatment(ErrorType t);
}
