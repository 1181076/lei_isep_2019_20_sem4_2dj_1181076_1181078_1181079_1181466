package lapr4.productionmanagement.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;

public interface ProductionOrderRepository extends DomainRepository<Long, ProductionOrder > {

    default Optional<ProductionOrder> findByID(
            final Long Id) {
        return ofIdentity(Id);
    }

    Iterable<ProductionOrder> findAllActive();
    
    Iterable<ProductionOrder> findProductionOrderByIdEncomenda(final String idEncomenda);

    public Iterable<ProductionOrder> findAllByState(OrderStatus orderStatus);

}

