/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productionmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author migue
 */
@Entity
public class Notification implements Serializable, AggregateRoot<Long>  {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private NotificationDescription desc;
    
    @Enumerated(EnumType.STRING)
    private NotificationType type;
    
    @Enumerated(EnumType.STRING)
    private ErrorType errorType;

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }
    
    public Notification() {
    }

    public Notification(NotificationDescription desc) {
        this.desc = desc;
    }

    public NotificationDescription getDesc() {
        return desc;
    }

    public void setDesc(NotificationDescription desc) {
        this.desc = desc;
    }

    public Notification(NotificationDescription desc, NotificationType type) {
        Preconditions.nonNull(desc);
        Preconditions.nonNull(type);
        this.desc = desc;
        this.type = type;
        this.errorType = null;
    }

    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notification)) {
            return false;
        }
        Notification other = (Notification) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lapr4.productionmanagement.domain.Notification[ id=" + id + " ]";
    }
    
    @Override
    public boolean sameAs(Object object) {
        Notification other = (Notification) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public Long identity() {
        return id;
    }
    
    public String toXml() {
        String a = "<notification idNotification = \"" + id + "\">" + "<type>" + type.toString() + "</type>";
        if(errorType!=null){
                a = a + "<errorType>" + errorType.toString() + "</errorType>";
        }
        
        a = a + "<notificationDescription>" + desc.toXml() + "</notificationDescription>" + "</notification>";
        
        return a;
    }
}
