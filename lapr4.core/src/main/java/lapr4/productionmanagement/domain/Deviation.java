/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productionmanagement.domain;

import eapli.framework.domain.model.ValueObject;
import javax.persistence.Embeddable;

/**
 *
 * @author migue
 */
@Embeddable
public class Deviation implements ValueObject {

    private static final long serialVersionUID = 1L;

    private int expectedMateria;
    private int expectedProduct;
    private int realMateria;
    private int realProduct;

    public Deviation() {
    }

    public Deviation(int expectedMateria, int expectedProduct, int realMateria, int realProduct) {
        this.expectedMateria = expectedMateria;
        this.expectedProduct = expectedProduct;
        this.realMateria = realMateria;
        this.realProduct = realProduct;
    }

    public Deviation(int expectedMateria, int expectedProduct) {
        this.expectedMateria = expectedMateria;
        this.expectedProduct = expectedProduct;
        this.realMateria = 0;
        this.realProduct = 0;
    }

    public int getExpectedMateria() {
        return expectedMateria;
    }

    public void setExpectedMateria(int expectedMateria) {
        this.expectedMateria = expectedMateria;
    }

    public int getExpectedProduct() {
        return expectedProduct;
    }

    public void setExpectedProduct(int expectedProduct) {
        this.expectedProduct = expectedProduct;
    }

    public int getRealMateria() {
        return realMateria;
    }

    public void setRealMateria(int realMateria) {
        this.realMateria = realMateria;
    }

    public int getRealProduct() {
        return realProduct;
    }

    public void setRealProduct(int realProduct) {
        this.realProduct = realProduct;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Deviation)) {
            return false;
        }
        Deviation other = (Deviation) object;
        
        return true;
    }

    @Override
    public String toString() {
        return "expectedMateria: " + expectedMateria + "\nexpectedProduct: " + expectedProduct + "\nrealMateria: " + realMateria + "\nrealProduct:" + realProduct;
    }
    
    
    public String toXml() {
        return "<deviation>" + "<expectedMateria>" + expectedMateria + "</expectedMateria>" + "<expectedProduct>" + expectedProduct + "</expectedProduct>" + "<realMateria>" + realMateria + "</realMateria>" + "<realProduct>" + realProduct + "</realProduct>" + "</deviation>";
    }
}
