package lapr4.productionmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.productmanagement.domain.Deposit;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductLot;

@Entity
public class ProductionOrder implements Serializable, AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    private Long id;

    @Temporal(TemporalType.DATE)
    private Date orderEmissionDate;

    @Temporal(TemporalType.DATE)
    private Date previewFinalDate;

    @OneToOne(cascade = CascadeType.ALL)
    private Product product;

    @OneToOne(cascade = CascadeType.ALL)
    private ProductionLine line;

    private String idEncomenda;

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Notification> notifications = new HashSet<>();
    
    @OneToMany(cascade = CascadeType.ALL)
    private Set<ProductLot> lots = new HashSet<>();

    private Deviation dev;

    private StockMovements mov;

    private int amount;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Set<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(Set<Notification> notifications) {
        this.notifications = notifications;
    }

    public Deviation getDev() {
        return dev;
    }

    public void setDev(Deviation dev) {
        this.dev = dev;
    }

    public StockMovements getMov() {
        return mov;
    }

    public void setMov(StockMovements mov) {
        this.mov = mov;
    }

    public ProductionOrder(Long id, Date orderEmissionDate, Date previewFinalDate, Product product, ProductionLine line, String idEncomenda, OrderStatus orderStatus, int amount, Set<ProductLot> deps) {
        Preconditions.nonNull(orderEmissionDate);
        Preconditions.nonNull(product);
        Preconditions.nonNull(idEncomenda);
        Preconditions.nonNull(orderStatus);
        this.id = id;
        this.orderEmissionDate = orderEmissionDate;
        this.previewFinalDate = previewFinalDate;
        this.product = product;
        this.line = line;
        this.idEncomenda = idEncomenda;
        this.orderStatus = orderStatus;
        this.amount = amount;
        this.lots = deps;
        this.dev = new Deviation();
        this.mov = new StockMovements();
    }

    public ProductionOrder(Long id, Date orderEmissionDate, Date previewFinalDate, Product product, ProductionLine line, String idEncomenda, OrderStatus orderStatus, Set<Notification> sNot, int amount, Set<ProductLot> deps) {
        Preconditions.nonNull(orderEmissionDate);
        Preconditions.nonNull(product);
        Preconditions.nonNull(idEncomenda);
        Preconditions.nonNull(sNot);
        Preconditions.nonNull(orderStatus);
        this.id = id;
        this.orderEmissionDate = orderEmissionDate;
        this.previewFinalDate = previewFinalDate;
        this.product = product;
        this.line = line;
        this.idEncomenda = idEncomenda;
        this.orderStatus = orderStatus;
        this.notifications = sNot;
        this.amount = amount;
        this.lots = deps;
        this.dev = new Deviation();
        this.mov = new StockMovements();
    }

    public ProductionOrder() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Preconditions.nonNull(id);
        this.id = id;
    }

    public Date getOrderEmissionDate() {
        return orderEmissionDate;
    }

    public void setOrderEmissionDate(Date orderEmissionDate) {
        Preconditions.nonNull(orderEmissionDate);
        this.orderEmissionDate = orderEmissionDate;
    }

    public Date getPreviewFinalDate() {
        return previewFinalDate;
    }

    public void setPreviewFinalDate(Date previewFinalDate) {
        this.previewFinalDate = previewFinalDate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        Preconditions.nonNull(product);
        this.product = product;
    }

    public ProductionLine getLine() {
        return line;
    }

    public void setLine(ProductionLine line) {
        this.line = line;
    }

    public String getIdEncomenda() {
        return idEncomenda;
    }

    public void setIdEncomenda(String idEncomenda) {
        Preconditions.nonNull(idEncomenda);
        this.idEncomenda = idEncomenda;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Set<ProductLot> getDeps() {
        return lots;
    }

    public void setDeps(Set<ProductLot> deps) {
        this.lots = deps;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductionOrder)) {
            return false;
        }
        ProductionOrder other = (ProductionOrder) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lapr4.productionmanagement.domain.ProductionOrder[ id=" + id + " ]";
    }

    @Override
    public boolean sameAs(Object object) {
        ProductionOrder other = (ProductionOrder) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public Long identity() {
        return id;
    }

    public String toXml() {

        String a;

        a = "<productionOrder idProductionOrder= \"" + id + "\">" + product.toXml()
                + "<amount>" + amount + "</amount>";

        if (mov != null) {
            a = a + mov.toXml();
        }

        if (dev != null) {
            a = a + dev.toXml();
        }

        a = a + "<orderEmissionDate>" + orderEmissionDate.toString() + "</orderEmissionDate>";

        if (previewFinalDate != null) {
            a = a + "<previewFinalDate>" + previewFinalDate.toString() + "</previewFinalDate>";
        }

        if (line != null) {
            a = a + line.toXml();
        }

        a = a + "<orderStatus>" + orderStatus.toString() + "</orderStatus>"
                + "<idEncomenda>" + idEncomenda + "</idEncomenda>";

        if (!lots.isEmpty()) {

            a = a + "<productlots>";

            Iterator<ProductLot> it = lots.iterator();
            ProductLot ma;

            for (int i = 0;
                    i < lots.size();
                    i++) {
                ma = it.next();
                a = a + ma.toXml();
            }

            a = a + "</productlots>";

        }
        
        if (!notifications.isEmpty()) {

            a = a + "<notifications>";

            Iterator<Notification> it = notifications.iterator();
            Notification ma;

            for (int i = 0;
                    i < notifications.size();
                    i++) {
                ma = it.next();
                a = a + ma.toXml();
            }

            a = a + "</notifications>";

        }

        a = a + "</productionOrder>";

        return a;

    }
}
