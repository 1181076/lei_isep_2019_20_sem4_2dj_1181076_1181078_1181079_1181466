/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productionmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.ValueObject;
import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author migue
 */
@Embeddable
public class StockMovements implements ValueObject {

    private static final long serialVersionUID = 1L;

    private int usedMateria;

    public StockMovements(int usedMateria, int newProduct) {
        this.usedMateria = usedMateria;
        this.newProduct = newProduct;
    }

    public StockMovements() {
    }

    public int getUsedMateria() {
        return usedMateria;
    }

    public void setUsedMateria(int usedMateria) {
        this.usedMateria = usedMateria;
    }

    public int getNewProduct() {
        return newProduct;
    }

    public void setNewProduct(int newProduct) {
        this.newProduct = newProduct;
    }
    
    private int newProduct;
    

    @Override
    public int hashCode() {
        int hash = 0;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StockMovements)) {
            return false;
        }
        StockMovements other = (StockMovements) object;
        
        return true;
    }

    @Override
    public String toString() {
        return "usedMateria:" + usedMateria + "\nnewProduct:" + newProduct;
    }
    

    
    public String toXml() {
        return "<stockMovement>" + "<usedMateria>" + usedMateria + "</usedMateria>" + "<newProduct>" + newProduct + "</newProduct>" + "</stockMovement>";
    }
}
