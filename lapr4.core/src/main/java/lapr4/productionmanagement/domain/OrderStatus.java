package lapr4.productionmanagement.domain;

import eapli.framework.domain.model.ValueObject;

public enum OrderStatus implements ValueObject{
    ACCEPTED, PROCESSING, DELIVERING, FINISHED, STOPPED;
}