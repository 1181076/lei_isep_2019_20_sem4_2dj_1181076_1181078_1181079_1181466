package lapr4.productionmanagement.domain;

import eapli.framework.domain.model.ValueObject;

public enum ErrorType implements ValueObject{
    MESSAGE_FORMAT, NON_EXISTENT_PRODUCT, NON_EXISTENT_DEPOSIT, NOT_ENOUGH_PRODUCT, NON_EXISTENT_LOT;
}
