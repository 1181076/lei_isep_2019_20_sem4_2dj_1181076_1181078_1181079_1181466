package lapr4.productionmanagement.domain;

import eapli.framework.domain.model.ValueObject;

public enum NotificationType implements ValueObject{
    DEALT, TBD;
}
