package lapr4.productionmanagement.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Embeddable
public class NotificationDescription implements ValueObject{

    public NotificationDescription() {
    }

    private static final long serialVersionUID = 1L;
    

    private String categ;

    public String getCateg() {
        return categ;
    }

    public void setCateg(String categ) {
        this.categ = categ;
    }

    public NotificationDescription(String categ) {
        Preconditions.nonNull(categ);
        this.categ = categ;
    }

    @Override
    public int hashCode() {
        return categ.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificationDescription)) {
            return false;
        }
        NotificationDescription other = (NotificationDescription) object;
        if (!this.categ.equals(other.categ)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Description:" + categ + "\n";
    }

    public String toXml() {
        return "<description>" + categ + "</description>";
    }
}
