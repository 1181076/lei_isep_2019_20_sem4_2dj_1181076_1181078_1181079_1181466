package lapr4.productionmanagement.application;

import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;

public class AssignUnassignLineToProductionOrderController {
    private final ProductionLineRepository pdRepository = PersistenceContext.repositories().productionLines();
    private final ProductionOrderRepository poRepository = PersistenceContext.repositories().productionOrders();
    
    public ProductionLine assignProductionLineToOrder(ProductionOrder po){
        Iterable<ProductionLine> plL = pdRepository.findAll();
        for(ProductionLine pl: plL){
            if(pl.isAvailability()&&po.getLine()==null){
                po.setLine(pl);
                pl.setAvailability(false);
                pdRepository.save(pl);
                poRepository.save(po);
            }
        }
        
        return po.getLine();
    }
    
    public boolean unassignProductionLineOfOrder(ProductionOrder po){
        Iterable<ProductionLine> plL = pdRepository.findAll();
        ProductionLine pl = po.getLine();
        if(pl!=null){
            pl.setAvailability(true);
            pdRepository.save(pl);
            
        }
        
        return false;
    }
    
}
