/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productionmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author mxlsa
 */
public class AddOrderCSVController {
    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ";";
    private final ProductionOrderRepository poRepo = PersistenceContext.repositories().productionOrders();
    private final ProductRepository pRepo = PersistenceContext.repositories().products();
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    
    public void read(String fileName) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO);
        this.readHeader(fileName);
         try {

            br = new BufferedReader(new FileReader(fileName));
            br.readLine(); //Ignore Header
            while ((line = br.readLine()) != null) {
                String[] pd = line.split(cvsSplitBy);
                this.addProductionOrder(pd[0], pd[1], pd[2], pd[3], pd[4], pd[6]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public void readHeader(String fileName) {
        try {

            br = new BufferedReader(new FileReader(fileName));

            // use comma as separator
            String[] header = line.split(cvsSplitBy);
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private ProductionOrder addProductionOrder(String string, String string0, String string1, String string2, String string3, String string4) {
        System.out.println(string2);
        Product p = pRepo.findByID(Long.parseLong(string2)).get();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, Integer.parseInt(string0.substring(0, 4)));
        c.set(Calendar.MONTH, Integer.parseInt(string0.substring(4, 6)));
        c.set(Calendar.DAY_OF_YEAR, Integer.parseInt(string0.substring(6, 8)));
        Date emission = c.getTime();
        c.set(Calendar.YEAR, Integer.parseInt(string1.substring(0, 4)));
        c.set(Calendar.MONTH, Integer.parseInt(string1.substring(4, 6)));
        c.set(Calendar.DAY_OF_YEAR, Integer.parseInt(string1.substring(6, 8)));
        Date preview = c.getTime();
        ProductionOrder po = new ProductionOrder(Long.parseLong(string), emission,preview,p,null, string4, OrderStatus.ACCEPTED, Integer.parseInt(string3), new HashSet());
        return this.poRepo.save(po);
    }
}
