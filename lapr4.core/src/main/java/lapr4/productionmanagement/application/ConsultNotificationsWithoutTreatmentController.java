/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productionmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.domain.ErrorType;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.repositories.NotificationRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author vrmpe
 */
public class ConsultNotificationsWithoutTreatmentController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final NotificationRepository notificationRepository = PersistenceContext.repositories().notifications();
    
    public Iterable<Notification> consultNotificationsWithoutTreatment() {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_CHAO, FactoryRoles.POWER_USER);

        Iterable<Notification> all = this.notificationRepository.findAllWithoutTreatment();

        return all;
    }
    
    public Iterable<Notification> consultNotificationsWithoutTreatment(ErrorType t) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_CHAO, FactoryRoles.POWER_USER);

        Iterable<Notification> all = this.notificationRepository.findAllWithoutTreatment(t);

        return all;
    }
}
