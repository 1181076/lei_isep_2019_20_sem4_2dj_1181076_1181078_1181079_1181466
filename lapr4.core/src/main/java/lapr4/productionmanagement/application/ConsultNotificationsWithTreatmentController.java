package lapr4.productionmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.domain.ErrorType;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.repositories.NotificationRepository;
import lapr4.usermanagement.domain.FactoryRoles;

public class ConsultNotificationsWithTreatmentController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final NotificationRepository notificationRepository = PersistenceContext.repositories().notifications();
    
    public Iterable<Notification> consultNotificationsWithTreatment() {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_CHAO, FactoryRoles.POWER_USER);

        Iterable<Notification> all = this.notificationRepository.findAllWithTreatment();

        return all;
    }
    
    public Iterable<Notification> consultNotificationsWithTreatment(ErrorType t) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_CHAO, FactoryRoles.POWER_USER);

        Iterable<Notification> all = this.notificationRepository.findAllWithTreatment(t);

        return all;
    }
}
