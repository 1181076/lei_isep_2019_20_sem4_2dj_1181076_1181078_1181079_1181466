/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productionmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Date;
import java.util.Set;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.domain.NotificationDescription;
import lapr4.productionmanagement.domain.NotificationType;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.NotificationRepository;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;
import lapr4.productmanagement.application.ListProductService;
import lapr4.productmanagement.domain.Product;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author migue
 */
public class AddNotificationController {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final NotificationRepository repository = PersistenceContext.repositories().notifications();
    private final ProductionOrderRepository pRepository = PersistenceContext.repositories().productionOrders();
    
    public Notification addNotification(NotificationDescription desc, NotificationType type, ProductionOrder po) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO, FactoryRoles.GESTOR_CHAO);

        
        final Notification newNotification = new Notification(desc, type);
        
        ProductionOrder p = pRepository.findByID(po.getId()).get();
        p.getNotifications().add(newNotification);
        pRepository.save(p);
        return this.repository.save(newNotification);
        
    }

    public Iterable<Notification> notifications() {
        return this.repository.findAll();
    }
}
