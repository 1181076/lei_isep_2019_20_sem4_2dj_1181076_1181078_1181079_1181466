/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productionmanagement.application;

import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;

/**
 *
 * @author vrmpe
 */
public class ListNotificationsByProductionOrderController {
    
    private final ProductionOrderRepository theRepo = PersistenceContext.repositories().productionOrders();
    
    public Iterable<ProductionOrder> getProductionOrderList(){
        
        return this.theRepo.findAll();

    }
    
}
