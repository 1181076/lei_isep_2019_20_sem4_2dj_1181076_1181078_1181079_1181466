/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productionmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Date;
import java.util.Set;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;
import lapr4.productmanagement.application.ListProductService;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductLot;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author Leticia
 */
public class AddProductionOrderController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionOrderRepository repository = PersistenceContext.repositories().productionOrders();
    private final ListProductService svc = new ListProductService();

    public ProductionOrder addProductionOrder(Long id, final Date orderEmissionDate, final Date previewFinalDate, final Product product, final ProductionLine line, final String idEncomenda, final OrderStatus orderStatus, int amount, Set<ProductLot> sp) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO, FactoryRoles.GESTOR_CHAO);

        final ProductionOrder newProductionOrder = new ProductionOrder(id, orderEmissionDate, previewFinalDate, product, line, idEncomenda, orderStatus, amount, sp);
        return this.repository.save(newProductionOrder);
    }
    
    public ProductionOrder addProductionOrder(Long id, final Date orderEmissionDate, final Date previewFinalDate, final Product product, final ProductionLine line, final String idEncomenda, final OrderStatus orderStatus, Set<Notification> s, int amount, Set<ProductLot> sp) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO, FactoryRoles.GESTOR_CHAO);

        final ProductionOrder newProductionOrder = new ProductionOrder(id, orderEmissionDate, previewFinalDate, product, line, idEncomenda, orderStatus, amount, sp);
        return this.repository.save(newProductionOrder);
    }

    public Iterable<Product> Products() {
        return this.svc.allProducts();
    }
}
