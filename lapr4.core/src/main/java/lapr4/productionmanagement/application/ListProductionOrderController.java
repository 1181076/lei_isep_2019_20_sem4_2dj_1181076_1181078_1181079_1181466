/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productionmanagement.application;

import lapr4.productionmanagement.domain.ProductionOrder;

/**
 *
 * @author Leticia
 */
public class ListProductionOrderController {
       private final ListProductionOrderService svc = new ListProductionOrderService();

    public Iterable<ProductionOrder> listProductionOrders() {
        return this.svc.allProductionOrders();
    }
}
