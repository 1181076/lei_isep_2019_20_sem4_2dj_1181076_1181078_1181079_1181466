/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.productionmanagement.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author mxlsa
 */
public class ConsultOrderStateController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
     private final ProductionOrderRepository notificationRepository = PersistenceContext.repositories().productionOrders();
     
    public Iterable<ProductionOrder> consultOrdersByState(OrderStatus orderStatus) {
       authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_PRODUCAO, FactoryRoles.POWER_USER);
        Iterable<ProductionOrder> all = this.notificationRepository.findAllByState(orderStatus);
        return all;
       
    }

}
