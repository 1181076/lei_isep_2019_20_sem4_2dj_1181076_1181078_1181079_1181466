/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.factoryusermanagement.repositories.FactoryUserRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.repositories.MessageRepository;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.NotificationRepository;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;
import lapr4.productmanagement.domain.Deposit;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductLot;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.RawMateria;
import lapr4.productmanagement.domain.RawMateriaCategory;
import lapr4.productmanagement.domain.repositories.DepositRepository;
import lapr4.productmanagement.domain.repositories.ProductLotRepository;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaCategoryRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaRepository;

/**
 *
 * @author migue
 */
public class ExportToXMLController {

    private final ProductRepository productRepository = PersistenceContext.repositories().products();
    private final RawMateriaRepository rawMateriaRepository = PersistenceContext.repositories().rawMaterias();
    private final DepositRepository depositRepository = PersistenceContext.repositories().deposits();
    private final RawMateriaCategoryRepository rawMateriaCategoryRepository = PersistenceContext.repositories().rawMateriaCategories();
    private final ProductionSheetRepository productionSheetsRepository = PersistenceContext.repositories().productionSheets();
    private final NotificationRepository notificationRepository = PersistenceContext.repositories().notifications();
    private final ProductionOrderRepository productionOrderRepository = PersistenceContext.repositories().productionOrders();
    private final ProductLotRepository productionLotRepository = PersistenceContext.repositories().productLots();
    private final FactoryUserRepository repo = PersistenceContext.repositories().factoryUsers();
    private final ProductionLineRepository pdRepository = PersistenceContext.repositories().productionLines();
    private final MachineRepository machineRepository = PersistenceContext.repositories().machines();
    private final MessageRepository messageRepository = PersistenceContext.repositories().messages();

    private String[] conf;

    public void exportTo(String name, String[] c, Date s, Date e) {
        conf = c;

        String xmlProducts = getProductsInXml();
        String xmlRawMaterial = getRawMateriaInXml();
        String xmlRawMaterialCategory = getRawMateriaCategoryInXml();
        String xmlDeposit = getDepositInXml();
        String xmlProdSheet = getProductionSheetsInXml();
        String xmlNotif = getNotificationsInXml();
        String xmlProdOrder = getProductionOrderInXml(s, e);
        String xmlProdLine = getProductionLineInXml();
        String xmlMachine = getMachineInXml();
        String xmlMessage = getMessageInXml();
        String xmlProdL = getProductLotsInXml();
        String finalS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n<fabrica xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
                + "          xsi:noNamespaceSchemaLocation=\"EFactory.xsd\">"
                + xmlDeposit + xmlRawMaterial + xmlRawMaterialCategory + xmlProducts + xmlProdSheet
                + xmlProdOrder + xmlProdL + xmlNotif + xmlProdLine + xmlMachine + xmlMessage
                + "</fabrica>";
        save(finalS, name);
    }

    public String getProductsInXml() {
        Iterable<Product> listProducts = productRepository.findAll();
        StringBuilder xml = new StringBuilder("<products>");
        if (conf[0].equalsIgnoreCase("Y")) {
            for (Product p : listProducts) {
                xml.append(p.toXml()).append("\n");
            }
            xml.append("</products>");
        }
        return xml.toString();
    }

    public String getRawMateriaInXml() {
        Iterable<RawMateria> listRM = rawMateriaRepository.findAll();
        StringBuilder xml = new StringBuilder("<rawMaterias>");
        if (conf[1].equalsIgnoreCase("Y")) {
            for (RawMateria p : listRM) {
                xml.append(p.toXml()).append("\n");
            }
        }
        xml.append("</rawMaterias>");
        return xml.toString();
    }

    public String getRawMateriaCategoryInXml() {
        Iterable<RawMateriaCategory> listRMC = rawMateriaCategoryRepository.findAll();
        StringBuilder xml = new StringBuilder("<rawMateriaCategories>");
        if (conf[2].equalsIgnoreCase("Y")) {
            for (RawMateriaCategory p : listRMC) {
                xml.append(p.toXml()).append("\n");
            }
        }
        xml.append("</rawMateriaCategories>");
        return xml.toString();
    }

    public String getDepositInXml() {
        Iterable<Deposit> listD = depositRepository.findAll();
        StringBuilder xml = new StringBuilder("<deposits>");
        if (conf[3].equalsIgnoreCase("Y")) {
            for (Deposit p : listD) {
                xml.append(p.toXml()).append("\n");
            }
        }
        xml.append("</deposits>");
        return xml.toString();
    }

    public String getProductionSheetsInXml() {
        Iterable<ProductionSheet> listPS = productionSheetsRepository.findAll();
        StringBuilder xml = new StringBuilder("<productionSheets>");
        if (conf[4].equalsIgnoreCase("Y")) {
            for (ProductionSheet p : listPS) {
                xml.append(p.toXml()).append("\n");
            }
        }
        xml.append("</productionSheets>");
        return xml.toString();
    }

    public String getNotificationsInXml() {
        Iterable<Notification> listN = notificationRepository.findAll();
        StringBuilder xml = new StringBuilder("<notifications>");
        if (conf[5].equalsIgnoreCase("Y")) {
            for (Notification p : listN) {
                xml.append(p.toXml()).append("\n");
            }
        }
        xml.append("</notifications>");
        return xml.toString();
    }

    public String getProductionOrderInXml(Date s, Date e) {
        Iterable<ProductionOrder> listPO = productionOrderRepository.findAll();
        StringBuilder xml = new StringBuilder("<productionOrders>");
        if (conf[6].equalsIgnoreCase("Y")) {
            for (ProductionOrder p : listPO) {
                if (s != null && e != null) {
                    if (p.getOrderEmissionDate().after(s) && p.getOrderEmissionDate().before(e)) {
                        xml.append(p.toXml()).append("\n");
                    }
                } else {
                    xml.append(p.toXml()).append("\n");
                }
            }
        }
        xml.append("</productionOrders>");
        return xml.toString();
    }

    public String getProductionLineInXml() {
        Iterable<ProductionLine> listPL = pdRepository.findAll();
        StringBuilder xml = new StringBuilder("<productionLines>");
        if (conf[8].equalsIgnoreCase("Y")) {
            for (ProductionLine p : listPL) {
                xml.append(p.toXml()).append("\n");
            }
        }
        xml.append("</productionLines>");
        return xml.toString();
    }

    public String getMachineInXml() {
        Iterable<Machine> listMac = machineRepository.findAll();
        StringBuilder xml = new StringBuilder("<machines>");
        if (conf[9].equalsIgnoreCase("Y")) {
            for (Machine p : listMac) {
                xml.append(p.toXml()).append("\n");
            }
        }
        xml.append("</machines>");
        return xml.toString();
    }

    public String getMessageInXml() {
        Iterable<Message> listMes = messageRepository.findAll();
        StringBuilder xml = new StringBuilder("<messages>");
        if (conf[10].equalsIgnoreCase("Y")) {
            for (Message p : listMes) {
                xml.append(p.toXml()).append("\n");
            }
        }
        xml.append("</messages>");
        return xml.toString();
    }

    public String getProductLotsInXml() {
        Iterable<ProductLot> listProdL = productionLotRepository.findAll();
        StringBuilder xml = new StringBuilder("<productlots>");
        if (conf[7].equalsIgnoreCase("Y")) {
            for (ProductLot p : listProdL) {
                xml.append(p.toXml()).append("\n");
            }
        }
        xml.append("</productlots>");
        return xml.toString();
    }

    public void save(String xml, String fileName) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(xml);
        } catch (IOException e) {
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
            }
        }
    }

}
