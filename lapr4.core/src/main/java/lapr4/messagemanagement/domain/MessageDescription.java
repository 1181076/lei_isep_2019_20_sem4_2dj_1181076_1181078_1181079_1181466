/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.messagemanagement.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lapr4.productmanagement.domain.RawMateriaDescription;

/**
 *
 * @author migue
 */

@Embeddable
public class MessageDescription implements ValueObject{

    public MessageDescription() {
    }
    private static final long serialVersionUID = 1L;
    
    private String desc;

    public MessageDescription(String desc) {
        Preconditions.nonNull(desc);
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    @Override
    public int hashCode() {
        return desc.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MessageDescription)) {
            return false;
        }
        MessageDescription other = (MessageDescription) object;
        if (this.desc.equals(other.desc)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "description: "+ desc + "\n" ;
    }
    
    public String toXml() {
        return "<messageDescription>" + desc + "</messageDescription>";
    }
}
