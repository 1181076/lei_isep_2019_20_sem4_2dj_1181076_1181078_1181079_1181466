/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.messagemanagement.domain;

import eapli.framework.domain.model.ValueObject;

/**
 *
 * @author migue
 */
public enum MessageType implements ValueObject {
     CONSUMO, ENTREGA, PRODUCAO, ESTORNO, INICIO, RETOMA, PARAGEM, FIM

}
