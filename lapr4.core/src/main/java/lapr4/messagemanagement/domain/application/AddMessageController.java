/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.messagemanagement.domain.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.domain.MessageDescription;
import lapr4.messagemanagement.domain.MessageType;
import lapr4.messagemanagement.repositories.MessageRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author migue
 */
public class AddMessageController {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MessageRepository repository = PersistenceContext.repositories().messages();
    private final ProductionLineRepository pRepository = PersistenceContext.repositories().productionLines();
    
    public Message addMessage(MessageDescription desc, MessageType type, ProductionLine po) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO, FactoryRoles.GESTOR_CHAO);

        
        final Message newMessage = new Message(desc, type);
        
        ProductionLine p = pRepository.findByID(po.getId()).get();
        p.getMessages().add(newMessage);
        pRepository.save(p);
        return newMessage;
        
    }
    
    public Message addMessage(Message m, ProductionLine po) {
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO, FactoryRoles.GESTOR_CHAO);

        
        ProductionLine p = pRepository.findByID(po.getId()).get();
        p.getMessages().add(m);
        pRepository.save(p);
        return m;
        
    }

    public Iterable<Message> messages() {
        return this.repository.findAll();
    }
}
