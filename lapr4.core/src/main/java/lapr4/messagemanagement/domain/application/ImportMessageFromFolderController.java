/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.messagemanagement.domain.application;

import eapli.framework.domain.repositories.IntegrityViolationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr4.factorymachines.application.ListProductionLineController;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.domain.MessageDescription;
import lapr4.messagemanagement.domain.MessageType;
import lapr4.messagemanagement.repositories.MessageRepository;

/**
 *
 * @author migue
 */
public class ImportMessageFromFolderController {

    private final String out = "processedInputMessageFiles";

    private final MessageRepository repository = PersistenceContext.repositories().messages();
    private final ProductionLineRepository Pepository = PersistenceContext.repositories().productionLines();
    private final MachineRepository Mepository = PersistenceContext.repositories().machines();
    private final ProductionLineRepository pdRepository = PersistenceContext.repositories().productionLines();

    private final File fff;

    BufferedReader br = null;

    private boolean empty = true;

    public ImportMessageFromFolderController(File fff) {
        this.fff = fff;
    }

    public synchronized void read() throws IOException, InterruptedException {

        List<Message> lst = new ArrayList();

        String line = null;

        try {
            br = new BufferedReader(new FileReader(fff));
            line = br.readLine();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImportMessageFromFolderController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImportMessageFromFolderController.class.getName()).log(Level.SEVERE, null, ex);
        }

        String pID = line.split(";")[0];

        Machine m = Mepository.findByNumSerie(pID).get();
        Iterable<ProductionLine> ls = pdRepository.findAll();

        ProductionLine pl1 = new ProductionLine();

        for (ProductionLine pl : ls) {
            if (pl.getMachines().contains(m)) {
                pl1 = pl;
            }
        }

        while (line != null) {
            MessageDescription desc = new MessageDescription(line);

            String ty = line.split(";")[1];

            MessageType type = MessageType.CONSUMO;

            Message newMessage;

            switch (ty) {
                case "C0":
                    type = MessageType.CONSUMO;
                    break;
                case "C9":
                    type = MessageType.ENTREGA;
                    break;
                case "P1":
                    type = MessageType.PRODUCAO;
                    break;
                case "P2":
                    type = MessageType.ESTORNO;
                    break;
                case "S0":
                    type = MessageType.INICIO;
                    break;
                case "S1":
                    type = MessageType.RETOMA;
                    break;
                case "S8":
                    type = MessageType.PARAGEM;
                    break;
                case "S9":
                    type = MessageType.FIM;
                    break;
            }
            
            
            String[] d = desc.getDesc().split(";");
            Calendar date = Calendar.getInstance();
            date.set(Calendar.YEAR, Integer.parseInt(d[2].substring(0, 4)));
            date.set(Calendar.MONTH, Integer.parseInt(d[2].substring(4, 6)));
            date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(d[2].substring(6, 8)));
            date.set(Calendar.HOUR_OF_DAY, Integer.parseInt(d[2].substring(8, 10)));
            date.set(Calendar.MINUTE, Integer.parseInt(d[2].substring(10, 12)));
            date.set(Calendar.SECOND, Integer.parseInt(d[2].substring(12, 14)));
            
            System.out.println(date.getTime().toString());

            newMessage = new Message(desc, type, date.getTime());

            try {
                System.out.println(lst.add(newMessage));
                line = br.readLine();
            } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
                System.out.println("You tried to enter a Message which already exists in the database.");
            } catch (IOException ex) {
                Logger.getLogger(ImportMessageFromFolderController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        System.out.println("***" + pl1.getMessages().addAll(lst));
        Pepository.save(pl1);

        br.close();

        empty = false;
        System.out.println("reader Notifying...");
        notifyAll();

    }

    public synchronized void delete() {
        while (empty) {
            System.out.println("WAITING... " + fff.getName());

            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(ImportMessageFromFolderController.class.getName()).log(Level.SEVERE, null, ex);
            }
            fff.renameTo(new File(out + "/" + fff.getName()));
            fff.delete();

        }
    }

}
