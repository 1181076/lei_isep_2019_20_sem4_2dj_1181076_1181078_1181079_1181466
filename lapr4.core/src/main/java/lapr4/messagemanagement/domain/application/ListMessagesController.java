/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.messagemanagement.domain.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.repositories.MessageRepository;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.repositories.NotificationRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author migue
 */
public class ListMessagesController {    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MessageRepository mRepository = PersistenceContext.repositories().messages();
    
    public Iterable<Message> listAllMessages(){
        
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_CHAO, FactoryRoles.POWER_USER);

        Iterable<Message> all = this.mRepository.findAll();

        return all;
    }
}
