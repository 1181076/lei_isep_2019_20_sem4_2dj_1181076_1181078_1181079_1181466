/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.messagemanagement.domain.application;

import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.Optional;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.repositories.MessageRepository;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.repositories.NotificationRepository;
import lapr4.usermanagement.domain.FactoryRoles;

/**
 *
 * @author migue
 */
public class RemoveMessageController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MessageRepository mRepository = PersistenceContext.repositories().messages();
    
    public void removeMessage(final long id){
        
        authz.ensureAuthenticatedUserHasAnyOf(FactoryRoles.GESTOR_CHAO, FactoryRoles.POWER_USER);
        Optional<Message> no = mRepository.findByID(id);
        mRepository.remove(no.get());
    }
}
