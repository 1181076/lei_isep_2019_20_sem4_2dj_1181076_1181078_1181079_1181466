/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.messagemanagement.domain.application;

import eapli.framework.domain.repositories.IntegrityViolationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.domain.MessageDescription;
import lapr4.messagemanagement.domain.MessageType;
import lapr4.messagemanagement.repositories.MessageRepository;

/**
 *
 * @author migue
 */
public class ImportMessageFromDataController {
    
    public synchronized Message importMessage(String message) {

        
            MessageDescription desc = new MessageDescription(message);

            String ty = message.split(";")[1];

            MessageType type = MessageType.CONSUMO;

            Message newMessage;

            switch (ty) {
                case "C0":
                    type = MessageType.CONSUMO;
                    break;
                case "C9":
                    type = MessageType.ENTREGA;
                    break;
                case "P1":
                    type = MessageType.PRODUCAO;
                    break;
                case "P2":
                    type = MessageType.ESTORNO;
                    break;
                case "S0":
                    type = MessageType.INICIO;
                    break;
                case "S1":
                    type = MessageType.RETOMA;
                    break;
                case "S8":
                    type = MessageType.PARAGEM;
                    break;
                case "S9":
                    type = MessageType.FIM;
                    break;
            }
            
            
            String[] d = desc.getDesc().split(";");
            Calendar date = Calendar.getInstance();
            date.set(Calendar.YEAR, Integer.parseInt(d[2].substring(0, 4)));
            date.set(Calendar.MONTH, Integer.parseInt(d[2].substring(4, 6)));
            date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(d[2].substring(6, 8)));
            date.set(Calendar.HOUR_OF_DAY, Integer.parseInt(d[2].substring(8, 10)));
            date.set(Calendar.MINUTE, Integer.parseInt(d[2].substring(10, 12)));
            date.set(Calendar.SECOND, Integer.parseInt(d[2].substring(12, 14)));
            
            System.out.println(date.getTime().toString());

            newMessage = new Message(desc, type, date.getTime());

            return newMessage;
    }
}
