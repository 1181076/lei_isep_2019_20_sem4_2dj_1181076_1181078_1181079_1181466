/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.messagemanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author migue
 */
@Entity
public class Message implements Serializable, AggregateRoot<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public Message() {
    }

    @Column(unique = true)
    private MessageDescription desc;

    private Date data;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    private boolean processed;

    @Enumerated(EnumType.STRING)
    private MessageType type;

    public Message(MessageDescription desc, MessageType type) {
        Preconditions.nonNull(desc);
        Preconditions.nonNull(type);
        this.desc = desc;
        this.type = type;
        data = Calendar.getInstance().getTime();
        processed = false;
    }

    public Message(MessageDescription desc, MessageType type, Date data) {
        Preconditions.nonNull(desc);
        Preconditions.nonNull(type);
        this.desc = desc;
        this.type = type;
        this.data = data;
        processed = false;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MessageDescription getDesc() {
        return desc;
    }

    public void setDesc(MessageDescription desc) {
        this.desc = desc;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Message)) {
            return false;
        }
        Message other = (Message) object;
        if (this.desc.equals(other.desc)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "<message idMessage= \"" + id + "\">" + "<description>" + desc + "</description>" + "<messageType>" + type.name() + "</messageType>" + "<date>" + data.toString() + "</date>" + "<processed>" + processed + "</processed>" + "</message>";
    }

    @Override
    public boolean sameAs(Object object) {
        Message other = (Message) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public Long identity() {
        return id;
    }

    public String toXml() {
        return "<message idMessage= \"" + id + "\">" + "<description>" + desc.getDesc()+ "</description>" + "<messageType>" + type.name() + "</messageType>" + "<date>" + data.toString() + "</date>" + "<processed>" + processed + "</processed>" + "</message>";
    }
}
