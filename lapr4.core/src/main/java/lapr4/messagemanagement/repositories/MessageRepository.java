package lapr4.messagemanagement.repositories;

import eapli.framework.domain.repositories.DomainRepository;
import java.util.Optional;
import lapr4.messagemanagement.domain.Message;

public interface MessageRepository extends DomainRepository<Long, Message > {

    default Optional<Message> findByID(
            final Long Id) {
        return ofIdentity(Id);
    }

    Iterable<Message> findAllActive();
    void process(Long id);
}
