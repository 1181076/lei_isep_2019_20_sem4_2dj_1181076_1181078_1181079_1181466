#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For constants “mode” */
#include <fcntl.h> /* For constants O_* */
#include <pthread.h>
#include <sys/socket.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <openssl/crypto.h>
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define BUF_SIZE 30
#define SERVER_PORT "9999"
#define SERVER_ALT "9995"
#define SERVER_ALT2 "9991"
// read a string from stdin protecting buffer overflow
#define GETS(B,S) {fgets(B,S-2,stdin);B[strlen(B)-1]=0;}

typedef struct{ 
	char * ipAddressName;
	char * machineID;
	int cadence;
        char * configFile;
        char state[10];
} Machine;

void* thread_func(void *arg){
	
    Machine *mach = (Machine*) arg;
    
    while(strcmp(mach->state,"REBOOTING")==0){
    }

    int err, sock;
    unsigned long f, i, n, num;
    unsigned char bt;
    char linha[BUF_SIZE];
    struct addrinfo req, *list;
    
    bzero((char *)&req,sizeof(req));// let getaddrinfo set the family depending on the supplied server address
    
    req.ai_family = AF_UNSPEC;
    req.ai_socktype = SOCK_STREAM;
    
    err=getaddrinfo(mach->ipAddressName, "9999", &req, &list);
    if(err) {  //COLOCAR AQUI IP DO SERVIDOR
        printf("Failed to get server address, error: %s\n",gai_strerror(err)); 
        exit(1); 
    }
    
    sock=socket(list->ai_family,list->ai_socktype,list->ai_protocol);
    
    if(sock==-1){
        perror("Failed to open SCM socket"); 
        freeaddrinfo(list);
        close(sock);
        pthread_exit((void*)NULL);
    }
    
    if(connect(sock,(struct sockaddr *)list->ai_addr, list->ai_addrlen)==-1){
        perror("Failed connect to SCM"); 
        freeaddrinfo(list); 
        close(sock);
        pthread_exit((void*)NULL);
    }  
    

	printf("------->1\n");
        const SSL_METHOD *method=SSLv23_client_method();
        printf("------->2\n");
        SSL_CTX *ctx = SSL_CTX_new(method);
        printf("------->3\n");
        SSL_CTX_use_certificate_file(ctx, "client1.pem", SSL_FILETYPE_PEM);
        printf("------->4\n");
        	SSL_CTX_use_PrivateKey_file(ctx, "client1.key", SSL_FILETYPE_PEM);
        printf("------->5\n");
        	if (!SSL_CTX_check_private_key(ctx)) {
                	puts("Error loading client's certificate/key");
			close(sock);
                	exit(1);
			}
        printf("------->6\n");

	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER,NULL);
        printf("------->7\n");
        // THE SERVER'S CERTIFICATE IS TRUSTED
        SSL_CTX_load_verify_locations(ctx,"server_J.pem",NULL);
        printf("------->8\n");
	// Restrict TLS version and cypher suites
        SSL_CTX_set_min_proto_version(ctx,TLS1_2_VERSION);
        printf("------->9\n");
	SSL_CTX_set_cipher_list(ctx, "HIGH:!aNULL:!kRSA:!PSK:!SRP:!MD5:!RC4");
        printf("------->10\n");
	SSL *sslConn = SSL_new(ctx);
        printf("------->11\n");
        SSL_set_fd(sslConn, sock);
        printf("------->12\n");
        if(SSL_connect(sslConn)!=1) {
		puts("TLS handshake error");
                SSL_free(sslConn);
                close(sock);
                exit(1);
	}
        printf("------->13\n");
	printf("TLS version: %s\nCypher suite: %s\n",SSL_get_cipher_version(sslConn),SSL_get_cipher(sslConn));
        printf("------->14\n");
	if(SSL_get_verify_result(sslConn)!=X509_V_OK) {
		puts("Sorry: invalid server certificate");
                SSL_free(sslConn);
                close(sock);
                exit(1);
        }
        printf("------->15\n");
	X509* cert=SSL_get_peer_certificate(sslConn);
        X509_free(cert);
        printf("------->16\n");
        if(cert==NULL) {
                puts("Sorry: no certificate provided by the server");
                SSL_free(sslConn);
                close(sock);
                exit(1);
        }
        printf("------->17\n");

    FILE * fp;
    char * line = (char*)malloc(60 * sizeof(char));
    size_t len = 60;
    ssize_t read;
    char *file = (char*)malloc(13 * sizeof(char));
    
    sprintf(file, "Mensagens_%s.txt",mach->machineID);
    
    fp = fopen(file, "r");
    
    if (fp == NULL){
        exit(EXIT_FAILURE);
	}
    
	strncpy(mach->state, "RUNNING", 10);
	
	int nBytes;
    
    while ((read = getline(&line, &len, fp)) != -1) {
        while(strcmp(mach->state,"REBOOTING")==0){
        }
        nBytes =  strlen(line);
        printf("%s", line);
        printf("\n%d", nBytes);
        printf("\n---\n");
        uint32_t un = htonl(nBytes);
        SSL_write(sslConn, &un, sizeof(un));
        line[nBytes] = 0;
        SSL_write(sslConn,line,nBytes);
        time_t current_time = 0;
        current_time = clock();
        for ( ; (clock() - current_time) < (mach->cadence * CLOCKS_PER_SEC); );
        
    }
    
    nBytes = -1;
    uint32_t un = htonl(nBytes);
    SSL_write(sslConn,&un, sizeof(un));
    printf("Success");
    SSL_free(sslConn);
	strncpy(mach->state, "ONLINE", 10);
    fclose(fp);
    close(sock);
    pthread_exit((void*)NULL);
}



void* thread_func2(void *arg){
		
    Machine *mach = (Machine*) arg;
    
    struct sockaddr_storage client;
    
    int err, sock, res, i;
    
    unsigned int adl;
    
    char linha[BUF_SIZE];
    
    char* linha1;
    
    char cliIPtext[BUF_SIZE], cliPortText[BUF_SIZE];
    
    struct addrinfo  req, *list;
    
    bzero((char *)&req,sizeof(req));// request a IPv6 local address will allow both IPv4 and IPv6 clients to use it
    
    req.ai_family = AF_INET6;req.ai_socktype = SOCK_DGRAM;
    
    req.ai_flags = AI_PASSIVE;// local address
    
    err=getaddrinfo(NULL, SERVER_PORT , &req, &list);
    
    if(err) {
        printf("Failed to get SMM local address, error: %s\n",gai_strerror(err));
         exit(1); 
    }
    
    sock=socket(list->ai_family,list->ai_socktype,list->ai_protocol);
    
    if(sock==-1) {
		perror("Failed to open SMM socket"); 
		freeaddrinfo(list); 
		exit(1);
	}
		
    if(bind(sock,(struct sockaddr *)list->ai_addr, list->ai_addrlen)==-1) {
        perror("Bind with SMM failed");
        close(sock);
        freeaddrinfo(list);
        exit(1);
    }
    
    freeaddrinfo(list);
    
    puts("Listening for UDP requests (IPv6/IPv4) from SMM. Use CTRL+C to terminate the server");
    
    adl=sizeof(client);
    
    while(1){
		res=recvfrom(sock,linha,BUF_SIZE,0,(struct sockaddr *)&client,&adl);
		
		if(!getnameinfo((struct sockaddr *)&client,adl,cliIPtext,BUF_SIZE,cliPortText,BUF_SIZE,NI_NUMERICHOST|NI_NUMERICSERV)){ 
			printf("\n\nRequest from node %s, port number %s\n", cliIPtext, cliPortText);
		}else{
			puts("Got request, but failed to get client address");
		}

                printf("%s\n\n", linha);
		
		if(strcmp(linha, mach->machineID) == 0){
			linha1 = mach->state;
		}else{
			linha1 = "OFFLINE";
		}
		
		sendto(sock,linha1,strlen(linha1),0,(struct sockaddr *)&client,adl);
                 memset(linha, 0, sizeof linha);
	}
    
    pthread_exit((void*)NULL);
    
    close(sock);
    exit(0);
}

void* thread_func3(void *arg){
		
    Machine *mach = (Machine*) arg;
    
    struct sockaddr_storage client;
    
    int err, sock, res, i;
    
    unsigned int adl;
    
    char linha[BUF_SIZE];
    
    char* linha1;
    
    char cliIPtext[BUF_SIZE], cliPortText[BUF_SIZE];
    
    struct addrinfo  req, *list;
    
    bzero((char *)&req,sizeof(req));// request a IPv6 local address will allow both IPv4 and IPv6 clients to use it
    
    req.ai_family = AF_INET6;req.ai_socktype = SOCK_DGRAM;
    
    req.ai_flags = AI_PASSIVE;// local address
    
    err=getaddrinfo(NULL, SERVER_ALT, &req, &list);
    
    if(err) {
        printf("Failed to get SMM local address, error: %s\n",gai_strerror(err));
         exit(1); 
    }
    
    sock=socket(list->ai_family,list->ai_socktype,list->ai_protocol);
    
    if(sock==-1) {
		perror("Failed to open SMM socket"); 
		freeaddrinfo(list); 
		exit(1);
	}
		
    if(bind(sock,(struct sockaddr *)list->ai_addr, list->ai_addrlen)==-1) {
        perror("Bind with SMM failed");
        close(sock);
        freeaddrinfo(list);
        exit(1);
    }
    
    freeaddrinfo(list);
    
    puts("Listening for UDP rebooting requests (IPv6/IPv4) from SMM. Use CTRL+C to terminate the server");
    
    adl=sizeof(client);
    while(1){
		res=recvfrom(sock,linha,BUF_SIZE,0,(struct sockaddr *)&client,&adl);
		
		if(!getnameinfo((struct sockaddr *)&client,adl,cliIPtext,BUF_SIZE,cliPortText,BUF_SIZE,NI_NUMERICHOST|NI_NUMERICSERV)){ 
			printf("\n\nRebooting request from node %s, port number %s\n", cliIPtext, cliPortText);
		}else{
			puts("Got request, but failed to get client address");
		}

                printf("%s\n\n", linha);
		
		if(strcmp(linha, mach->machineID) == 0){
			strncpy(mach->state, "REBOOTING", 10);
            linha1= mach->state;
            
		}else{
			linha1 = "Machine Offline unable to reboot";
		}
		
		sendto(sock,linha1,strlen(linha1),0,(struct sockaddr *)&client,adl);
        memset(linha, 0, sizeof linha);  
        time_t current_time = 0;
        current_time = clock();
        for ( ; (clock() - current_time) < (20 * CLOCKS_PER_SEC); );
        printf("\nRebooted \n");
        strncpy(mach->state, "ONLINE", 10);
	}
    
    pthread_exit((void*)NULL);
    
    close(sock);
    exit(0);
}

void* thread_func4(void *arg){
	
    Machine *mach = (Machine*) arg;
    
    int err, sock, res;
    unsigned long f, i, n, num;
    unsigned char bt;
    char linha[BUF_SIZE];
    struct addrinfo req, *list;
    
    struct sockaddr_storage serv;
    
    unsigned int adl;
    
    bzero((char *)&req,sizeof(req));// let getaddrinfo set the family depending on the supplied server address
    
    req.ai_family = AF_UNSPEC;
    req.ai_socktype = SOCK_STREAM;
    
    err=getaddrinfo(mach->ipAddressName, SERVER_ALT2, &req, &list);
    if(err) {  //COLOCAR AQUI IP DO SERVIDOR
        printf("Failed to get server address, error: %s\n",gai_strerror(err)); 
        exit(1); 
    }
    
    sock=socket(list->ai_family,list->ai_socktype,list->ai_protocol);
    
    if(sock==-1){
        perror("Failed to open SCM socket"); 
        freeaddrinfo(list);
        close(sock);
        pthread_exit((void*)NULL);
    }
    
    if(connect(sock,(struct sockaddr *)list->ai_addr, list->ai_addrlen)==-1){
        perror("Failed connect with Gestor de Chão de Fábrica"); 
        freeaddrinfo(list); 
        close(sock);
        pthread_exit((void*)NULL);
    }  
    
    char * line = (char*)malloc(60 * sizeof(char));
    size_t len = 60;

    
    strncpy(mach->state, "RUNNING", 10);
	
    int nBytes;
    
    nBytes =  strlen(mach->machineID);
    uint32_t un = htonl(nBytes);
    write(sock, &un, sizeof(un));
    write(sock,mach->machineID,nBytes);
    nBytes = 0;
    
    adl=sizeof(serv);
    
    printf("\nReceiving config file...\n");
    read(sock,linha,BUF_SIZE);
    
    printf("\Setting config file... %s\n", linha);
    mach->configFile = linha;
    
    printf("\nConfig file %s assigned to machine %s\n", mach->configFile, mach->machineID);
    
    strncpy(mach->state, "ONLINE", 10);

    close(sock);
    pthread_exit((void*)NULL);
}

int main(int argc, char *argv[]){
	char a[10];
	int d;
	pthread_t threads[3];
    Machine mach;
    if(argc!=4){
		printf("\nVolte a introduzir os parametros: \n - IP do Servidor\n - Numero da máquina\n - Cadência (segundos))\n");
		return 1;
	}
    
    mach.ipAddressName = argv[1];
    mach.machineID = argv[2];
    mach.cadence = atoi(argv[3]);
    
    printf("\nIP do Servidor: %s\nNumero da maquina: %s\nCadencia: %d\n\n", argv[1], argv[2], atoi(argv[3]));	
    
    strncpy(mach.state, "ONLINE", 10);
    
    pthread_create(&threads[1], NULL, thread_func2, &mach);
    pthread_create(&threads[2], NULL, thread_func3, &mach);
    pthread_create(&threads[3], NULL, thread_func4, &mach);
    fflush(stdout);
    
    time_t current_time = 0;
    current_time = clock();
    for ( ; (clock() - current_time) < (10 * CLOCKS_PER_SEC); );
    
    
	pthread_create(&threads[0], NULL, thread_func, &mach);
	pthread_join(threads[0], NULL);
	pthread_join(threads[1], NULL);
        pthread_join(threads[2], NULL);
    	pthread_join(threads[3], NULL);
    return 0;
}
