REM set the class path,
REM assumes the build was executed with maven copy-dependencies
SET EFACTORY_CP=lapr4.app.backoffice.console\target\app.backoffice.console-2.0.0.jar;lapr4.app.backoffice.console\target\dependency\*;

REM call the java VM, e.g, 
java -cp %EFACTORY_CP% lapr4.app.backoffice.console.EFactoryBackoffice
