package lapr4.app.bootstrap;

import lapr4.app.common.console.EFactoryBaseApplication; 
import lapr4.factoryusermanagement.application.eventhandlers.NewUserRegisteredFromSignupWatchDog;
import lapr4.factoryusermanagement.domain.events.NewUserRegisteredFromSignupEvent;
import lapr4.factoryusermanagement.domain.events.SignupAcceptedEvent;
import lapr4.infrastructure.bootstrapers.EFactoryBootstrapper;
import lapr4.infrastructure.bootstrapers.demo.EFactoryDemoBootstrapper;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.usermanagement.application.eventhandlers.SignupAcceptedWatchDog;
import lapr4.usermanagement.domain.FactoryPasswordPolicy;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.infrastructure.eventpubsub.EventDispatcher;
import eapli.framework.util.Arrays;
 
@SuppressWarnings("squid:S106")
public final class EFactoryBootstrap extends EFactoryBaseApplication {
    private boolean isToBootstrapDemoData;
    private boolean isToRunSampleE2E;

    /**
     * avoid instantiation of this class.
     */
    private EFactoryBootstrap() {
    }

    public static void main(final String[] args) {

        AuthzRegistry.configure(PersistenceContext.repositories().users(),
                new FactoryPasswordPolicy(),
                new PlainTextEncoder());

        new EFactoryBootstrap().run(args);
    }

    @Override
    protected void doMain(final String[] args) {
        handleArgs(args);

        System.out.println("\n\n------- MASTER DATA -------");
        new EFactoryBootstrapper().execute();

        if (isToBootstrapDemoData) {
            System.out.println("\n\n------- DEMO DATA -------");
            new EFactoryDemoBootstrapper().execute();
        }
        
    }

    private void handleArgs(final String[] args) {
        isToRunSampleE2E = Arrays.contains(args, "-smoke:basic");
        if (isToRunSampleE2E) {
            isToBootstrapDemoData = true;
        } else {
            isToBootstrapDemoData = Arrays.contains(args, "-bootstrap:demo");
        }
    }

    @Override
    protected String appTitle() {
        return "Bootstrapping eFactory data ";
    }

    @Override
    protected String appGoodbye() {
        return "Bootstrap data done.";
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doSetupEventHandlers(final EventDispatcher dispatcher) {
        dispatcher.subscribe(new NewUserRegisteredFromSignupWatchDog(),
                NewUserRegisteredFromSignupEvent.class);
        dispatcher.subscribe(new SignupAcceptedWatchDog(),
                SignupAcceptedEvent.class);
    }
}
