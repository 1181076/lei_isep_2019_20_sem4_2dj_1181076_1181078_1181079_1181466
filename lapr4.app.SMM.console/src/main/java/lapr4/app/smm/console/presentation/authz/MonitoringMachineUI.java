/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.smm.console.presentation.authz;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr4.app.smm.console.controllers.MonitoringMachineController;
import lapr4.factorymachines.domain.Machine;
import lapr4.app.backoffice.console.presentation.authz.MachinePrinter;
import eapli.framework.util.Console;
import lapr4.factorymachines.domain.MachineStatus;

/**
 *
 * @author Leticia
 */
public class MonitoringMachineUI extends AbstractUI {

    static InetAddress targetIP;

    private static final int TIMEOUT = 60; // seconds
    private static final MonitoringMachineController theController = new MonitoringMachineController();

    @Override
    public boolean doShow() {
        try {
            byte[] data = new byte[300];
            String frase;
            targetIP = InetAddress.getByName("255.255.255.255");

            Iterable<Machine> machines = theController.allMachines();

            DatagramSocket sock = new DatagramSocket();
            sock.setBroadcast(true);
            
            DatagramPacket udpPacket = new DatagramPacket(data, data.length, targetIP, 9999 );

            while (true) {
                final SelectWidget<Machine> selector = new SelectWidget<>("Machine:", machines,
                        new MachinePrinter());
                selector.show();
                final Machine ma = selector.selectedElement();
                String a = ma.getNumSerie();
                byte[] buff = a.getBytes();
//                data[0] = (byte) 0;
//                data[1] = (byte) 0;
//                data[2] = buff[0];
//                data[3] = (byte) ((int) buff[1] * 256);
//                data[4] = (byte) 0;
//                data[5] = (byte) 0;

                udpPacket.setData(buff);
                udpPacket.setLength(buff.length);
                sock.send(udpPacket);
                udpPacket.setData(new byte[300]);
                udpPacket.setLength(300);
                sock.receive(udpPacket);
                frase = new String(udpPacket.getData(), 0, udpPacket.getLength());
                System.out.println("\nReceived status: " + frase);
                ma.setStatus(MachineStatus.valueOf(frase));
                theController.updateMachine(ma);

                String b = Console.readLine("write exit to leave, or else press enter");
                if (b.equalsIgnoreCase("exit")) {
                    break;
                }
            }
            sock.close();
        } catch (UnknownHostException ex) {
            Logger.getLogger(MonitoringMachineUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SocketException ex) {
            Logger.getLogger(MonitoringMachineUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MonitoringMachineUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Monitoring Machine";
    }
}
