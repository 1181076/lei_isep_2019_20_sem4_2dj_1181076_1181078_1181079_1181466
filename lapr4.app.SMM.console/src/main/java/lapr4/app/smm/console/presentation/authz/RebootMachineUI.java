/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.smm.console.presentation.authz;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr4.app.backoffice.console.presentation.authz.MachinePrinter;
import lapr4.app.smm.console.controllers.MonitoringMachineController;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.MachineStatus;

/**
 *
 * @author Leticia
 */
public class RebootMachineUI extends AbstractUI {

    static InetAddress targetIP;

    private static final int TIMEOUT = 60; // seconds
    private static final MonitoringMachineController theController = new MonitoringMachineController();

    @Override
    public boolean doShow() {
        try {
            byte[] data = new byte[300];
            String frase;
            targetIP = InetAddress.getByName("255.255.255.255");

            Iterable<Machine> machines = theController.allMachines();

            DatagramSocket sock = new DatagramSocket();
            sock.setBroadcast(true);
            
            DatagramPacket udpPacket = new DatagramPacket(data, data.length, targetIP, 9995 );

            while (true) {
                final SelectWidget<Machine> selector = new SelectWidget<>("Machine:", machines,
                        new MachinePrinter());
                selector.show();
                final Machine ma = selector.selectedElement();
                String a = ma.getNumSerie();
                byte[] buff = a.getBytes();
                udpPacket.setData(buff);
                udpPacket.setLength(buff.length);
                System.out.println("\nRebooting: " + a);
                sock.send(udpPacket);
                udpPacket.setData(new byte[300]);
                udpPacket.setLength(300);
                sock.receive(udpPacket);
                frase = new String(udpPacket.getData(), 0, udpPacket.getLength());
                System.out.println("Result: " + frase);
                String b = Console.readLine("write exit to leave, or else press enter");
                if (b.equalsIgnoreCase("exit")) {
                    break;
                }
            }
            sock.close();
        } catch (UnknownHostException ex) {
            Logger.getLogger(MonitoringMachineUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SocketException ex) {
            Logger.getLogger(MonitoringMachineUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MonitoringMachineUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Reboot Machine";
    }
    
}
