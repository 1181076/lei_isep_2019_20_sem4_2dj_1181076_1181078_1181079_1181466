/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.smm.console.presentation.authz;

import eapli.framework.actions.Action;

/**
 *
 * @author Leticia
 */
public class RebootMachineAction implements Action{

    @Override
    public boolean execute() {
    return new RebootMachineUI().doShow();
    }
}
