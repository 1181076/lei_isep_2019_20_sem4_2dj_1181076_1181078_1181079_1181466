/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.smm.console.controllers;


import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;


/**
 *
 * @author Leticia
 */
public class MonitoringMachineController {
    private final MachineRepository machineRepository = PersistenceContext.repositories().machines();
    
    
    public Iterable<Machine> allMachines() {
        return this.machineRepository.findAll();
    }
    public void updateMachine(Machine m){
        this.machineRepository.save(m);
    }
}
