/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.smm.console;

import eapli.framework.infrastructure.eventpubsub.EventDispatcher;
import lapr4.app.common.console.EFactoryBaseApplication;
import lapr4.app.smm.presentation.MainMenu;

/**
 *
 * @author Leticia
 */
@SuppressWarnings("squid:S106")
public class EFactorySMM extends EFactoryBaseApplication {

    public EFactorySMM() {
    }

    public static void main(String[] args) {
        new EFactorySMM().run(args);
    }

    @Override
    protected void doMain(String[] args) {
        final MainMenu menu = new MainMenu();
        menu.mainLoop();
    }

    @Override
    protected String appTitle() {
        return "eFactory Machine Monitoring Service (SMM)";
    }

    @Override
    protected String appGoodbye() {
        return "eFactory Machine Monitoring Service (SMM)";
    }

    @Override
    protected void doSetupEventHandlers(EventDispatcher dispatcher) {
        
    }
}
