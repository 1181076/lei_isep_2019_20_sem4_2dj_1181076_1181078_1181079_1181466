<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>

    <xsl:template match="/">{
        <xsl:apply-templates select="fabrica"/>
        }
    </xsl:template>

    <xsl:template match="fabrica">
        "fabrica": {
        "xsi:noNamespaceSchemaLocation": "EFactory.xsd",
        <xsl:apply-templates select="deposits"/>,
        <xsl:apply-templates select="rawMaterias"/>,
        <xsl:apply-templates select="rawMateriaCategories"/>,
        <xsl:apply-templates select="products"/>,
        <xsl:apply-templates select="productionSheets"/>,
        <xsl:apply-templates select="productionOrders"/>,
        <xsl:apply-templates select="productlots"/>,
        <xsl:apply-templates select="notifications"/>,
        <xsl:apply-templates select="productionLines"/>,
        <xsl:apply-templates select="machines"/>,
        <xsl:apply-templates select="messages"/>
        }
    </xsl:template>

    <xsl:template match="deposits">
        "<xsl:value-of select="name()"/>" : { "deposit": [<xsl:apply-templates select="deposit"/>]
        }
    </xsl:template>
    
    <xsl:template match="rawMaterias">
        "<xsl:value-of select="name()"/>" : { "rawMateria": [<xsl:apply-templates select="rawMateria"/>]
        }
    </xsl:template>
        
    <xsl:template match="rawMateriaCategories">
        "<xsl:value-of select="name()"/>" : { "rawMateriaCategory": [<xsl:apply-templates select="rawMateriaCategory"/>]
        }
    </xsl:template>
    
    <xsl:template match="products">
        "<xsl:value-of select="name()"/>" : { "product": [<xsl:apply-templates select="product"/>]
        }
    </xsl:template>
    
    <xsl:template match="productionSheets">
        "<xsl:value-of select="name()"/>" : { "productionSheet": [<xsl:apply-templates select="productionSheet"/>]
        }
    </xsl:template>
    
    <xsl:template match="productionOrders">
        "<xsl:value-of select="name()"/>" : { "productionOrder": [<xsl:apply-templates select="productionOrder"/>]
        }
    </xsl:template>
    
    <xsl:template match="productlots">
        "<xsl:value-of select="name()"/>" : { "productlot": [<xsl:apply-templates select="productlot"/>]
        }
    </xsl:template>
        
    <xsl:template match="notifications">
        "<xsl:value-of select="name()"/>" : { "notification": [<xsl:apply-templates select="notification"/>]
        }
    </xsl:template>
    
    <xsl:template match="productionLines">
        "<xsl:value-of select="name()"/>" : { "productionLine": [<xsl:apply-templates select="productionLine"/>]
        }
    </xsl:template>
    
    <xsl:template match="machines">
        "<xsl:value-of select="name()"/>" : { "machine": [<xsl:apply-templates select="machine"/>]
        }
    </xsl:template>
    
    <xsl:template match="messages">
        "<xsl:value-of select="name()"/>" : { "message": [<xsl:apply-templates select="message"/>]
        }
    </xsl:template>
        
    <xsl:template match="product_amount">{ 
        "product": <xsl:apply-templates select="product"/>
        "amount": "<xsl:value-of select="amount"/>"
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="material_amount">{ 
        "product": <xsl:apply-templates select="product"/>
        "amount": "<xsl:value-of select="amount"/>"
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
              
    <xsl:template match="deposit">
        {
        "idDeposit": "<xsl:value-of select="@idDeposit"/>",
        "type": "<xsl:value-of select="type"/>",
        "desc": "<xsl:value-of select="desc"/>",
        "products": { 
        "product_amount" : [<xsl:apply-templates select="products/product_amount"/>]}
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="rawMateria">
        {
        "idRawMateria": "<xsl:value-of select="@idRawMateria"/>",
        "description": { "rawMateriaDescription": "<xsl:value-of select="description/rawMateriaDescription"/>"},
        "category": { "rawMateriaCategory": <xsl:apply-templates select="category/rawMateriaCategory"/>},
        "internalCode": { "code": "<xsl:value-of select="internalCode/code"/>"},
        "technicalSheet": { "sheet": "<xsl:value-of select="technicalSheet/sheet"/>"}
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="rawMateriaCategory">
        {
        "idRawMateriaCategory": "<xsl:value-of select="@idRawMateriaCategory"/>",
        "categoryName": "<xsl:value-of select="categoryName"/>"
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>

    <xsl:template match="product">
        {
        "idProduct": "<xsl:value-of select="@idProduct"/>",
        "manufactureCode": { "code": "<xsl:value-of select="manufactureCode/code"/>"},
        "commercialCode": { "code": "<xsl:value-of select="commercialCode/code"/>"},
        "briefDescription": { "description": "<xsl:value-of select="briefDescription/description"/>"},
        "completeDescription": { "description": "<xsl:value-of select="completeDescription/description"/>"},
        "rawMateria": <xsl:apply-templates select="rawMateria"/>
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="productionSheet">
        {
        "idProductionSheet": "<xsl:value-of select="@idProductionSheet"/>",
        "materials": { 
        "material_amount" : [<xsl:apply-templates select="materials/material_amount"/>]}
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="productionOrder">
        {
        "idProductionOrder": "<xsl:value-of select="@idProductionOrder"/>",
        "product": <xsl:apply-templates select="product"/>
        "amount": "<xsl:value-of select="amount"/>",
        "stockMovement": { "usedMateria": "<xsl:value-of select="stockMovement/usedMateria"/>", 
        "newProduct": "<xsl:value-of select="stockMovement/newProduct"/>"},
        "deviation": { "expectedMateria": "<xsl:value-of select="deviation/expectedMateria"/>", 
        "expectedProduct": "<xsl:value-of select="deviation/expectedProduct"/>", 
        "realMateria": "<xsl:value-of select="deviation/realMateria"/>", 
        "realProduct": "<xsl:value-of select="deviation/realProduct"/>"},
        "orderEmissionDate": "<xsl:value-of select="orderEmissionDate"/>",
        "previewFinalDate": "<xsl:value-of select="previewFinalDate"/>",
        "orderStatus": "<xsl:value-of select="orderStatus"/>",
        "idEncomenda": "<xsl:value-of select="idEncomenda"/>",
        "notifications": {
        "notification": [<xsl:apply-templates select="notification"/>]},
        "productlots": {
        "productlot": [<xsl:apply-templates select="productlots/productlot"/>]}}
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="productlot">
        {
        "idlot": "<xsl:value-of select="@idlot"/>",
        "products": { 
        "product_amount" : [<xsl:apply-templates select="products/product_amount"/>]}
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="notification">
        {
        "idNotification": "<xsl:value-of select="@idNotification"/>",
        "type": "<xsl:value-of select="type"/>",
        "notificationDescription": { "description": "<xsl:value-of select="notificationDescription/description"/>"}
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="productionLine">
        {
        "idProductionLine": "<xsl:value-of select="@idProductionLine"/>",
        "code": "<xsl:value-of select="code"/>",
        "availability": "<xsl:value-of select="availability"/>",
        "processable": "<xsl:value-of select="processable"/>",
        "machines": { 
        "machine" : [<xsl:apply-templates select="machines/machine"/>]},
        "messages": { 
        "message" : [<xsl:apply-templates select="messages/message"/>]}
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="machine">
        {
        "idMachine": "<xsl:value-of select="@idMachine"/>",
        "numSerie": "<xsl:value-of select="@numSerie"/>",
        "protocol": { "machineProtocol": "<xsl:value-of select="protocol/machineProtocol"/>"},
        "description": { "machineDescription": "<xsl:value-of select="description/machineDescription"/>"},
        "installationDate": "<xsl:value-of select="installationDate"/>",
        "brand": { "name": "<xsl:value-of select="brand/name"/>"},
        "model": { "name": "<xsl:value-of select="model/name"/>"},
        "manufacturer": { "name": "<xsl:value-of select="manufacturer/name"/>"},
        "status": "<xsl:value-of select="status"/>"
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="message">
        {
        "idMessage": "<xsl:value-of select="@idMessage"/>",
        "description": { "description": "<xsl:value-of select="description/description"/>"},
        "messageType": "<xsl:value-of select="messageType"/>",
        "date": "<xsl:value-of select="date"/>",
        "processed": "<xsl:value-of select="processed"/>"
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
</xsl:stylesheet>