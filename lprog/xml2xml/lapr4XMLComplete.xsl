<?xml version="1.0" encoding="UTF-16" ?>

<!--
    Document   : lapr4.xsl
    Created on : 1 de junho de 2020, 18:57
    Author     : migue
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    
    
    
    
    <xsl:template match="fabrica">
        <html>
            <body>
                
                <h2>Deposits</h2>
                <xsl:apply-templates select="deposits">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
                <h2>Raw Materias</h2>
                <xsl:apply-templates select="rawMaterias">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                    
                <h2>Raw Materia Categories</h2>
                <xsl:apply-templates select="rawMateriaCategories">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
                <h2>Products</h2>
                <xsl:apply-templates select="products">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                    
                <h2>Production Sheets</h2>
                <xsl:apply-templates select="productionSheets">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
                <h2>Production Orders</h2>
                <xsl:apply-templates select="productionOrders">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                    
                <h2>Product Lots</h2>
                <xsl:apply-templates select="productlots">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
                <h2>Notifications</h2>
                <xsl:apply-templates select="notifications">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                    
                <h2>Production Lines</h2>
                <xsl:apply-templates select="productionLines">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
                <h2>Machines</h2>
                <xsl:apply-templates select="machines">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                    
                <h2>Messages</h2>
                <xsl:apply-templates select="messages">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
            </body>
            
        </html>
    </xsl:template>
    
    <xsl:template match="deposits">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>ID</th>
                <th>Type</th>
                <th>Description</th>
            </tr>
            <xsl:for-each select="deposit">
                <tr>
                    <td>
                        <xsl:element name ="ID">
                            <xsl:value-of select="@idDeposit"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Type">
                            <xsl:value-of select="type"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Description">
                            <xsl:value-of select="desc"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
        <h2>Deposit Products</h2>
        
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Deposit ID</th>
                <th>Product ID</th>
                <th>Amount</th>
            </tr>
                    
            <xsl:for-each select="deposit">
                   
                <xsl:variable name="pl" select="@idDeposit" />
                <xsl:for-each select="products/product_amount">
                    <tr>
                        <td>
                            <xsl:element name ="depositId">
                                <xsl:value-of select="$pl"/>
                            </xsl:element>
                        </td>
                        <td>
                            <xsl:element name ="productID">
                                <xsl:value-of select="product/@idProduct"/>
                            </xsl:element>
                        </td>
                        <td>
                            <xsl:element name ="amount">
                                <xsl:value-of select="amount"/>
                            </xsl:element>
                        </td>
                    </tr>   
                </xsl:for-each>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template match="rawMaterias">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>ID </th>
                <th>Description</th>
                <th>Category</th>
                <th>Internal Code</th>
                <th>Technical Sheet</th>
            </tr>
            <xsl:for-each select="rawMateria">
                <tr>
                    <td>
                        <xsl:element name ="ID">
                            <xsl:value-of select="@idRawMateria"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Description">
                            <xsl:value-of select="description/rawMateriaDescription"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Category">
                            <xsl:value-of select="category/rawMateriaCategory/categoryName"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Internal Code">
                            <xsl:value-of select="internalCode/code"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Technical Sheet">
                            <xsl:value-of select="technicalSheet/sheet"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template match="categoryName">
        <span style="color:#ff0000">
            <xsl:value-of select="."/>
        </span>
        <br />
    </xsl:template>
    
    
    <xsl:template match="rawMateriaCategories">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>ID </th>
                <th>Name</th>
            </tr>
            <xsl:for-each select="rawMateriaCategory">
                <tr>
                
                    <td>
                        <xsl:value-of select="@idRawMateriaCategory"/>
                    </td>
                    <td>
                        <xsl:value-of select="categoryName"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template match="categoryName">
        <span style="color:#ff0000">
            <xsl:value-of select="."/>
        </span>
        <br />
    </xsl:template>
    
    
    <xsl:template match="messages">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>ID</th>
                <th>Description</th>
                <th>Message Type</th>
                <th>Date</th>
                <th>Processed</th>
            </tr>
            <xsl:for-each select="message">
                <tr>
                    <td>
                        <xsl:element name ="ID">
                            <xsl:value-of select="@idMessage"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Description">
                            <xsl:value-of select="description"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Message Type">
                            <xsl:value-of select="messageType"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Date">
                            <xsl:value-of select="date"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Processed">
                            <xsl:value-of select="processed"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>

    <xsl:template match="products">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Product ID</th>
                <th>Manufacture Code</th>
                <th>Commercial Code</th>
                <th>Brief Description</th>
                <th>Complete Description</th>
                <th>Raw Materia</th>
            </tr>
            <xsl:for-each select="product">
                <tr>
                    <td>
                        <xsl:element name ="Product ID">
                            <xsl:value-of select="@idProduct"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Manufacture Code">
                            <xsl:value-of select="manufactureCode/code"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Commercial Code">
                            <xsl:value-of select="commercialCode/code"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Brief Description">
                            <xsl:value-of select="briefDescription/description"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Complete Description">
                            <xsl:value-of select="completeDescription/description"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Raw Materia">
                            <xsl:value-of select="rawMateria/@idRawMateria"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template match="productionSheets">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>ID</th>
            </tr>
            <xsl:for-each select="productionSheet">
                <tr>
                    <td>
                        <xsl:element name =" psID">
                            <xsl:value-of select="@idProductionSheet"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
        
        <h2>Production Sheet Materials</h2>
                
        
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Production Sheet ID</th>
                <th>Product ID</th>
                <th>Amount</th>
            </tr>
                    
            <xsl:for-each select="productionSheet">
                   
                <xsl:variable name="ps" select="@idProductionSheet" />
                <xsl:for-each select="materials/material_amount">
                    <tr>
                        <td>
                            <xsl:element name =" productSheetID">
                                <xsl:value-of select="$ps"/>
                            </xsl:element>
                        </td>
                        <td>
                            <xsl:element name =" product_inSheetID">
                                <xsl:value-of select="product/@idProduct"/>
                            </xsl:element>
                        </td>
                        <td>
                            <xsl:element name =" Amount">
                                <xsl:value-of select="amount"/>
                            </xsl:element>
                        </td>     
                    </tr>   
                </xsl:for-each>
            </xsl:for-each>
                            
        </table>
    </xsl:template>
    
    <xsl:template match="machines">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Machine ID</th>
                <th>Número de Série</th>
                <th>Protocolo da máquina</th>
                <th>Descrição da máquina</th>
                <th>Data de instalação</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Manufacturador</th>
                <th>Estado</th>
            </tr>
            <xsl:for-each select="machine">
                <tr>
                    <td>
                        <xsl:element name ="Machine ID">
                            <xsl:value-of select="@idMachine"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Número de série">
                            <xsl:value-of select="numSerie"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Protocolo da máquina">
                            <xsl:value-of select="protocol/machineProtocol"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Descrição da máquina">
                            <xsl:value-of select="description/machineDescription"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Data de instalação">
                            <xsl:value-of select="installationDate"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Marca">
                            <xsl:value-of select="brand/name"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Modelo">
                            <xsl:value-of select="model/name"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Manufacturador">
                            <xsl:value-of select="manufacturer/name"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Estado">
                            <xsl:value-of select="status"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template match="notifications">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Notification ID</th>
                <th>Notification Type</th>
                <th>Notification Description</th>
            </tr>
            <xsl:for-each select="notification">
                <tr>
                    <td>
                        <xsl:element name ="Notification ID">
                            <xsl:value-of select="@idNotification"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Notification Type">
                            <xsl:value-of select="type"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Notification Description">
                            <xsl:value-of select="notificationDescription/description"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template match="productionLines">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Production Line ID</th>
                <th>Code</th>
                <th>Availability</th>
                <th>Processing Status</th>
                <th>Date Last Processed</th>
            </tr>
            <xsl:for-each select="productionLine">
                <tr>
                    <td>
                        <xsl:element name ="Production Line ID">
                            <xsl:value-of select="@idProductionLine"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Code">
                            <xsl:value-of select="code"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Availability">
                            <xsl:value-of select="availability"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Processing Status">
                            <xsl:value-of select="processable"/>
                        </xsl:element>
                    </td>
                    
                    <xsl:variable name="defaultText" select="'no date yet'" />
                    
                    <td>
                        <xsl:choose>
                            <xsl:when test="lastprocessed != ''">
                                <xsl:value-of select="lastprocessed" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$defaultText" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
                
        <h2>Production Line Machines</h2>
        
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Production Line ID</th>
                <th>Machine ID</th>
            </tr>
                    
            <xsl:for-each select="productionLine">
                   
                <xsl:variable name="pl" select="@idProductionLine" />
                <xsl:for-each select="machines/machine">
                    <tr>
                        <td>
                            <xsl:element name =" productLineID">
                                <xsl:value-of select="$pl"/>
                            </xsl:element>
                        </td>
                        <td>
                            <xsl:element name =" machID">
                                <xsl:value-of select="@idMachine"/>
                            </xsl:element>
                        </td>
                    </tr>   
                </xsl:for-each>
            </xsl:for-each>
                            
        </table>
        
        
        
        <h2>Production Line Messages</h2>
        
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Production Line ID</th>
                <th>Message ID</th>
            </tr>
                    
            <xsl:for-each select="productionLine">
                   
                <xsl:variable name="pl" select="@idProductionLine" />
                <xsl:for-each select="messages/message">
                    <tr>
                        <td>
                            <xsl:element name =" productLineID">
                                <xsl:value-of select="$pl"/>
                            </xsl:element>
                        </td>
                        <td>
                            <xsl:element name =" messID">
                                <xsl:value-of select="@idMessage"/>
                            </xsl:element>
                        </td>
                    </tr>   
                </xsl:for-each>
            </xsl:for-each>
                            
        </table>
        
    </xsl:template>
    
    <xsl:template match="productionOrders">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Production Order ID</th>
                <th>Product ID</th>
                <th>Amount</th>
                <th>Used Materia</th>
                <th>New Product</th>
                <th>Expected Materia</th>
                <th>Expected Product</th>
                <th>Real Materia</th>
                <th>Real Product</th>
                <th>Order Emission Date</th>
                <th>Preview Final Date</th>
                <th>Order Status</th>
                <th>ID Encomenda</th>
            </tr>
            <xsl:for-each select="productionOrder">
                <tr>
                    <td>
                        <xsl:element name ="Production Order ID">
                            <xsl:value-of select="@idProductionOrder"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Product ID">
                            <xsl:value-of select="product/@idProduct"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Amount">
                            <xsl:value-of select="amount"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Used Materia">
                            <xsl:value-of select="stockMovement/usedMateria"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="New Product">
                            <xsl:value-of select="stockMovement/newProduct"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Expected Materia">
                            <xsl:value-of select="deviation/expectedMateria"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Expected Product">
                            <xsl:value-of select="deviation/expectedProduct"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Real Materia">
                            <xsl:value-of select="deviation/realMateria"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Real Product">
                            <xsl:value-of select="deviation/realProduct"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Order Emission Date">
                            <xsl:value-of select="orderEmissionDate"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Preview Final Date">
                            <xsl:value-of select="previewFinalDate"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Order Status">
                            <xsl:value-of select="orderStatus"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="ID Encomenda">
                            <xsl:value-of select="idEncomenda"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
                
        <h2>Production Order Notifications</h2>
        
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Production Order ID</th>
                <th>Notification ID</th>
            </tr>
                    
            <xsl:for-each select="productionOrder">
                   
                <xsl:variable name="pl" select="@idProductionOrder" />
                <xsl:for-each select="notifications/notification">
                    <tr>
                        <td>
                            <xsl:element name ="productOrderID">
                                <xsl:value-of select="$pl"/>
                            </xsl:element>
                        </td>
                        <td>
                            <xsl:element name =" notifID">
                                <xsl:value-of select="@idNotification"/>
                            </xsl:element>
                        </td>
                    </tr>   
                </xsl:for-each>
            </xsl:for-each>
                            
        </table>
        
        <h2>Production Order Product Lots</h2>
        
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Production Order ID</th>
                <th>Product Lot ID</th>
            </tr>
                    
            <xsl:for-each select="productionOrder">
                   
                <xsl:variable name="pl" select="@idProductionOrder" />
                <xsl:for-each select="productlots/productlot">
                    <tr>
                        <td>
                            <xsl:element name ="productOrderID">
                                <xsl:value-of select="$pl"/>
                            </xsl:element>
                        </td>
                        <td>
                            <xsl:element name ="proLotsID">
                                <xsl:value-of select="@idlot"/>
                            </xsl:element>
                        </td>
                    </tr>   
                </xsl:for-each>
            </xsl:for-each>
                            
        </table>
        
        
    </xsl:template>
    
    <xsl:template match="productlots">
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Product Lot ID</th>
            </tr>
            <xsl:for-each select="productlot">
                <tr>
                    <td>
                        <xsl:element name ="Product Lot ID">
                            <xsl:value-of select="@idlot"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
                
        <h2>Product Lot Amount</h2>
        
        <table border="1">
            <tr bgcolor="#9acd32">
                <th>Production Lot ID</th>
                <th>Amount</th>
            </tr>
                    
            <xsl:for-each select="productlot">
                   
                <xsl:variable name="pl" select="@idlot" />
                <xsl:for-each select="products/product_amount">
                    <tr>
                        <td>
                            <xsl:element name ="productlotID">
                                <xsl:value-of select="$pl"/>
                            </xsl:element>
                        </td>
                        <td>
                            <xsl:element name ="prodId">
                                <xsl:value-of select="product/@idProduct"/>
                            </xsl:element>
                        </td>
                        <td>
                            <xsl:element name ="amount">
                                <xsl:value-of select="amount"/>
                            </xsl:element>
                        </td>
                    </tr>   
                </xsl:for-each>
            </xsl:for-each>
                            
        </table>
        
        
    </xsl:template>
    
    
</xsl:stylesheet>



            
