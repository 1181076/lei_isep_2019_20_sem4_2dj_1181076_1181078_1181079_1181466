<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : relatorio.xsl
    Created on : 8 de junho de 2020, 17:54
    Author     : vrmpe
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:ns="http://www.dei.isep.ipp.pt/lprog">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    
    <xsl:template match="/">
        <html>
            <body>
                <xsl:apply-templates select="ns:relatório">
                    <xsl:sort />
                </xsl:apply-templates>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="ns:relatório">
        <xsl:apply-templates select="ns:páginaRosto">
            <xsl:sort />
        </xsl:apply-templates>
        <h2>Indice</h2>    
        <br/>
        <br/> 
        <strong> Introdução ---------------------------------------------------------------------- 3</strong>
        <br/>
        <br/>
        <strong> Análise  --------------------------------------------------------------------------- 3</strong>
        <br/>
        <br/>
        <strong> Linguagem ---------------------------------------------------------------------- 4</strong>
        <br/>
        <br/>
        <strong> Transformações ---------------------------------------------------------------- 5</strong>
        <br/>
        <br/>
        <strong> Conclusão ----------------------------------------------------------------------- 6</strong>
        <br/>
        <br/>
        <strong> Referências --------------------------------------------------------------------- 7</strong>
        <br/>
        <br/>
        <strong> Anexos --------------------------------------------------------------------------- 8</strong>
        <br/>
        <br/> <br/>
        <br/> <br/>
        <br/> <br/>
        <br/> <br/>
        <br/> 
        <br/> <br/>
        <br/> <br/>
        <br/> <br/>
        <br/> <br/>
        <br/> 
        <br/> <br/>
        <br/> <br/>
        <br/> <br/>
        <br/> <br/>
        <br/> 
        <br/> <br/>
        <br/> 
        <xsl:apply-templates select="ns:corpo">
            <xsl:sort />
        </xsl:apply-templates>
        <xsl:apply-templates select="ns:anexos">
            <xsl:sort />
        </xsl:apply-templates>
    </xsl:template>
    
    
    
    <xsl:template match="ns:páginaRosto">
        <h1>Relatório</h1>  
        <h2>Tema</h2>
        <xsl:element name ="Tema">
            <xsl:value-of select="ns:tema"/>
        </xsl:element>
        <br/>
        <br/>
        <h2>Disciplina</h2>    
        <xsl:element name ="Designação">
            <strong>Designação</strong> - <xsl:value-of select="ns:disciplina/ns:designação"/>
        </xsl:element>
        <br/>   
        <xsl:element name ="Ano">
            <strong>Ano</strong> - <xsl:value-of select="ns:disciplina/ns:anoCurricular"/>
        </xsl:element>
        <br/>   
        <xsl:element name ="Sigla">
            <strong>Sigla</strong> - <xsl:value-of select="ns:disciplina/ns:sigla"/>
        </xsl:element>
            
        <br/>
        <br/>
        
        <h2>Autores</h2>
        <table border="1">
            <tr bgcolor="#FF7400">
                <th>Nome</th>
                <th>Número</th>
                <th>Mail</th>
            </tr>   
            <xsl:for-each select="ns:autor">                 
                <tr>
                    <td>      
                        <xsl:element name ="Nome">
                            <xsl:value-of select="ns:nome"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Número">
                            <xsl:value-of select="ns:número"/>
                        </xsl:element>
                    </td>
                    <td> 
                        <xsl:element name ="Mail">
                            <xsl:value-of select="ns:mail"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
        
        <br/>
        <br/>
        
        <h2>Professores</h2>   
        <table border="1">
            <tr bgcolor="#FF7400">
                <th>Sigla</th>
                <th>Tipo de Aula</th>
            </tr>   
            <xsl:for-each select="ns:professor">               
                <tr>
                    <td>      
                        <xsl:element name ="Sigla">
                            <xsl:value-of select="@sigla"/>
                        </xsl:element>
                    </td>
                    <td>
                        <xsl:element name ="Tipo de Aula">
                            <xsl:value-of select="@tipoAula"/>
                        </xsl:element>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
       
        <br/>  
        <br/>   
        <br/>    
        
        <xsl:variable name="url" select="ns:logotipoDEI" />
        <img src="{$url}"/>
        
        <br/>  
        <br/>   
        <br/>    
        <br/>
        <br/>
        
        <xsl:element name ="Data">
            <strong>Data</strong> - <xsl:value-of select="ns:data"/>
        </xsl:element>
        
        <br/>  
        <br/>   
        <br/>    
        <br/>  
        <br/>   
        <br/>    
        
    </xsl:template>
   
    <xsl:template match="ns:corpo">
        <h2>Introdução</h2>
        <xsl:element name ="Titulo de Secção">
            <strong>Titulo de Secção</strong> - <xsl:value-of select="ns:introdução/@tituloSecção"/>
        </xsl:element>
        <br/>        
        <xsl:element name ="ID de Secção">
            <strong>ID de Secção</strong> - <xsl:value-of select="ns:introdução/@id"/>
        </xsl:element>
        
        <br/>
        <br/>
        <br/>
        <br/>
        <xsl:element name ="ID Subsecção">
            <h3><xsl:value-of select="ns:introdução/ns:subsecção/@id"/></h3>
        </xsl:element>          
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:introdução/ns:subsecção"/>
        </xsl:element>          
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:introdução/ns:parágrafo"/>
        </xsl:element>       
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:introdução/ns:listaItems"/>
        </xsl:element>        
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:introdução/ns:figura"/>
        </xsl:element>
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:introdução/ns:codigo"/>
        </xsl:element>
        <br/>
        <br/>
        <br/>
        <br/>
        <h2>Outras Secções</h2>
        <h3>Análise</h3>
        <xsl:element name ="Titulo de Secção">
            <strong>Titulo de Secção</strong> - <xsl:value-of select="ns:outrasSecções/ns:análise/@tituloSecção"/>
        </xsl:element>
        <br/>        
        <xsl:element name ="ID de Secção">
            <strong>ID de Secção</strong> - <xsl:value-of select="ns:outrasSecções/ns:análise/@id"/>
        </xsl:element>
        <br/>
        <br/>
        <xsl:element name ="ID Subsecção">
            <strong>
                <xsl:value-of select="ns:outrasSecções/ns:análise/ns:subsecção/@id"/>
            </strong>
        </xsl:element>
        <br/>
        <br/>
        <xsl:element name ="Subsecção">
            <xsl:value-of select="ns:outrasSecções/ns:análise/ns:subsecção"/>
        </xsl:element>   
        <br/>
        <br/>       
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:outrasSecções/ns:análise/ns:parágrafo"/>
        </xsl:element>
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:outrasSecções/ns:análise/ns:listaItems"/>
        </xsl:element>
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:outrasSecções/ns:análise/ns:figura"/>
        </xsl:element>
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:outrasSecções/ns:análise/ns:codigo"/>
        </xsl:element>
            
        <br/>
        <br/>
        <h3>Linguagem</h3>
        
        <xsl:element name ="Titulo de Secção">
            <strong>Titulo de Secção</strong> - <xsl:value-of select="ns:outrasSecções/ns:linguagem/@tituloSecção"/>
        </xsl:element>
        <br/>
        <xsl:element name ="ID de Secção">
            <strong>ID de Secção</strong> - <xsl:value-of select="ns:outrasSecções/ns:linguagem/@id"/>
        </xsl:element>
        <br/>
        <br/>
        <xsl:element name ="ID Subsecção">
            <strong>
                <xsl:value-of select="ns:outrasSecções/ns:linguagem/ns:subsecção/@id"/>
            </strong>
        </xsl:element>
        <br/>
        <br/> 
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:outrasSecções/ns:linguagem/ns:parágrafo"/>
        </xsl:element>
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:outrasSecções/ns:linguagem/ns:listaItems"/>
        </xsl:element> 
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:outrasSecções/ns:linguagem/ns:figura"/>
        </xsl:element>       
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:outrasSecções/ns:linguagem/ns:codigo"/>
        </xsl:element>
        <br/>
        <h3>Transformações</h3>
        <xsl:element name ="Titulo de Secção">
            <strong>Titulo de Secção</strong> - <xsl:value-of select="ns:outrasSecções/ns:transformações/@tituloSecção"/>
        </xsl:element>
        <br/>
        <xsl:element name ="ID de Secção">
            <strong>ID de Secção</strong> - <xsl:value-of select="ns:outrasSecções/ns:transformações/@id"/>
        </xsl:element>
        <br/>
        <br/>
        <xsl:element name ="ID Subsecção">
            <strong>
                <xsl:value-of select="ns:outrasSecções/ns:transformações/ns:subsecção/@id"/>
            </strong>
        </xsl:element>
        <br/>
        <br/>
        <xsl:element name ="Subsecção">
            <strong>
                <xsl:value-of select="ns:outrasSecções/ns:transformações/ns:subsecção"/>
            </strong>
        </xsl:element>    
        <xsl:element name ="Texto">
            <br/>
            <br/>
            <xsl:value-of select="ns:outrasSecções/ns:transformações/ns:parágrafo"/>
            <br/>
            <br/>
            <xsl:value-of select="(ns:outrasSecções/ns:transformações/ns:parágrafo)[2]"/>
            <br/>
            <br/>
            <xsl:value-of select="(ns:outrasSecções/ns:transformações/ns:parágrafo)[3]"/>
            <br/>
            <br/>
        </xsl:element>
        <xsl:element name ="Texto">
            <strong>
                <xsl:value-of select="ns:outrasSecções/ns:transformações/ns:codigo"/> 
            </strong>
            <br/>
            <br/>
            <xsl:value-of select="(ns:outrasSecções/ns:transformações/ns:parágrafo)[4]"/>
            <br/>
            <br/>
            <strong>
                <xsl:value-of select="(ns:outrasSecções/ns:transformações/ns:codigo)[2]"/>
            </strong>
            <br/>
            <br/>
        </xsl:element>
        <h2>Conclusão</h2>
        <xsl:element name ="Titulo de Secção">
            <strong>Titulo de Secção</strong> - <xsl:value-of select="ns:conclusão/@tituloSecção"/>
        </xsl:element>
        <br/>
        <xsl:element name ="ID de Secção">
            <strong>ID de Secção</strong> - <xsl:value-of select="ns:conclusão/@id"/>
        </xsl:element>
        
        <xsl:element name ="ID Subsecção">
            <strong>
                <xsl:value-of select="ns:conclusão/ns:subsecção/@id"/>
            </strong>
        </xsl:element>
        <br/>
        <br/>         
        <xsl:element name ="Subsecção">
            <strong>
                <xsl:value-of select="ns:conclusão/ns:subsecção"/>
            </strong>
        </xsl:element>      
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:conclusão/ns:parágrafo"/>
        </xsl:element>
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:conclusão/ns:listaItems"/>
        </xsl:element>
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:conclusão/ns:figura"/>
        </xsl:element>
        <xsl:element name ="Texto">
            <xsl:apply-templates select="ns:conclusão/ns:codigo"/>
        </xsl:element>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/> <br/>
        <br/> 
        <br/> <br/>
        <br/> 
        <br/> <br/>
        <br/> 
        <br/> <br/>
        <br/> 
        <h2>Referências</h2>
        <xsl:element name ="Titulo de Secção">
            <strong>Titulo de Secção</strong> - <xsl:value-of select="ns:referências/@tituloSecção"/>
        </xsl:element>
        <br/>
        <br/>
        <xsl:element name ="ID de Secção">
            <strong>ID de Secção</strong> - <xsl:value-of select="ns:referências/@id"/>
        </xsl:element>
        <br/>
        <br/>
        ---------------
        <br/>
        <br/>
        <xsl:for-each select="ns:referências/ns:refWeb">       
            <xsl:element name ="id">
                <strong>ID de Referência</strong> - <xsl:value-of select="@idRef"/>
            </xsl:element>
            <br/>
            <br/>
            <xsl:element name ="url">
                <strong>URL</strong> - <xsl:value-of select="ns:URL"/>
            </xsl:element>
            <br/>
            <br/>
            <xsl:element name ="descrição">
                <strong>Descrição</strong> - <xsl:value-of select="ns:descrição"/>
            </xsl:element>
            <br/>
            <br/>
            <xsl:element name ="Consulta">
                <strong>Consultado em</strong> - <xsl:value-of select="ns:consultadoEm"/>
            </xsl:element>
            <br/>
            <br/>
            ---------------
            <br/>
            <br/>
        </xsl:for-each>
    </xsl:template>
    
    
    <xsl:template match="ns:anexos">
        <br/> <br/>
        <br/> 
        <br/> <br/>
        <br/> <br/>
        <br/> 
        <br/> 
        <h1>Anexos</h1> 
        <xsl:element name ="Titulo de Secção">
            <strong>Titulo de Secção</strong> - <xsl:value-of select="@tituloSecção"/>
        </xsl:element>
        <br/>
        <xsl:element name ="ID de Secção">
            <strong>ID de Secção</strong> - <xsl:value-of select="@id"/>
        </xsl:element>
        <br/>
        <br/>            
        <strong>ID Subsecção</strong> - <xsl:value-of select="ns:subsecção/@id"/>
        <br/>
        <strong>Subsecção</strong> - <xsl:value-of select="ns:subsecção"/>
        <br/>
        <br/>
        <xsl:value-of select="ns:parágrafo"/>
        <br/>
        <br/>
        <xsl:element name ="Texto">
            <strong>ID Subsecção</strong> - <xsl:value-of select="(ns:subsecção)[2]/@id"/>
            <br/>
            <strong>Subsecção</strong> - <xsl:value-of select="(ns:subsecção)[2]"/> 
            <br/>
            <br/>
            <xsl:value-of select="(ns:parágrafo)[2]"/>
            <br/>
            <br/>
            <strong>ID Subsecção</strong> - <xsl:value-of select="(ns:subsecção)[3]/@id"/>
            <br/>
            <strong>Subsecção</strong> - <xsl:value-of select="(ns:subsecção)[3]"/> 
            <br/>
            <br/>
            <xsl:value-of select="(ns:parágrafo)[3]"/>
            <br/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="ns:introdução/ns:subseccção">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:introdução/ns:parágrafo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:introdução/ns:listaItems">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:introdução/ns:figura">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:introdução/ns:codigo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:análise/ns:parágrafo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:análise/ns:listaItems">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:análise/ns:figura">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:análise/ns:codigo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:linguagem/ns:subsecção">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:linguagem/ns:parágrafo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:linguagem/ns:listaItems">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:linguagem/ns:figura">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:linguagem/ns:codigo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:transformações/ns:parágrafo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:transformações/ns:listaItems">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:transformações/ns:figura">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:outrasSecções/ns:transformações/ns:codigo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    <xsl:template match="ns:conclusão/ns:parágrafo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:conclusão/ns:listaItems">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:conclusão/ns:figura">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:conclusão/ns:codigo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    <xsl:template match="ns:parágrafo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:listaItems">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:figura">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template>
    
    <xsl:template match="ns:codigo">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template> 
    
    <xsl:template match="ns:subsecção">
        <br/>   
        <xsl:value-of select="."/>
        <br/>   
    </xsl:template> 
    
</xsl:stylesheet>
