<?xml version="1.0" encoding="UTF-16" ?>

<!--
    Document   : lapr4.xsl
    Created on : 1 de junho de 2020, 18:57
    Author     : migue
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    
    
    
    
    <xsl:template match="fabrica">
        <html>
            <body>
                
                <h2>Deposits Stats</h2>
                <xsl:apply-templates select="deposits">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
                <h2>Raw Materias Stats</h2>
                <xsl:apply-templates select="rawMaterias">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                    
                <h2>Raw Materia Categories Stats</h2>
                <xsl:apply-templates select="rawMateriaCategories">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
                <h2>Products Stats</h2>
                <xsl:apply-templates select="products">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                    
                <h2>Production Sheets Stats</h2>
                <xsl:apply-templates select="productionSheets">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
                <h2>Production Orders Stats</h2>
                <xsl:apply-templates select="productionOrders">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                    
                <h2>Product Lots Stats</h2>
                <xsl:apply-templates select="productlots">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
                <h2>Notifications Stats</h2>
                <xsl:apply-templates select="notifications">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                    
                <h2>Production Lines Stats</h2>
                <xsl:apply-templates select="productionLines">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
                <h2>Machines Stats</h2>
                <xsl:apply-templates select="machines">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                    
                <h2>Messages Stats</h2>
                <xsl:apply-templates select="messages">
                    <xsl:sort />
                  
                </xsl:apply-templates>
                
            </body>
            
        </html>
    </xsl:template>
    
    <xsl:template match="deposits">
        <table border="1">
            
            <xsl:variable name="numTotal" select="count(deposit)" />
            <xsl:variable name="numTotalEntry" select="count(deposit[type = 'ENTRY'])" />
            <xsl:variable name="numTotalExit" select="count(deposit[type = 'EXIT'])" />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of Deposits    </strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Entry Deposits    </strong>
                </td>
                <td>
                    <xsl:element name ="numEntry">
                        <xsl:value-of select="$numTotalEntry div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Exit Deposits      </strong>
                </td>
                <td>
                    <xsl:element name ="numExit">
                        <xsl:value-of select="$numTotalExit div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    
    <xsl:template match="rawMaterias">
        <table border="1">
            <xsl:variable name="numTotal" select="count(rawMateria)" />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of Raw Materias</strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
        </table>
    </xsl:template>
        
    
    <xsl:template match="rawMateriaCategories">
        <table border="1">
            <xsl:variable name="numTotal" select="count(rawMateriaCategory)" />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of Raw Materia Categories</strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
        </table>
    </xsl:template>

    
    <xsl:template match="messages">
        <table border="1">
             
            <xsl:variable name="numTotal" select="count(message)" />
            <xsl:variable name="numTotalProcessed" select="count(message[processed = 'true'])" />
            <xsl:variable name="numTotalNonProcessed" select="count(message[processed = 'false'])" />
            <xsl:variable name="numTotalConsumo" select="count(message[messageType = 'CONSUMO'])" />
            <xsl:variable name="numTotalEntrega" select="count(message[messageType = 'ENTREGA'])" />
            <xsl:variable name="numTotalProducao" select="count(message[messageType = 'PRODUCAO'])" />
            <xsl:variable name="numTotalEstorno" select="count(message[messageType = 'ESTORNO'])" />
            <xsl:variable name="numTotalInicio" select="count(message[messageType = 'INICIO'])" />
            <xsl:variable name="numTotalRetoma" select="count(message[messageType = 'RETOMA'])" />
            <xsl:variable name="numTotalParagem" select="count(message[messageType = 'PARAGEM'])" />
            <xsl:variable name="numTotalFim" select="count(message[messageType = 'FIM'])" />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of Messages    </strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
            <tr>
                
                <td bgcolor="5F9EA0"> - </td><td> - </td>
                
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Processed Messages    </strong>
                </td>
                <td>
                    <xsl:element name ="numP">
                        <xsl:value-of select="$numTotalProcessed div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Unprocessed Messages      </strong>
                </td>
                <td>
                    <xsl:element name ="numNP">
                        <xsl:value-of select="$numTotalNonProcessed div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                
                <td bgcolor="5F9EA0"> - </td><td> - </td>
                
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Messages of Consumo     </strong>
                </td>
                <td>
                    <xsl:element name ="numCons">
                        <xsl:value-of select="$numTotalConsumo div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Messages of Entrega      </strong>
                </td>
                <td>
                    <xsl:element name ="numEnt">
                        <xsl:value-of select="$numTotalEntrega div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Messages of Producao      </strong>
                </td>
                <td>
                    <xsl:element name ="numProd">
                        <xsl:value-of select="$numTotalProducao div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Messages of Estorno      </strong>
                </td>
                <td>
                    <xsl:element name ="numEst">
                        <xsl:value-of select="$numTotalEstorno div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Messages of Inicio      </strong>
                </td>
                <td>
                    <xsl:element name ="numIn">
                        <xsl:value-of select="$numTotalInicio div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Messages of Retoma      </strong>
                </td>
                <td>
                    <xsl:element name ="numRet">
                        <xsl:value-of select="$numTotalRetoma div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Messages of Paragem      </strong>
                </td>
                <td>
                    <xsl:element name ="numPar">
                        <xsl:value-of select="$numTotalParagem div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Messages of Fim      </strong>
                </td>
                <td>
                    <xsl:element name ="numFim">
                        <xsl:value-of select="$numTotalFim div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
        </table>
    </xsl:template>

    <xsl:template match="products">
        <table border="1">
            <xsl:variable name="numTotal" select="count(product)" />
            <xsl:variable name="numTotalWithSheet" select="count(product/productionSheet)" />
            <xsl:variable name="numTotalWithoutSheet" select=" $numTotal - $numTotalWithSheet " />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of Products    </strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Products with Production Sheet    </strong>
                </td>
                <td>
                    <xsl:element name ="numWithSheet">
                        <xsl:value-of select="$numTotalWithSheet div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Products without Production Sheet      </strong>
                </td>
                <td>
                    <xsl:element name ="numWithoutSheet">
                        <xsl:value-of select="$numTotalWithoutSheet div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <xsl:template match="productionSheets">
        <table border="1">
            <xsl:variable name="numTotal" select="count(productionSheet)" />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of ProductionSheets    </strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
        </table>
        
    </xsl:template>
    
    <xsl:template match="machines">
        <table border="1">
             
            <xsl:variable name="numTotal" select="count(machine)" />
            <xsl:variable name="numTotalWithConfigFile" select="count(machine/configurationFile)" />
            <xsl:variable name="numTotalWithoutConfigFile" select=" $numTotal - $numTotalWithConfigFile" />
            <xsl:variable name="numTotalOnline" select="count(machine[status = 'ONLINE'])" />
            <xsl:variable name="numTotalRunning" select="count(machine[status = 'RUNNING'])" />
            <xsl:variable name="numTotalRebooting" select="count(machine[status = 'REBOOTING'])" />
            <xsl:variable name="numTotalOffline" select="count(machine[status = 'OFFLINE'])" />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of Machines    </strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
            <tr>
                
                <td bgcolor="5F9EA0"> - </td><td> - </td>
                
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Machines with Config File    </strong>
                </td>
                <td>
                    <xsl:element name ="numWithC">
                        <xsl:value-of select="$numTotalWithConfigFile div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Machines without Config File      </strong>
                </td>
                <td>
                    <xsl:element name ="numWithoutConfig">
                        <xsl:value-of select="$numTotalWithoutConfigFile div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                
                <td bgcolor="5F9EA0"> - </td><td> - </td>
                
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Online Machines     </strong>
                </td>
                <td>
                    <xsl:element name ="numCons">
                        <xsl:value-of select="$numTotalOnline div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Running Machines      </strong>
                </td>
                <td>
                    <xsl:element name ="numEnt">
                        <xsl:value-of select="$numTotalRunning div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Rebooting Machines      </strong>
                </td>
                <td>
                    <xsl:element name ="numProd">
                        <xsl:value-of select="$numTotalRebooting div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Offline Machines      </strong>
                </td>
                <td>
                    <xsl:element name ="numEst">
                        <xsl:value-of select="$numTotalOffline div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <xsl:template match="notifications">
        <table border="1">
             
            <xsl:variable name="numTotal" select="count(notification)" />
            <xsl:variable name="numTotalDealt" select="count(notification[type = 'DEALT'])" />
            <xsl:variable name="numTotalTBD" select="count(notification[type = 'TBD'])" />
            <xsl:variable name="numTotalMessageFormat" select="count(notification[errorType = 'MESSAGE_FORMAT'])" />
            <xsl:variable name="numTotalInProduct" select="count(notification[errorType = 'NON_EXISTENT_PRODUCT'])" />
            <xsl:variable name="numTotalInDeposit" select="count(notification[errorType = 'NON_EXISTENT_DEPOSIT'])" />
            <xsl:variable name="numTotalInLot" select="count(notification[errorType = 'NON_EXISTENT_LOT'])" />
            <xsl:variable name="numTotalInsufProduct" select="count(notification[errorType = 'NOT_ENOUGH_PRODUCT'])" />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of Notifications    </strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
            <tr>
                
                <td bgcolor="5F9EA0"> - </td><td> - </td>
                
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Archived Notifications    </strong>
                </td>
                <td>
                    <xsl:element name ="numWithC">
                        <xsl:value-of select="$numTotalDealt div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Unarchived Notifications      </strong>
                </td>
                <td>
                    <xsl:element name ="numWithoutConfig">
                        <xsl:value-of select="$numTotalTBD div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                
                <td bgcolor="5F9EA0"> - </td><td> - </td>
                
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Malformated Message Notification    </strong>
                </td>
                <td>
                    <xsl:element name ="numCons">
                        <xsl:value-of select="$numTotalMessageFormat div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Non-existent Product Notification      </strong>
                </td>
                <td>
                    <xsl:element name ="numEnt">
                        <xsl:value-of select="$numTotalInProduct div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Non-existent Deposit Notification      </strong>
                </td>
                <td>
                    <xsl:element name ="numProd">
                        <xsl:value-of select="$numTotalInDeposit div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Non-existent Lot Notification      </strong>
                </td>
                <td>
                    <xsl:element name ="numEst">
                        <xsl:value-of select="$numTotalInLot div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Not Enough Product Notification      </strong>
                </td>
                <td>
                    <xsl:element name ="numInst">
                        <xsl:value-of select="$numTotalInsufProduct div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <xsl:template match="productionLines">
        <table border="1">
            <xsl:variable name="numTotal" select="count(productionLine)" />
            <xsl:variable name="numTotalAvailable" select="count(productionLine[availability = 'true'])" />
            <xsl:variable name="numTotalUnavailable" select="count(productionLine[availability = 'false'])" />
            <xsl:variable name="numTotalProcessable" select="count(productionLine[processable = 'true'])" />
            <xsl:variable name="numTotalUnprocessable" select="count(productionLine[processable = 'false'])" />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of Production Lines    </strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
            <tr>
                
                <td bgcolor="5F9EA0"> - </td><td> - </td>
                
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Available Production Lines    </strong>
                </td>
                <td>
                    <xsl:element name ="numWithC">
                        <xsl:value-of select="$numTotalAvailable div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Unavailable Production Lines      </strong>
                </td>
                <td>
                    <xsl:element name ="numWithoutConfig">
                        <xsl:value-of select="$numTotalUnavailable div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                
                <td bgcolor="5F9EA0"> - </td><td> - </td>
                
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Processable Production Lines    </strong>
                </td>
                <td>
                    <xsl:element name ="numCons">
                        <xsl:value-of select="$numTotalProcessable div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Unprocessable Production Lines      </strong>
                </td>
                <td>
                    <xsl:element name ="numEnt">
                        <xsl:value-of select="$numTotalUnprocessable div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
        </table>
                
    </xsl:template>
    
    <xsl:template match="productionOrders">
        <table border="1">
            <xsl:variable name="numTotal" select="count(productionOrder)" />
            <xsl:variable name="numTotalAccepted" select="count(productionOrder[orderStatus = 'ACCEPTED'])" />
            <xsl:variable name="numTotalProcessing" select="count(productionOrder[orderStatus = 'PROCESSING'])" />
            <xsl:variable name="numTotalDelivering" select="count(productionOrder[orderStatus = 'DELIVERING'])" />
            <xsl:variable name="numTotalFinished" select="count(productionOrder[orderStatus = 'FINISHED'])" />
            <xsl:variable name="numTotalStopped" select="count(productionOrder[orderStatus = 'STOPPED'])" />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of Production Orders    </strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
            <tr>
                
                <td bgcolor="5F9EA0"> - </td><td> - </td>
                
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Accepted Production Orders    </strong>
                </td>
                <td>
                    <xsl:element name ="numWithC">
                        <xsl:value-of select="$numTotalAccepted div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Processing Production Orders    </strong>
                </td>
                <td>
                    <xsl:element name ="numWithP">
                        <xsl:value-of select="$numTotalProcessing div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Delivering Production Orders    </strong>
                </td>
                <td>
                    <xsl:element name ="numWithD">
                        <xsl:value-of select="$numTotalDelivering div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Finished Production Orders    </strong>
                </td>
                <td>
                    <xsl:element name ="numWithF">
                        <xsl:value-of select="$numTotalFinished div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Stopped Production Orders    </strong>
                </td>
                <td>
                    <xsl:element name ="numWithS">
                        <xsl:value-of select="$numTotalStopped div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
        </table>
              
    </xsl:template>
    
    <xsl:template match="productlots">
        <table border="1">
            <xsl:variable name="numTotal" select="count(productlot)" />
            <xsl:variable name="numTotalNonEmpty" select="count(productlot/products)" />
            <xsl:variable name="numTotalEmpty" select=" $numTotal - $numTotalNonEmpty " />
            
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Number of Product Lots    </strong>
                </td>
                <td>
                    <xsl:element name ="num">
                        <xsl:value-of select="$numTotal"/>
                    </xsl:element>
                </td>
            </tr>
            <tr>
                
                <td bgcolor="5F9EA0"> - </td><td> - </td>
                
            </tr>
            <tr>
                <td bgcolor="5F9EA0">
                    <strong>Empty Product Lots    </strong>
                </td>
                <td>
                    <xsl:element name ="numWithC">
                        <xsl:value-of select="$numTotalEmpty div $numTotal div 0.01"/>%
                    </xsl:element>
                </td>
            </tr>
        </table>
                
    </xsl:template>
    
    
</xsl:stylesheet>



            
