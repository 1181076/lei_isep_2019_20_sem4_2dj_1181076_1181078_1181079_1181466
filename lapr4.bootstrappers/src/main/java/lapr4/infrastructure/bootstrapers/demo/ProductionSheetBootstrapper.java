/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.application.SpecifyProductionSheetForAProductController;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

/**
 *
 * @author migue
 */
public class ProductionSheetBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(MachineBootstrapper.class);

    private final SpecifyProductionSheetForAProductController controller = new SpecifyProductionSheetForAProductController();
    private final ProductionSheetRepository productionLineRepository = PersistenceContext.repositories().productionSheets();
    private final ProductRepository pdRepo = PersistenceContext.repositories().products();

    @Override
    public boolean execute() {

        Iterator<Product> listP = pdRepo.findAll().iterator();

        Product p1 = listP.next();
        Product p2 = listP.next();
        Product p3 = listP.next();
        Product p4 = listP.next();

        int amount = 1;
        int amount2 = 2;
        int amount3 = 3;
        int amount4 = 4;

        Map<Product, Integer> map = new HashMap();
        Map<Product, Integer> map2 = new HashMap();

        map2.put(p3, amount3);
        map2.put(p4, amount4);

        register("ps12", p1, map);
        register("ppp23", p2, map2);

        return true;
    }

    private Optional<ProductionSheet> register(String id, Product p, Map<Product, Integer> amount) {

        ProductionSheet ps = new ProductionSheet(id, amount);
        p = pdRepo.findByID(p.getId()).get();
        p.setProductSheet(ps);
        pdRepo.save(p);
        return Optional.of(controller.specifyProductionSheetForAProduct(ps));

    }
}
