package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.Optional;
import lapr4.productmanagement.application.AddRawMateriaController;
import lapr4.productmanagement.domain.InternalRawMateriaCode;
import lapr4.productmanagement.domain.RawMateria;
import lapr4.productmanagement.domain.RawMateriaCategory;
import lapr4.productmanagement.domain.RawMateriaDescription;
import lapr4.productmanagement.domain.TechnicalSheet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

public class RawMateriaBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(MachineBootstrapper.class);

    private final AddRawMateriaController controller = new AddRawMateriaController();

    @Override
    public boolean execute() {
        RawMateriaCategory cat = controller.RawMateriaCategories().iterator().next();
        RawMateriaDescription desc1 = new RawMateriaDescription("basic raw materia");
        RawMateriaDescription desc2 = new RawMateriaDescription("basic raw materia 2");
        RawMateriaDescription desc3 = new RawMateriaDescription("basic raw materia 3");
        RawMateriaDescription desc4 = new RawMateriaDescription("basic raw materia 4");
        TechnicalSheet sh1 = new TechnicalSheet("materia1");
        TechnicalSheet sh2 = new TechnicalSheet("materia2");
        TechnicalSheet sh3 = new TechnicalSheet("materia3");
        TechnicalSheet sh4 = new TechnicalSheet("materia4");
        InternalRawMateriaCode irmc1 = new InternalRawMateriaCode("m1");
        InternalRawMateriaCode irmc2 = new InternalRawMateriaCode("m2");
        InternalRawMateriaCode irmc3 = new InternalRawMateriaCode("m3");
        InternalRawMateriaCode irmc4 = new InternalRawMateriaCode("m4");

        register(desc1, cat, irmc1, sh1);
        register(desc2, cat, irmc2, sh2);
        register(desc3, cat, irmc3, sh3);
        register(desc4, cat, irmc4, sh4);

        return true;
    }

    private Optional<RawMateria> register(RawMateriaDescription desc, RawMateriaCategory cat, InternalRawMateriaCode code, TechnicalSheet ts) {
        try {
            return Optional.of(controller.addRawMateria(desc, cat, code, ts));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object
            LOGGER.warn("Assuming {} already exists (see trace log for details on {} {})",
                    desc,
                    e.getClass().getSimpleName(), e.getMessage());
            LOGGER.trace(e);
            return Optional.empty();
        }
    }
}
