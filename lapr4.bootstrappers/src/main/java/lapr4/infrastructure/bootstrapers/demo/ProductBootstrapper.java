/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.Iterator;
import java.util.Optional;
import lapr4.productmanagement.application.AddProductController;
import lapr4.productmanagement.application.ListRawMateriaController;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductCommercialCode;
import lapr4.productmanagement.domain.ProductDescriptionBrief;
import lapr4.productmanagement.domain.ProductDescriptionComplete;
import lapr4.productmanagement.domain.ProductManufactureCode;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.RawMateria;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

/**
 *
 * @author mxlsa
 */
public class ProductBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(MachineBootstrapper.class);

    private final ListRawMateriaController controller = new ListRawMateriaController();
    private final AddProductController pController = new AddProductController();

    @Override
    public boolean execute() {
        Iterator<RawMateria> rm = controller.listRawMaterias().iterator();
        RawMateria mat = rm.next();
        RawMateria mat2 = rm.next();
        RawMateria mat3 = rm.next();
        RawMateria mat4 = rm.next();
        Product pd = new Product(new ProductManufactureCode("50000106"), new ProductCommercialCode("CD1"), new ProductDescriptionBrief("Desc"),new ProductDescriptionComplete("Description"), mat);
        Product pd1 = new Product(new ProductManufactureCode("32000142"), new ProductCommercialCode("CD2"), new ProductDescriptionBrief("Desc"),new ProductDescriptionComplete("Description"), mat2);
        Product pd2 = new Product(new ProductManufactureCode("41000651"), new ProductCommercialCode("CD3"), new ProductDescriptionBrief("Desc"),new ProductDescriptionComplete("Description"), mat3);
        Product pd3 = new Product(new ProductManufactureCode("COD4"), new ProductCommercialCode("CD4"), new ProductDescriptionBrief("Desc"),new ProductDescriptionComplete("Description"), mat4);
        
        register(pd);
        register(pd1);
        register(pd2);
        register(pd3);

        return true;
    }

    private Optional<Product> register(Product pd) {
        try {
            return Optional.of(pController.addProduct(pd));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object
            LOGGER.warn("Assuming {} already exists (see trace log for details on {} {})",
                    pd.getId(),
                    e.getClass().getSimpleName(), e.getMessage());
            LOGGER.trace(e);
            return Optional.empty();
        }
    }
}
