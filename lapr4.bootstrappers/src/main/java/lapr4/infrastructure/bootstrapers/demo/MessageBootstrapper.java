/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import lapr4.factorymachines.application.ListProductionLineController;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.domain.MessageDescription;
import lapr4.messagemanagement.domain.MessageType;
import lapr4.messagemanagement.domain.application.AddMessageController;
import lapr4.productmanagement.application.SpecifyProductionSheetForAProductController;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

/**
 *
 * @author migue
 */
public class MessageBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(MachineBootstrapper.class);

    private final AddMessageController controller = new AddMessageController();
    private final ListProductionLineController lController = new ListProductionLineController();

    @Override
    public boolean execute() {
        
        ProductionLine pl = lController.allProductionLines().iterator().next();

        MessageDescription m1 = new MessageDescription("teste1");
        MessageDescription m2 = new MessageDescription("teste2");

        Message mes1 = new Message(m1, MessageType.CONSUMO);
        Message mes2 = new Message(m2, MessageType.ENTREGA);
        
        mes1.setProcessed(true);
        mes2.setProcessed(true);
        
        register(mes1, pl);
        register(mes2, pl);

        return true;
    }

    private Optional<Message> register(Message m, ProductionLine pl) {
        try {
            return Optional.of(controller.addMessage(m, pl));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object

            LOGGER.trace(e);
            return Optional.empty();
        }
    }
}
