/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.Optional;
import lapr4.factorymachines.domain.Protocol;
import lapr4.productmanagement.application.DefineRawMateriaCategoryController;
import lapr4.productmanagement.domain.RawMateriaCategory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

/**
 *
 * @author Leticia
 */
public class RawMateriaCategoryBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(RawMateriaCategoryBootstrapper.class);

    private final DefineRawMateriaCategoryController controller = new DefineRawMateriaCategoryController();

    @Override
    public boolean execute() {
        Protocol p = new Protocol("test");
        String cat1 = "Ferro";
        String cat2 = "Madeira";
        String cat3 = "Algodão";
        String cat4 = "Couro";
        String cat5 = "Cobre";
        String cat6 = "Gesso";
        register(cat1);
        register(cat2);
        register(cat3);
        register(cat4);
        register(cat5);
        register(cat6);

        return true;
    }

    private Optional<RawMateriaCategory> register(String cat) {
        try {
            return Optional.of(controller.defineRawMateriaCategory(cat));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object
            LOGGER.warn("Assuming {} already exists (see trace log for details on {} {})",
                    cat,
                    e.getClass().getSimpleName(), e.getMessage());
            LOGGER.trace(e);
            return Optional.empty();
        }
    }

}
