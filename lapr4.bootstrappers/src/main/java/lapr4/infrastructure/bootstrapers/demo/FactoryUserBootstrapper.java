package lapr4.infrastructure.bootstrapers.demo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lapr4.factoryusermanagement.application.AcceptRefuseSignupFactory;
import lapr4.factoryusermanagement.application.AcceptRefuseSignupRequestController;
import lapr4.factoryusermanagement.application.AcceptRefuseSignupRequestControllerEventfulImpl;
import lapr4.factoryusermanagement.application.AcceptRefuseSignupRequestControllerTxImpl;
import lapr4.factoryusermanagement.domain.SignupRequest;
import lapr4.infrastructure.bootstrapers.TestDataConstants;
import lapr4.myfactoryuser.application.SignupController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;

public class FactoryUserBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(FactoryUserBootstrapper.class);

    private final SignupController signupController = new SignupController();
    private final AcceptRefuseSignupRequestController acceptController = AcceptRefuseSignupFactory
            .build();

    @Override
    public boolean execute() {
        signupAndApprove("user1", "Password1", "John",
                "Smith",
                "john@smith.com",
                "user1");
        signupAndApprove("isep959", "Password1", "Mary", "Smith",
                "mary@smith.com", "isep959");
        return true;
    }

    private SignupRequest signupAndApprove(final String username,
            final String password,
            final String firstName,
            final String lastName,
            final String email,
            final String mecanographicNumber) {
        SignupRequest request = null;
        try {
            request = signupController
                    .signup(username, password, firstName, lastName, email,
                            mecanographicNumber);
            acceptController.acceptSignupRequest(request);
        } catch (final ConcurrencyException | IntegrityViolationException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn(
                    "Assuming {} already exists (activate trace log for details)",
                    username);
            LOGGER.trace("Assuming existing record", e);
        }
        return request;
    }
}
