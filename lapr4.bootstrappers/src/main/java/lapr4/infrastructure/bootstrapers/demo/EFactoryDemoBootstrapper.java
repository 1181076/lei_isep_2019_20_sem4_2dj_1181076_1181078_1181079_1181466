package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.infrastructure.authz.application.AuthenticationService;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.util.Strings;
import eapli.framework.validations.Invariants;

@SuppressWarnings("squid:S106")
public class EFactoryDemoBootstrapper implements Action {

    private static final String POWERUSER_A1 = "poweruserA1";
    private static final String POWERUSER = "poweruser";

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final AuthenticationService authenticationService = AuthzRegistry
            .authenticationService();

    @Override
    public boolean execute() {
        // declare bootstrap actions
        final Action[] actions = {
            new BackofficeUsersBootstrapper(),
            new FactoryUserBootstrapper(),
            new ProductionLineBootstrapper(),
            new MachineBootstrapper(),
            new RawMateriaCategoryBootstrapper(),
            new RawMateriaBootstrapper(),
            new ProductBootstrapper(),
            new DepositBootstrapper(),
            new ProductionOrderBootstrapper(),
            new NotificationBootstrapper(),
            new ProductionSheetBootstrapper(),
            new MessageBootstrapper()
               
        };

        authenticateForBootstrapping();

        // execute all bootstrapping
        boolean ret = true;
        for (final Action boot : actions) {
            System.out.println("Bootstrapping " + nameOfEntity(boot) + "...");
            ret &= boot.execute();
        }
        return ret;
    }

    /**
     * authenticate a super user to be able to register new users
     *
     */
    protected void authenticateForBootstrapping() {
        authenticationService.authenticate(POWERUSER, POWERUSER_A1);
        Invariants.ensure(authz.hasSession());
    }

    private String nameOfEntity(final Action boot) {
        final String name = boot.getClass().getSimpleName();
        return Strings.left(name, name.length() - "Bootstrapper".length());
    }
}
