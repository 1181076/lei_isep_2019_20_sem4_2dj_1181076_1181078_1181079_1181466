/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import lapr4.productionmanagement.domain.Notification;
import lapr4.factorymachines.application.RegisterProductionLineController;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.messagemanagement.domain.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

/**
 *
 * @author migue
 */
public class ProductionLineBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(MachineBootstrapper.class);

    private final RegisterProductionLineController controller = new RegisterProductionLineController();

    @Override
    public boolean execute() {
        Set<Machine> sMac = new HashSet();
        Set<Message> sMes = new HashSet();
        Set<Machine> sMac2 = new HashSet();
        Set<Message> sMes2 = new HashSet();
        Set<Machine> sMac3 = new HashSet();
        Set<Message> sMes3 = new HashSet();
        Set<Machine> sMac4 = new HashSet();
        Set<Message> sMes4 = new HashSet();

        register(1L, sMac, sMes);
        register(2L, sMac2, sMes2);
        register(3L, sMac3, sMes3);
        register(4L, sMac4, sMes4);

        return true;
    }

    private Optional<ProductionLine> register(Long code, Set<Machine> sMac, Set<Message> sMes) {
        try {
            return Optional.of(controller.addProductionLine(code, sMac, sMes));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object
            LOGGER.warn("Assuming {} already exists (see trace log for details on {} {})",
                    code,
                    e.getClass().getSimpleName(), e.getMessage());
            LOGGER.trace(e);
            return Optional.empty();
        }
    }
}
