/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.application.AddNotificationController;
import lapr4.productionmanagement.application.AddProductionOrderController;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.domain.NotificationDescription;
import lapr4.productionmanagement.domain.NotificationType;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.NotificationRepository;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

/**
 *
 * @author migue
 */
public class NotificationBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(MachineBootstrapper.class);

    private final AddNotificationController controller = new AddNotificationController();
    private final ProductionOrderRepository productionOrderRepository = PersistenceContext.repositories().productionOrders();

    @Override
    public boolean execute() {

        Iterator<ProductionOrder> list = productionOrderRepository.findAll().iterator();

        ProductionOrder pl1 = list.next();

        register(new NotificationDescription("Notification1"), NotificationType.TBD, pl1);
        register(new NotificationDescription("Notification2"), NotificationType.DEALT, pl1);
        register(new NotificationDescription("Notification3"), NotificationType.TBD, pl1);
        register(new NotificationDescription("Notification4"), NotificationType.TBD, pl1);
        
        return true;
    }

    private Optional<Notification> register(NotificationDescription desc, NotificationType type, ProductionOrder po) {
        try {
            return Optional.of(controller.addNotification(desc, type, po));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object

            LOGGER.trace(e);
            return Optional.empty();
        }
    }
}
