/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.application.AddProductionOrderController;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductLot;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

/**
 *
 * @author migue
 */
public class ProductionOrderBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(MachineBootstrapper.class);

    private final AddProductionOrderController controller = new AddProductionOrderController();
    private final ProductionLineRepository productionLineRepository = PersistenceContext.repositories().productionLines();
    private final ProductRepository pdRepo = PersistenceContext.repositories().products();

    @Override
    public boolean execute() {

        Iterator<ProductionLine> list = productionLineRepository.findAll().iterator();

        ProductionLine pl1 = list.next();
        ProductionLine pl2 = list.next();

        Iterator<Product> listP = pdRepo.findAll().iterator();

        Product p1 = listP.next();
        Product p2 = listP.next();
        
        Set<ProductLot> s1 = new HashSet();
        Set<ProductLot> s2 = new HashSet();
        
        s1.add(new ProductLot( "DE06191071", new HashMap()));
        s1.add(new ProductLot( "DE06191072", new HashMap()));
        s1.add(new ProductLot( "DE06191073", new HashMap()));
        s1.add(new ProductLot( "DE06191081", new HashMap()));
        s1.add(new ProductLot( "DE06191082", new HashMap()));
        s1.add(new ProductLot( "DE06191083", new HashMap()));
        s1.add(new ProductLot( "DE06191091", new HashMap()));
        s1.add(new ProductLot( "DE06191092", new HashMap()));
        s1.add(new ProductLot( "DE06191093", new HashMap()));
        s1.add(new ProductLot( "DE06191105", new HashMap()));
        s1.add(new ProductLot( "DE06191131", new HashMap()));
        s1.add(new ProductLot( "DE06191132", new HashMap()));
        s1.add(new ProductLot( "DE06191133", new HashMap()));
        s1.add(new ProductLot( "DE06191141", new HashMap()));
        s1.add(new ProductLot( "DE06191142", new HashMap()));
        s1.add(new ProductLot( "DE06191143", new HashMap()));
        
        register(12L, new Date("2020/01/01"), new Date("2021/01/01"), p1, null, "112", OrderStatus.ACCEPTED, new HashSet(), 455, s2);
        register(1234L, Calendar.getInstance().getTime(), new Date("2020/12/01"), p2, null, "162", OrderStatus.FINISHED, new HashSet(), 65, s1);

        return true;
    }

    private Optional<ProductionOrder> register(Long id, Date orderEmissionDate, Date previewFinalDate, Product product, ProductionLine line, String idEncomenda, OrderStatus orderStatus, Set<Notification> sNot, int amount, Set<ProductLot> pls) {
        try {
            return Optional.of(controller.addProductionOrder(id,orderEmissionDate, previewFinalDate, product, line, idEncomenda, orderStatus, sNot, amount, pls));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object
            
            
            LOGGER.trace(e);
            return Optional.empty();
        }
    }
}
