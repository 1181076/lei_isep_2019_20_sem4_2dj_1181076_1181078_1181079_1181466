package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.Date;
import java.util.Optional;
import lapr4.factorymachines.application.DefineNewMachineController;
import lapr4.factorymachines.application.ListProductionLineController;
import lapr4.factorymachines.application.RegisterProductionLineController;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.MachineBrand;
import lapr4.factorymachines.domain.MachineDescript;
import lapr4.factorymachines.domain.MachineModel;
import lapr4.factorymachines.domain.Manufacturer;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.domain.Protocol;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

public class MachineBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(MachineBootstrapper.class);

    private final DefineNewMachineController controller = new DefineNewMachineController();
    private final ListProductionLineController lController = new ListProductionLineController();

    @Override
    public boolean execute() {
        Protocol p = new Protocol("test");
        MachineDescript desc1 = new MachineDescript("BasicMachine1");
        MachineDescript desc2 = new MachineDescript("BasicMachine2");
        MachineDescript desc3 = new MachineDescript("BasicMachine3");
        MachineDescript desc4 = new MachineDescript("BasicMachine4");
        MachineDescript desc5 = new MachineDescript("BasicMachine5");
        Date installationDate1 = new Date(2020, 1, 1);
        Date installationDate2 = new Date(2020, 1, 2);
        Date installationDate3 = new Date(2020, 1, 3);
        Date installationDate4 = new Date(2020, 1, 4);
        Date installationDate5 = new Date(2020, 1, 5);
        MachineBrand brand1 = new MachineBrand("M1");
        MachineBrand brand2 = new MachineBrand("M2");
        MachineBrand brand3 = new MachineBrand("M3");
        MachineBrand brand4 = new MachineBrand("M4");
        MachineBrand brand5 = new MachineBrand("M5");
        MachineModel model1 = new MachineModel("A1");
        MachineModel model2 = new MachineModel("A2");
        MachineModel model3 = new MachineModel("A3");
        MachineModel model4 = new MachineModel("A4");
        MachineModel model5 = new MachineModel("A5");
        Manufacturer man1 = new Manufacturer("Company1");
        Manufacturer man2 = new Manufacturer("Company2");
        Manufacturer man3 = new Manufacturer("Company3");
        Manufacturer man4 = new Manufacturer("Company4");
        Manufacturer man5 = new Manufacturer("Company5");
        String numSerie1 = "T3";
        String numSerie2 = "DD4";
        String numSerie3 = "2";
        String numSerie4 = "3";
        String numSerie5 = "4";
        
        ProductionLine pl = lController.allProductionLines().iterator().next();
        
        register(numSerie1, p, desc1, installationDate1, brand1, model1, man1, pl);
        register(numSerie2, p, desc2, installationDate2, brand2, model2, man2, pl);
        register(numSerie3, p, desc3, installationDate3, brand3, model3, man3, pl);
        register(numSerie4, p, desc4, installationDate4, brand4, model4, man4, pl);
        register(numSerie5, p, desc5, installationDate5, brand5, model5, man5, pl);

        return true;
    }

    private Optional<Machine> register(String numSerie, Protocol prot, MachineDescript desc, Date installationDate, MachineBrand brand, MachineModel model, Manufacturer man, ProductionLine pl) {
        try {
            return Optional.of(controller.addMachine(numSerie, prot, desc, installationDate, brand, model, man, pl));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object
            LOGGER.warn("Assuming {} already exists (see trace log for details on {} {})",
                    desc,
                    e.getClass().getSimpleName(), e.getMessage());
            LOGGER.trace(e);
            return Optional.empty();
        }
    }
}
