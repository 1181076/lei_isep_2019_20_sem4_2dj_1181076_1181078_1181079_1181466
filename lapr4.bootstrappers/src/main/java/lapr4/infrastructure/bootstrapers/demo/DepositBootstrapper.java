/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.infrastructure.bootstrapers.demo;

import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import lapr4.productmanagement.application.AddDepositController;
import lapr4.productmanagement.application.ListProductsController;
import lapr4.productmanagement.domain.Deposit;
import lapr4.productmanagement.domain.DepositType;
import lapr4.productmanagement.domain.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.TransactionSystemException;

/**
 *
 * @author vrmpe
 */
public class DepositBootstrapper implements Action{
    
    private static final Logger LOGGER = LogManager.getLogger(MachineBootstrapper.class);
    private final ListProductsController listProductsController= new ListProductsController();
    private final AddDepositController controller = new AddDepositController();
    
    @Override
    public boolean execute() {
        
        String desc1= "Basic Description1";
        String desc2= "Basic Description2";
        String desc3= "Basic Description3";
        String desc4= "Basic Description4";
        DepositType dp1= DepositType.ENTRY;
        DepositType dp2= DepositType.EXIT;
        
        
        Product pro = listProductsController.allProducts().iterator().next();
        
//        
        Map<Product,Integer> setPro = new HashMap();
        setPro.put(pro, 1);
        Map<Product,Integer> setPro2 = new HashMap();
        setPro2.put(pro, 2);
        Map<Product,Integer> setPro3 = new HashMap();
        setPro3.put(pro, 2);
        Map<Product,Integer> setPro4 = new HashMap();
        setPro4.put(pro, 4);
        
        register("L040", desc1, dp1, setPro);
        register("L042", desc2, dp2, setPro2);
        register("L030", desc3, dp2, setPro3);
        register("depo4", desc4, dp1, setPro4);
        
        
        
        
        return true;
    }
    
    private Optional<Deposit> register(String id, String desc, DepositType type, Map<Product,Integer> prods) {
        try {
            return Optional.of(controller.addDeposit(id, desc, type, prods));
        } catch (final IntegrityViolationException | ConcurrencyException
                | TransactionSystemException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated object
            LOGGER.warn("Assuming {} already exists (see trace log for details on {} {})",
                    desc,
                    e.getClass().getSimpleName(), e.getMessage());
            LOGGER.trace(e);
            return Optional.empty();
        }
    }
}
