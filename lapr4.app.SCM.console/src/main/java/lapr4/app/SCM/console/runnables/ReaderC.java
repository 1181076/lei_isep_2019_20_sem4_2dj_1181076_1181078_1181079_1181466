/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.SCM.console.runnables;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr4.messagemanagement.domain.application.ImportMessageFromFolderController;

/**
 *
 * @author migue
 */
public class ReaderC implements Runnable {

    private ImportMessageFromFolderController objec;

    public ReaderC(ImportMessageFromFolderController obj) {
        this.objec = obj;
    }

    public void run() {

        try {
            objec.read();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(ReaderC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
