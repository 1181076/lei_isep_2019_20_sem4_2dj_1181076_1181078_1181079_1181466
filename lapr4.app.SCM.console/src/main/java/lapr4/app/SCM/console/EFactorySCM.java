package lapr4.app.SCM.console;

import eapli.framework.infrastructure.eventpubsub.EventDispatcher;
import lapr4.app.SCM.console.presentation.MainMenu;
import lapr4.app.common.console.EFactoryBaseApplication;

@SuppressWarnings("squid:S106")
public class EFactorySCM extends EFactoryBaseApplication {

    public EFactorySCM() {
    }

    public static void main(String[] args) {
        new EFactorySCM().run(args);
    }

    @Override
    protected void doMain(String[] args) {
        final MainMenu menu = new MainMenu();
        menu.mainLoop();
    }

    @Override
    protected String appTitle() {
        return "eFactory Machine Communication Service (SCM)";
    }

    @Override
    protected String appGoodbye() {
        return "eFactory Machine Communication Service (SCM)";
    }

    @Override
    protected void doSetupEventHandlers(EventDispatcher dispatcher) {
        
    }

}
