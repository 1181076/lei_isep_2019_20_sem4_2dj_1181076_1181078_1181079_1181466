/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.SCM.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr4.Application;
import lapr4.app.SCM.console.runnables.DeleterC;
import lapr4.app.SCM.console.runnables.ReaderC;
import lapr4.messagemanagement.domain.application.ImportMessageFromFolderController;

/**
 *
 * @author migue
 */
public class ImportMessageFromFileUI extends AbstractUI {

    private final String in = Application.settings().getInputFolderName();
    private final String out = Application.settings().getOutputFolderName();

    @Override
    protected boolean doShow() {
        String conf = Console.readLine("\n\n\n\nPlease move all the files you want used \n"
                + "to the following folder (located in the project folder):\n" + in
                + "\n\nWhen finished moving the files, enter \"1\"");

        while (!conf.equals("1")) {
            conf = Console.readLine("Please enter \"1\" once everything is ready");
        }

        File f = new File(in);
        File[] matchingFiles = f.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith("txt");
            }
        }
        );

        Thread[] takersThreadss = new Thread[matchingFiles.length * 2];
        int j = 0;

        try {
            for (File fff : matchingFiles) {

                System.out.println("\n\nUsing file " + fff.getName() + "\n\n");
                ImportMessageFromFolderController cont = new ImportMessageFromFolderController(fff);
                ReaderC rc = new ReaderC(cont);
                DeleterC dc = new DeleterC(cont);

                takersThreadss[j] = new Thread(rc, "Taker-" + j);
                takersThreadss[j + 1] = new Thread(dc, "Taker-" + j);

                takersThreadss[j].start();
                takersThreadss[j + 1].start();

                takersThreadss[j].join();
                takersThreadss[j + 1].join();
                j++;
                j++;

            }
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Message which already exists in the database.");
        } catch (InterruptedException ex) {
            Logger.getLogger(ImportMessageFromFileUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("All files have been used and messages added to the repository, soon to be processed."
                + "The files used have been moved to a new folder with the name:\n" + out);

        return false;

    }

    @Override
    public String headline() {
        return "Importing Messages From Files";
    }

}
