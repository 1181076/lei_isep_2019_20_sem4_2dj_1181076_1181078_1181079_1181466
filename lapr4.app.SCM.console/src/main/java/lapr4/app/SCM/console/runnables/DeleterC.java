/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.SCM.console.runnables;

import lapr4.messagemanagement.domain.application.ImportMessageFromFolderController;

/**
 *
 * @author migue
 */
public class DeleterC implements Runnable {

    private ImportMessageFromFolderController objec;

    public DeleterC(ImportMessageFromFolderController obj) {
        this.objec = obj;
    }

    public void run() {

        objec.delete();
    }
}
