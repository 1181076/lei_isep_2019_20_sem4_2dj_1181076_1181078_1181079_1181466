package lapr4.app.SCM.console.presentation.authz;

import eapli.framework.actions.Action;

public class ImportMessageFromMachineAction implements Action  {

    @Override
    public boolean execute() {
        return new ImportMessageFromMachineUI().doShow();
    }
}
