/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.SCM.console.presentation.authz;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.domain.application.ImportMessageFromDataController;
import lapr4.messagemanagement.repositories.MessageRepository;

/**
 *
 * @author migue
 */
public class ImportMessageFromMachineUI extends AbstractUI {

    static final String TRUSTED_STORE = "simulador/server_J.jks";
    static final String KEYSTORE_PASS = "forgotten";
    private static HashMap<Socket, DataOutputStream> cliList = new HashMap<>();

    public static synchronized void sendTo(int len, byte[] data, Socket s) throws Exception {
        DataOutputStream cOut = cliList.get(s);
        cOut.write(len);
        cOut.write(data, 0, len);

    }

    public static synchronized void addCli(Socket s) throws Exception {
        cliList.put(s, new DataOutputStream(s.getOutputStream()));
    }

    public static synchronized void remCli(Socket s) throws Exception {
        cliList.get(s).write(0);
        cliList.remove(s);
        s.close();
    }

    private static SSLServerSocket sslsock;
    private static SSLSocket sock;

    @Override
    protected boolean doShow() {
        System.setProperty("javax.net.ssl.trustStore", TRUSTED_STORE);
        System.setProperty("javax.net.ssl.trustStorePassword", KEYSTORE_PASS);

        System.setProperty("javax.net.ssl.keyStore", TRUSTED_STORE);
        System.setProperty("javax.net.ssl.keyStorePassword", KEYSTORE_PASS);

        SSLServerSocketFactory sslF = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();

        try {
            sslsock = (SSLServerSocket) sslF.createServerSocket(9999);
        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + 9999);
            System.exit(1);
        }

        sslsock.setNeedClientAuth(true);

        System.out.println("Connected to port: " + 9999);

        while (true) {
            try {
                System.out.println("Waiting for Machine...");
                sock = (SSLSocket) sslsock.accept();
                System.out.println("Found Machine...");
                addCli(sock);
                Thread cli = new TcpChatSrvClient(sock);
                cli.start();
            } catch (Exception ex) {
                Logger.getLogger(ImportMessageFromMachineUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String headline() {
        return "Importing Messages From Machines";

    }

}

class TcpChatSrvClient extends Thread {

    private final MessageRepository repository = PersistenceContext.repositories().messages();
    private final ProductionLineRepository Pepository = PersistenceContext.repositories().productionLines();
    private final MachineRepository Mepository = PersistenceContext.repositories().machines();
    private final ProductionLineRepository pdRepository = PersistenceContext.repositories().productionLines();

    private String line;

    private Socket myS;
    private DataInputStream sIn;

    public TcpChatSrvClient(Socket s) {
        myS = s;
    }

    public void run() {

        List<Message> lst = new ArrayList();

        try {
            int nChars = 0;
            byte[] data = new byte[500];

            ImportMessageFromDataController cont = new ImportMessageFromDataController();
            InetAddress clientIP;

            clientIP = myS.getInetAddress();
            System.out.println("New client connection from " + clientIP.getHostAddress()
                    + ", port number " + myS.getPort());

            sIn = new DataInputStream(myS.getInputStream());

            while (true) {
                nChars = sIn.readInt();

                if (nChars == -1) {
                    System.out.println("Exiting...");
                    break; // empty line means client wants to exit
                }
                sIn.read(data, 0, nChars);

                line = new String(data);

                System.out.println(nChars + "-----" + line.split(" ")[0].trim() + "-----");
                lst.add(cont.importMessage(line.split(" ")[0].trim()));
                data = new byte[500];
            }

            String pID = lst.get(0).getDesc().getDesc().split(";")[0];

            Machine m = Mepository.findByNumSerie(pID).get();
            Iterable<ProductionLine> ls = pdRepository.findAll();

            ProductionLine pl1 = new ProductionLine();

            for (ProductionLine pl : ls) {
                if (pl.getMachines().contains(m)) {
                    pl1 = pl;
                }
            }

            pl1.getMessages().addAll(lst);

            pdRepository.save(pl1);

            ImportMessageFromMachineUI.remCli(myS);
        } catch (IOException ex) {
            Logger.getLogger(TcpChatSrvClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(TcpChatSrvClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
