package lapr4.persistence.impl.inmemory;

import lapr4.factoryusermanagement.domain.SignupRequest;
import lapr4.factoryusermanagement.repositories.SignupRequestRepository;
import eapli.framework.infrastructure.authz.domain.model.Username;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemorySignupRequestRepository extends
        InMemoryDomainRepository<Username, SignupRequest> implements SignupRequestRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<SignupRequest> pendingSignupRequests() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
