package lapr4.persistence.impl.inmemory;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.RawMateria;
import lapr4.productmanagement.domain.repositories.RawMateriaRepository;

public class InMemoryRawMateriasRepository 
        extends InMemoryDomainRepository<Long, RawMateria>
        implements RawMateriaRepository {

      
    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<RawMateria> findById(final Long id) {
        return matchOne(e -> e.hasIdentity(id));
    }

    @Override
    public Iterable<RawMateria> findAllActive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
