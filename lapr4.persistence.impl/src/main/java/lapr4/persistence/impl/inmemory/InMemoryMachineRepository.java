/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.persistence.impl.inmemory;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.repositories.MachineRepository;

/**
 *
 * @author migue
 */
public class InMemoryMachineRepository 
        extends InMemoryDomainRepository<Long, Machine>
        implements MachineRepository {

      
    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<Machine> findById(final Long id) {
        return matchOne(e -> e.hasIdentity(id));
    }

    @Override
    public Iterable<Machine> findAllActive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<Machine> findByNumSerie(String s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
