/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.persistence.impl.inmemory;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;

/**
 *
 * @author migue
 */
public class InMemoryProductionSheetsRepository 
        extends InMemoryDomainRepository<String, ProductionSheet>
        implements ProductionSheetRepository {

      
    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<ProductionSheet> findById(final String id) {
        return matchOne(e -> e.hasIdentity(id));
    }

    @Override
    public Iterable<ProductionSheet> findAllActive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
