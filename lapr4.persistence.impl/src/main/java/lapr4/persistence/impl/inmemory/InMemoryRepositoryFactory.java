package lapr4.persistence.impl.inmemory;

import lapr4.factoryusermanagement.repositories.SignupRequestRepository;
import lapr4.infrastructure.bootstrapers.EFactoryBootstrapper;
import lapr4.infrastructure.persistence.RepositoryFactory;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.infrastructure.authz.repositories.impl.InMemoryUserRepository;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.factoryusermanagement.repositories.FactoryUserRepository;
import lapr4.messagemanagement.repositories.MessageRepository;
import lapr4.productionmanagement.repositories.NotificationRepository;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaCategoryRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaRepository;
import lapr4.productmanagement.domain.repositories.DepositRepository;
import lapr4.productmanagement.domain.repositories.ProductLotRepository;

public class InMemoryRepositoryFactory implements RepositoryFactory {

    static {
        // only needed because of the in memory persistence
        new EFactoryBootstrapper().execute();
    }

    @Override
    public UserRepository users(final TransactionalContext tx) {
        return new InMemoryUserRepository();
    }

    @Override
    public UserRepository users() {
        return users(null);
    }

    @Override
    public FactoryUserRepository factoryUsers(final TransactionalContext tx) {

        return new InMemoryFactoryUserRepository();
    }

    @Override
    public FactoryUserRepository factoryUsers() {
        return factoryUsers(null);
    }

    @Override
    public SignupRequestRepository signupRequests() {
        return signupRequests(null);
    }

    @Override
    public SignupRequestRepository signupRequests(final TransactionalContext tx) {
        return new InMemorySignupRequestRepository();
    }

    @Override
    public TransactionalContext newTransactionalContext() {
        // in memory does not support transactions...
        return null;
    }

    @Override
    public DepositRepository deposits() {
        return deposits(null);
    }

    @Override
    public DepositRepository deposits(final TransactionalContext tx) {
        return new InMemoryDepositsRepository();
    }

    @Override
    public ProductRepository products() {    
        return products(null);
    }

    @Override
    public ProductRepository products(final TransactionalContext tx) {
        return new InMemoryProductsRepository();
    }

    @Override
    public ProductionSheetRepository productionSheets() {
        return productionSheets(null);
    }

    @Override
    public ProductionSheetRepository productionSheets(final TransactionalContext tx) {
        return new InMemoryProductionSheetsRepository();
    }

    @Override
    public RawMateriaCategoryRepository rawMateriaCategories() {
        return rawMateriaCategories(null);
    }
    
    @Override
    public RawMateriaCategoryRepository rawMateriaCategories(final TransactionalContext tx) {
        return new InMemoryRawMateriaCategoryRepository();
    }
    

    @Override
    public RawMateriaRepository rawMaterias() {
        return rawMaterias(null);
    }
    
    @Override
    public RawMateriaRepository rawMaterias(final TransactionalContext tx) {
        return new InMemoryRawMateriasRepository();
    }

    @Override
    public ProductLotRepository productLots() {
        return productLots(null);
    }

    @Override
    public ProductLotRepository productLots(TransactionalContext autoTx) {
        return new InMemoryProductLotsRepository();
    }

    @Override
    public NotificationRepository notifications() {
        return notifications(null);
    }

    @Override
    public NotificationRepository notifications(TransactionalContext autoTx) {
        return new InMemoryNotificationRepository();
    }

    @Override
    public ProductionOrderRepository productionOrders() {
        return productionOrders(null);
    }

    @Override
    public ProductionOrderRepository productionOrders(TransactionalContext autoTx) {
        return new InMemoryProductionOrderRepository();
    }

    @Override
    public MachineRepository machines() {
        return machines(null);
    }

    @Override
    public MachineRepository machines(TransactionalContext autoTx) {
        return new InMemoryMachineRepository();
    }

    @Override
    public ProductionLineRepository productionLines() {
        return productionLines(null);
    }

    @Override
    public ProductionLineRepository productionLines(TransactionalContext autoTx) {
        return new InMemoryProductionLineRepository();
    }

    @Override
    public MessageRepository messages() {
        return messages(null);
    }

    @Override
    public MessageRepository messages(TransactionalContext autoTx) {
        return new InMemoryMessageRepository();
    }





}
