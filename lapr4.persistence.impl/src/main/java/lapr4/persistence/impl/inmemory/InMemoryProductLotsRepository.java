package lapr4.persistence.impl.inmemory;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.ProductLot;
import lapr4.productmanagement.domain.repositories.ProductLotRepository;

public class InMemoryProductLotsRepository 
        extends InMemoryDomainRepository<String, ProductLot>
        implements ProductLotRepository {

      
    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<ProductLot> findById(final String id) {
        return matchOne(e -> e.hasIdentity(id));
    }

    @Override
    public Iterable<ProductLot> findAllActive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
