/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.persistence.impl.inmemory;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;

/**
 *
 * @author migue
 */
public class InMemoryProductionOrderRepository 
        extends InMemoryDomainRepository<Long, ProductionOrder>
        implements ProductionOrderRepository {

      
    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<ProductionOrder> findById(final Long id) {
        return matchOne(e -> e.hasIdentity(id));
    }

    @Override
    public Iterable<ProductionOrder> findAllActive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Iterable<ProductionOrder> findProductionOrderByIdEncomenda(final String idEncomenda) {
         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.  
    }

    @Override
    public Iterable<ProductionOrder> findAllByState(OrderStatus orderStatus) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
