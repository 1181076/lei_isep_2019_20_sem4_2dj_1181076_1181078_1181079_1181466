package lapr4.persistence.impl.inmemory;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.ProductionLineRepository;

public class InMemoryProductionLineRepository
        extends InMemoryDomainRepository<Long, ProductionLine>
        implements ProductionLineRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<ProductionLine> findById(final Long id) {
        return matchOne(e -> e.hasIdentity(id));
    }

    @Override
    public Iterable<ProductionLine> findAllActive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<ProductionLine> findToBeProcessed() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int numTotal() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
