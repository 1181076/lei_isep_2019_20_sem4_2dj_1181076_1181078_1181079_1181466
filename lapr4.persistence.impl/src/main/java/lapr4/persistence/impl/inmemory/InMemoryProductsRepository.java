package lapr4.persistence.impl.inmemory;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductManufactureCode;
import lapr4.productmanagement.domain.repositories.ProductRepository;

public class InMemoryProductsRepository 
        extends InMemoryDomainRepository<Long, Product>
        implements ProductRepository {

      
    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<Product> findByID(final Long id) {
        return matchOne(e -> e.hasIdentity(id));
    }

    @Override
    public Iterable<Product> findAllActive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Product> findAllWithoutPS() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<Product> findByCode(String code) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
