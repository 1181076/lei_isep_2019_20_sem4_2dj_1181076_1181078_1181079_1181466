package lapr4.persistence.impl.inmemory;

import java.util.Optional;

import lapr4.factoryusermanagement.domain.FactoryUser;
import lapr4.factoryusermanagement.domain.MecanographicNumber;
import eapli.framework.infrastructure.authz.domain.model.Username;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import lapr4.factoryusermanagement.repositories.FactoryUserRepository;


public class InMemoryFactoryUserRepository
        extends InMemoryDomainRepository<MecanographicNumber, FactoryUser>
        implements FactoryUserRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<FactoryUser> findByUsername(final Username name) {
        return matchOne(e -> e.user().username().equals(name));
    }

    @Override
    public Optional<FactoryUser> findByMecanographicNumber(final MecanographicNumber number) {
        return Optional.of(data().get(number));
    }

    @Override
    public Iterable<FactoryUser> findAllActive() {
        return match(e -> e.user().isActive());
    }
}
