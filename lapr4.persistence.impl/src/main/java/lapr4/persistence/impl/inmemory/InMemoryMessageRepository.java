    package lapr4.persistence.impl.inmemory;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.repositories.MessageRepository;

public class InMemoryMessageRepository 
        extends InMemoryDomainRepository<Long, Message>
        implements MessageRepository {

      
    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<Message> findById(final Long id) {
        return matchOne(e -> e.hasIdentity(id));
    }

    @Override
    public Iterable<Message > findAllActive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void process(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
