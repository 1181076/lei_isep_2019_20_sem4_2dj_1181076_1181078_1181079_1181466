package lapr4.persistence.impl.inmemory;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;
import lapr4.productmanagement.domain.Deposit;
import lapr4.productmanagement.domain.repositories.DepositRepository;

public class InMemoryDepositsRepository 
        extends InMemoryDomainRepository<String, Deposit>
        implements DepositRepository {

      
    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<Deposit> findById(final String id) {
        return matchOne(e -> e.hasIdentity(id));
    }

    @Override
    public Iterable<Deposit> findAllActive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
