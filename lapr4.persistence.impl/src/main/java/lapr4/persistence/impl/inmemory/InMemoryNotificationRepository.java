package lapr4.persistence.impl.inmemory;

import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import java.util.Optional;
import lapr4.productionmanagement.domain.ErrorType;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.repositories.NotificationRepository;

public class InMemoryNotificationRepository 
        extends InMemoryDomainRepository<Long, Notification>
        implements NotificationRepository {

      
    static {
        InMemoryInitializer.init();
    }

    @Override
    public Optional<Notification> findById(final Long id) {
        return matchOne(e -> e.hasIdentity(id));
    }

    @Override
    public Iterable<Notification> findAllActive() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Notification> findAllWithoutTreatment() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Notification> findAllWithTreatment() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Notification> findAllWithTreatment(ErrorType t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Notification> findAllWithoutTreatment(ErrorType t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
