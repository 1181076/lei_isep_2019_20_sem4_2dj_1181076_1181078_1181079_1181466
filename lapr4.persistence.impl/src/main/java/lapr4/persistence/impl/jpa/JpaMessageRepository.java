package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.repositories.MessageRepository;

public class JpaMessageRepository 
        extends JpaAutoTxRepository<Message, Long, Long>
        implements MessageRepository  {

    public JpaMessageRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaMessageRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<Message> findAllActive() {
        final TypedQuery<Message> query = createQuery(
                "SELECT * FROM Message",
                Message.class);

        return query.getResultList();
    }

    @Override
    public void process(Long id) {
        
        final Query query = createQuery(
                "UPDATE Message m SET m.processed = :val WHERE id= :idM",
                Message.class);
        
        query.setParameter("val", true);
        query.setParameter("idM", id);

        query.executeUpdate();
    }
}
