package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductManufactureCode;
import lapr4.productmanagement.domain.repositories.ProductRepository;

public class JpaProductRepository 
        extends JpaAutoTxRepository<Product, Long, Long>
        implements ProductRepository {

    public JpaProductRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaProductRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<Product> findAllActive() {
        final TypedQuery<Product> query = createQuery(
                "SELECT * FROM Product",
                Product.class);

        return query.getResultList();
    }

    @Override
    public Iterable<Product> findAllWithoutPS() {
        
        final Map<String, Object> params = new HashMap<>();
        return match("e.productSheet is null");
        
    }

    @Override
    public Optional<Product> findByCode(String code) {
        
        return matchOne("e.productManufactureCode.code = '" + code + "'");
    }
    
}
