/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.productmanagement.domain.Deposit;
import lapr4.productmanagement.domain.repositories.DepositRepository;

/**
 *
 * @author migue
 */
public class JpaDepositRepository 
        extends JpaAutoTxRepository<Deposit, String, String>
        implements DepositRepository {

    public JpaDepositRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaDepositRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<Deposit> findAllActive() {
        final TypedQuery<Deposit> query = createQuery(
                "SELECT * FROM Deposit",
                Deposit.class);

        return query.getResultList();
    }
}
