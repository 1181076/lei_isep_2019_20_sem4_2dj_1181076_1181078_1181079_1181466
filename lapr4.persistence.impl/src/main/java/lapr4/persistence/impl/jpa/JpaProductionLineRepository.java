package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.ProductionLineRepository;

public class JpaProductionLineRepository
        extends JpaAutoTxRepository<ProductionLine, Long, Long>
        implements ProductionLineRepository {

    public JpaProductionLineRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaProductionLineRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<ProductionLine> findAllActive() {
        final TypedQuery<ProductionLine> query = createQuery(
                "SELECT * FROM ProductionLine",
                ProductionLine.class);

        return query.getResultList();
    }

    @Override
    public Iterable<ProductionLine> findToBeProcessed() {
        String qS = "SELECT e FROM ProductionLine e WHERE e.processable = :tprocessable";

        final TypedQuery<ProductionLine> q = createQuery(qS, ProductionLine.class);
        q.setParameter("tprocessable", true);

        return q.getResultList();
    }

    @Override
    public int numTotal() {
        final TypedQuery<ProductionLine> query = createQuery(
                "SELECT count(*) FROM ProductionLine",
                ProductionLine.class);

        return query.getFirstResult();
    }
}
