package lapr4.persistence.impl.jpa;

class PersistenceSettings {

    public static final String PERSISTENCE_UNIT_NAME = "lapr4e";

    private PersistenceSettings() {
    }
}
