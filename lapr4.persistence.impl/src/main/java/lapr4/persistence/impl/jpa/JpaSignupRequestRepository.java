package lapr4.persistence.impl.jpa;

import lapr4.Application;
import lapr4.factoryusermanagement.domain.SignupRequest;
import lapr4.factoryusermanagement.repositories.SignupRequestRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.model.Username;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

class JpaSignupRequestRepository extends JpaAutoTxRepository<SignupRequest, Username, Username>
        implements SignupRequestRepository {

    public JpaSignupRequestRepository(final TransactionalContext autoTx) {
        super(autoTx, "username");
    }

    public JpaSignupRequestRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(), "username");
    }

    @Override
    public Iterable<SignupRequest> pendingSignupRequests() {
        return match(
                "e.approvalStatus=lap4.factoryusermanagement.domain.ApprovalStatus.PENDING");
    }
}
