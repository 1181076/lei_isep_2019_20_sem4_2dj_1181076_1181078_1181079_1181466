/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;

/**
 *
 * @author migue
 */
public class JpaProductionOrderRepository 
        extends JpaAutoTxRepository<ProductionOrder, Long, Long>
        implements ProductionOrderRepository  {

    public JpaProductionOrderRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaProductionOrderRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<ProductionOrder> findAllActive() {
        final TypedQuery<ProductionOrder> query = createQuery(
                "SELECT * FROM ProductionOrder",
                ProductionOrder.class);

        return query.getResultList();
    }
    
    @Override
    public Iterable <ProductionOrder> findProductionOrderByIdEncomenda(final  String idEncomenda) {
        final Map<String, Object> params = new HashMap<>();
          params.put("idEncomenda", idEncomenda);
        return match("e.idEncomenda=:idEncomenda", params);
    }

    @Override
    public Iterable<ProductionOrder> findAllByState(OrderStatus orderStatus) {
        final Map<String, Object> params = new HashMap<>();
          params.put("orderStatus", orderStatus);
        return match("e.orderStatus=:orderStatus", params);
    }

    
}
