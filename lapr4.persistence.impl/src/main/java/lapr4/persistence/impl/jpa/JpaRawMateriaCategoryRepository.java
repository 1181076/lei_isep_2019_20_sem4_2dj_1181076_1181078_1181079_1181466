package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.productmanagement.domain.RawMateriaCategory;
import lapr4.productmanagement.domain.repositories.RawMateriaCategoryRepository;

public class JpaRawMateriaCategoryRepository 
        extends JpaAutoTxRepository<RawMateriaCategory, Long, Long>
        implements RawMateriaCategoryRepository  {

    public JpaRawMateriaCategoryRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaRawMateriaCategoryRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<RawMateriaCategory> findAllActive() {
        final TypedQuery<RawMateriaCategory> query = createQuery(
                "SELECT * FROM RawMateriaCategory",
                RawMateriaCategory.class);

        return query.getResultList();
    }
}
