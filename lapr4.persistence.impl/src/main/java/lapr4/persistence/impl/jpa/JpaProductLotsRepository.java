package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.productmanagement.domain.ProductLot;
import lapr4.productmanagement.domain.repositories.ProductLotRepository;

public class JpaProductLotsRepository 
        extends JpaAutoTxRepository<ProductLot, String, String>
        implements ProductLotRepository {

    public JpaProductLotsRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaProductLotsRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<ProductLot> findAllActive() {
        final TypedQuery<ProductLot> query = createQuery(
                "SELECT * FROM ProductLot",
                ProductLot.class);

        return query.getResultList();
    }
}
