/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import java.util.Optional;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.repositories.MachineRepository;

/**
 *
 * @author migue
 */
public class JpaMachineRepository 
        extends JpaAutoTxRepository<Machine, Long, Long>
        implements MachineRepository {

    public JpaMachineRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaMachineRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<Machine> findAllActive() {
        final TypedQuery<Machine> query = createQuery(
                "SELECT * FROM Machine",
                Machine.class);

        return query.getResultList();
    }

    @Override
    public Optional<Machine> findByNumSerie(String s) {
        return matchOne("e.numSerie = '" + s + "'");
    }
}
