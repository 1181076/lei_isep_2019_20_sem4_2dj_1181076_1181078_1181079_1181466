/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.productionmanagement.domain.ErrorType;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.domain.NotificationType;
import lapr4.productionmanagement.repositories.NotificationRepository;

/**
 *
 * @author migue
 */
public class JpaNotificationRepository
        extends JpaAutoTxRepository<Notification, Long, Long>
        implements NotificationRepository {

    public JpaNotificationRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaNotificationRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<Notification> findAllActive() {
        final TypedQuery<Notification> query = createQuery(
                "SELECT * FROM Notification",
                Notification.class);

        return query.getResultList();
    }

    @Override
    public Iterable<Notification> findAllWithoutTreatment() {
        final TypedQuery<Notification> q = createQuery(
                "SELECT e FROM Notification e WHERE e.type = :ttype",
                Notification.class);
        q.setParameter("ttype", NotificationType.TBD);
        return q.getResultList();
    }

    @Override
    public Iterable<Notification> findAllWithTreatment() {
        final TypedQuery<Notification> q = createQuery(
                "SELECT e FROM Notification e WHERE e.type = :ttype",
                Notification.class);
        q.setParameter("ttype", NotificationType.DEALT);
        return q.getResultList();
    }

    @Override
    public Iterable<Notification> findAllWithTreatment(ErrorType t) {
        String qS = "SELECT e FROM Notification e WHERE e.type = :ttype AND e.errorType =: tt1";

        final TypedQuery<Notification> q = createQuery(qS, Notification.class);
        q.setParameter("ttype", NotificationType.DEALT);

        q.setParameter("tt1", t);

        return q.getResultList();
    }

    @Override
    public Iterable<Notification> findAllWithoutTreatment(ErrorType t) {
        String qS = "SELECT e FROM Notification e WHERE e.type = :ttype AND e.errorType =: tt1";

        final TypedQuery<Notification> q = createQuery(qS, Notification.class);
        q.setParameter("ttype", NotificationType.TBD);

        q.setParameter("tt1", t);

        return q.getResultList();
    }

}
