package lapr4.persistence.impl.jpa;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import lapr4.Application;
import lapr4.factoryusermanagement.domain.FactoryUser;
import lapr4.factoryusermanagement.domain.MecanographicNumber;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.model.Username;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import lapr4.factoryusermanagement.repositories.FactoryUserRepository;

class JpaFactoryUserRepository
        extends JpaAutoTxRepository<FactoryUser, MecanographicNumber, MecanographicNumber>
        implements FactoryUserRepository {

    public JpaFactoryUserRepository(final TransactionalContext autoTx) {
        super(autoTx, "mecanographicNumber");
    }

    public JpaFactoryUserRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "mecanographicNumber");
    }

    @Override
    public Optional<FactoryUser> findByUsername(final Username name) {
        final Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        return matchOne("e.systemUser.username=:name", params);
    }

    @Override
    public Optional<FactoryUser> findByMecanographicNumber(final MecanographicNumber number) {
        final Map<String, Object> params = new HashMap<>();
        params.put("number", number);
        return matchOne("e.mecanographicNumber=:number", params);
    }

    @Override
    public Iterable<FactoryUser> findAllActive() {
        return match("e.systemUser.active = true");
    }
}
