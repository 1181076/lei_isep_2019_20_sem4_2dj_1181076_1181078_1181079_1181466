package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;

public class JpaProductionSheetRepository 
        extends JpaAutoTxRepository<ProductionSheet, String, String>
        implements ProductionSheetRepository {

    public JpaProductionSheetRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaProductionSheetRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<ProductionSheet> findAllActive() {
        final TypedQuery<ProductionSheet> query = createQuery(
                "SELECT * FROM ProductionSheet",
                ProductionSheet.class);

        return query.getResultList();
    }
    
}
