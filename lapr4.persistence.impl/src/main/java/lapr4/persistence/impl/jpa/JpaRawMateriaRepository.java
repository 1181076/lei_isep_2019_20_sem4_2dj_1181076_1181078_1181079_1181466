package lapr4.persistence.impl.jpa;

import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import javax.persistence.TypedQuery;
import lapr4.Application;
import lapr4.productmanagement.domain.RawMateria;
import lapr4.productmanagement.domain.repositories.RawMateriaRepository;

public class JpaRawMateriaRepository 
        extends JpaAutoTxRepository<RawMateria, Long, Long>
        implements RawMateriaRepository  {

    public JpaRawMateriaRepository(final TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    public JpaRawMateriaRepository(final String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(),
                "id");
    }

    @Override
    public Iterable<RawMateria> findAllActive() {
        final TypedQuery<RawMateria> query = createQuery(
                "SELECT * FROM RawMateria",
                RawMateria.class);

        return query.getResultList();
    }
}
