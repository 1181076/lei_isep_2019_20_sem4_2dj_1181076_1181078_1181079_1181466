package lapr4.persistence.impl.jpa;

import lapr4.Application;
import lapr4.factoryusermanagement.repositories.SignupRequestRepository;
import lapr4.infrastructure.persistence.RepositoryFactory;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.infrastructure.authz.repositories.impl.JpaAutoTxUserRepository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.messagemanagement.repositories.MessageRepository;
import lapr4.productionmanagement.repositories.NotificationRepository;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;
import lapr4.productmanagement.domain.repositories.DepositRepository;
import lapr4.productmanagement.domain.repositories.ProductLotRepository;
import lapr4.productmanagement.domain.repositories.ProductRepository;
import lapr4.productmanagement.domain.repositories.ProductionSheetRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaCategoryRepository;
import lapr4.productmanagement.domain.repositories.RawMateriaRepository;

public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public UserRepository users(final TransactionalContext autoTx) {
        return new JpaAutoTxUserRepository(autoTx);
    }

    @Override
    public UserRepository users() {
        return new JpaAutoTxUserRepository(Application.settings().getPersistenceUnitName(),
                Application.settings().getExtendedPersistenceProperties());
    }


    @Override
    public JpaFactoryUserRepository factoryUsers(final TransactionalContext autoTx) {
        return new JpaFactoryUserRepository(autoTx);
    }

    @Override
    public JpaFactoryUserRepository factoryUsers() {
        return new JpaFactoryUserRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public SignupRequestRepository signupRequests(final TransactionalContext autoTx) {
        return new JpaSignupRequestRepository(autoTx);
    }

    @Override
    public SignupRequestRepository signupRequests() {
        return new JpaSignupRequestRepository(Application.settings().getPersistenceUnitName());
    }


    @Override
    public TransactionalContext newTransactionalContext() {
        return JpaAutoTxRepository.buildTransactionalContext(
                Application.settings().getPersistenceUnitName(),
                Application.settings().getExtendedPersistenceProperties());
    }

    @Override
    public DepositRepository deposits() {
        return new JpaDepositRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public DepositRepository deposits(TransactionalContext autoTx) {
        return new JpaDepositRepository(autoTx);
    }

    @Override
    public ProductRepository products() {
        return new JpaProductRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public ProductRepository products(TransactionalContext autoTx) {
        return new JpaProductRepository(autoTx);
    }

    @Override
    public ProductionSheetRepository productionSheets() {
        return new JpaProductionSheetRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public ProductionSheetRepository productionSheets(TransactionalContext autoTx) {
        return new JpaProductionSheetRepository(autoTx);
    }

    @Override
    public RawMateriaCategoryRepository rawMateriaCategories() {
        return new JpaRawMateriaCategoryRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public RawMateriaCategoryRepository rawMateriaCategories(TransactionalContext autoTx) {
        return new JpaRawMateriaCategoryRepository(autoTx);
    }

    @Override
    public RawMateriaRepository rawMaterias() {
        return new JpaRawMateriaRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public RawMateriaRepository rawMaterias(TransactionalContext autoTx) {
        return new JpaRawMateriaRepository(autoTx);
    }

    @Override
    public ProductLotRepository productLots() {
        return new JpaProductLotsRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public ProductLotRepository productLots(TransactionalContext autoTx) {
        return new JpaProductLotsRepository(autoTx);
    }
    
    @Override
    public NotificationRepository notifications() {
        return new JpaNotificationRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public NotificationRepository notifications(TransactionalContext autoTx) {
        return new JpaNotificationRepository(autoTx);
    }

    @Override
    public ProductionOrderRepository productionOrders() {
        return new JpaProductionOrderRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public ProductionOrderRepository productionOrders(TransactionalContext autoTx) {
        return new JpaProductionOrderRepository(autoTx);
    }

    @Override
    public MachineRepository machines() {
        return new JpaMachineRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public MachineRepository machines(TransactionalContext autoTx) {
        return new JpaMachineRepository(autoTx);
    }

    @Override
    public ProductionLineRepository productionLines() {
        return new JpaProductionLineRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public ProductionLineRepository productionLines(TransactionalContext autoTx) {
        return new JpaProductionLineRepository(autoTx);
    }

    @Override
    public MessageRepository messages() {
        return new JpaMessageRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public MessageRepository messages(TransactionalContext autoTx) {
        return new JpaMessageRepository(autoTx);
    }

 
}
