/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import java.util.Iterator;
import java.util.Set;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.domain.ProductionOrder;

/**
 *
 * @author Leticia
 */
public class ProductionOrderPrinter implements Visitor<ProductionOrder> {

    public ProductionOrderPrinter() {
    }

    @Override
    public void visit(ProductionOrder visitee) {

        Set<Notification> itNot = visitee.getNotifications();
        Iterator<Notification> itNotit = itNot.iterator();

        Notification c;

        System.out.println("\n\n\n---------------------------------------");
        System.out.println("ProductionOrderID:        " + visitee.getId());
        System.out.println("Order Emission Date:      " + visitee.getOrderEmissionDate());
        if (visitee.getPreviewFinalDate() == null) {
            System.out.println("Preview Final Date:       " + null);
        } else {
            System.out.println("Preview Final Date:       " + visitee.getPreviewFinalDate());
        }
        System.out.println("Product:                  " + visitee.getProduct().getId());
        if (visitee.getLine() == null) {
            System.out.println("Production Line:          " + null);
        } else {
            System.out.println("Production Line:          " + visitee.getLine().getId());
        }
        System.out.println("ID Encomenda:             " + visitee.getIdEncomenda());
        System.out.println("Order Status:             " + visitee.getOrderStatus());
        if (visitee.getPreviewFinalDate() != null) {
            System.out.println("No Preview Final Date associated");
        }
        if (visitee.getLine() != null) {
            System.out.println("Product Line:                " + visitee.getLine().getId());
        } else {
            System.out.println("No Production Line associated");
        }

        System.out.println("====================================");
        System.out.println("\nNotifications");
        for (int i = 0; i < itNot.size(); i++) {
            c = itNotit.next();
            System.out.println("ID:               " + c.getId());
            System.out.println("Descrição:        " + c.getDesc());
            System.out.println("Tipo:             " + c.getType());
        }
        System.out.println("====================================");
        System.out.println("---------------------------------------");
    }

}
