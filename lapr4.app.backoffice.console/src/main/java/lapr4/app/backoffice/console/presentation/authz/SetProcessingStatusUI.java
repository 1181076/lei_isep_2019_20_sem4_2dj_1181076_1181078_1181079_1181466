/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import lapr4.factorymachines.application.ListProductionLineController;
import lapr4.factorymachines.application.SetProcessingStatusController;
import lapr4.factorymachines.domain.ProductionLine;

/**
 *
 * @author Leticia
 */
public class SetProcessingStatusUI extends AbstractUI{
    
    private ListProductionLineController cont = new ListProductionLineController();
    private SetProcessingStatusController contr = new SetProcessingStatusController();
    
    @Override
    protected boolean doShow() {

        System.out.println("Escolha a Linha de Produção");

        final Iterable<ProductionLine> pls = this.cont.allProductionLines();

        final SelectWidget<ProductionLine> selector = new SelectWidget<>("Production Line:", pls,
                new ProductionLineStatusPrinter());
        selector.show();
        final ProductionLine pl = selector.selectedElement();

        String escolha = Console.readLine("Set processing status to active or inative? ");
  
                
        try {
            this.contr.setProcessingStatus(pl, escolha);

        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            
        }
        return false;
    }

    @Override
    public String headline() {
        return "Set Processing Status for a Production Line";
    }
}
