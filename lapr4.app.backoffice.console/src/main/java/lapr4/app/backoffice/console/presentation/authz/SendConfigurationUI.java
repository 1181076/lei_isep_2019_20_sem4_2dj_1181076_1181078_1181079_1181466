/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr4.factorymachines.domain.ConfigurationFile;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.domain.application.ImportMessageFromDataController;
import lapr4.messagemanagement.repositories.MessageRepository;

/**
 *
 * @author mxlsa
 */
public class SendConfigurationUI extends AbstractUI {

    private static HashMap<Socket, DataOutputStream> cliList = new HashMap<>();

    public static synchronized void sendTo(int len, byte[] data, Socket s) throws Exception {
        DataOutputStream cOut = cliList.get(s);
        cOut.write(len);
        cOut.write(data, 0, len);

    }

    public static synchronized void addCli(Socket s) throws Exception {
        cliList.put(s, new DataOutputStream(s.getOutputStream()));
    }

    public static synchronized void remCli(Socket s) throws Exception {
        cliList.get(s).write(0);
        cliList.remove(s);
        s.close();
    }

    private static ServerSocket sock;

    @Override
    protected boolean doShow() {
        try {
            sock = new ServerSocket(9991);
        } catch (IOException ex) {
            System.out.println("Local port number not available.");
            System.exit(1);
        }

        while (true) {
            try {
                System.out.println("Waiting for Machine...");
                Socket s = sock.accept(); // wait for a new client connection request
                System.out.println("Found Machine...");
                addCli(s);
                Thread cli = new TcpChatSrvClient(s);
                cli.start();
            } catch (IOException ex) {
                Logger.getLogger(SendConfigurationUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(SendConfigurationUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String headline() {
        return "Send configuration file";
    }

}

class TcpChatSrvClient extends Thread {

    private final MachineRepository Mepository = PersistenceContext.repositories().machines();

    private String line;

    private Socket myS;
    private DataInputStream sIn;

    public TcpChatSrvClient(Socket s) {
        myS = s;
    }

    public void run() {

        try {
            int nChars = 0;
            byte[] data = new byte[500];

            sIn = new DataInputStream(myS.getInputStream());
            DataOutputStream sOut = new DataOutputStream(myS.getOutputStream());

            nChars = sIn.readInt();

            sIn.read(data, 0, nChars);

            line = new String(data);

            System.out.println(nChars + "-----" + line.split(" ")[0].trim() + "-----");
            Machine mach = Mepository.findByNumSerie(line.split(" ")[0].trim()).get();
            if (mach.getFile() == null) {
                System.out.println("Máquina não tem ficheiro de configuração");
                String file = Console.readLine("Qual a localização/nome do ficheiro?");
                mach.setFile(new ConfigurationFile(file));
                Mepository.save(mach);
                sOut.write(mach.getFile().getFile().getBytes(), 0, mach.getFile().getFile().length());

            } else {
                sOut.write(mach.getFile().getFile().getBytes(), 0, mach.getFile().getFile().length());
            }
            SendConfigurationUI.remCli(myS);
        } catch (IOException ex) {
            Logger.getLogger(TcpChatSrvClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(TcpChatSrvClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
