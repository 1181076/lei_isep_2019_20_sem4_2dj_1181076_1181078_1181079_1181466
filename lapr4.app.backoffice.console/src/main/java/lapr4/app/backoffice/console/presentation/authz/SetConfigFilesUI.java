/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import lapr4.factorymachines.application.ListMachineController;
import lapr4.factorymachines.application.SetConfigFilesController;
import lapr4.factorymachines.domain.ConfigurationFile;
import lapr4.factorymachines.domain.Machine;

/**
 *
 * @author migue
 */
public class SetConfigFilesUI extends AbstractUI{
    
    private ListMachineController cont = new ListMachineController();
    private SetConfigFilesController contr = new SetConfigFilesController();
    
    @Override
    protected boolean doShow() {

        System.out.println("Escolha a Máquina");

        final Iterable<Machine> machines = this.cont.allMachines();

        final SelectWidget<Machine> selectorr = new SelectWidget<>("Machine:", machines,
                new MachinePrinter());
        selectorr.show();
        final Machine mac = selectorr.selectedElement();

        String a = Console.readLine("Nome do Ficheiro de Configuração");
  
                
        try {
            this.contr.setFile(mac, new ConfigurationFile(a));

        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            
        }
        return false;
    }

    @Override
    public String headline() {
        return "Set Configuration Files for a Machine";
    }
}
