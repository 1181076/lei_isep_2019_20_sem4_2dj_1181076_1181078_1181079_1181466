/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import lapr4.productionmanagement.application.RemoveNotificationsController;

/**
 *
 * @author vrmpe
 */
public class RemoveNotificationsUI extends AbstractUI{
    
    private final RemoveNotificationsController theController = new RemoveNotificationsController();
    private final ListAllNotificationsUI theUI = new ListAllNotificationsUI();

    @Override
    protected boolean doShow() {
        
        theUI.elements();
        
        final long id = Console.readInteger("ID da Notificação");
        
        try{
            
            theController.removeNotifications(id);
            
        }catch(@SuppressWarnings("unused") final IntegrityViolationException e){
            System.out.println("You tried to enter a Notification that doesn't exist.");
        }
        
        System.out.println("Operação Terminada");
        return false;
    }

    @Override
    public String headline() {
        return "Remove Notification";
    }
    
}
