package lapr4.app.backoffice.console;

import lapr4.app.common.console.EFactoryBaseApplication;
import lapr4.app.common.console.presentation.authz.LoginUI;
import lapr4.app.backoffice.console.presentation.MainMenu;
import lapr4.factoryusermanagement.application.eventhandlers.NewUserRegisteredFromSignupWatchDog;
import lapr4.factoryusermanagement.domain.events.NewUserRegisteredFromSignupEvent;
import lapr4.factoryusermanagement.domain.events.SignupAcceptedEvent;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.usermanagement.application.eventhandlers.SignupAcceptedWatchDog;
import lapr4.usermanagement.domain.FactoryPasswordPolicy;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.infrastructure.eventpubsub.EventDispatcher;

@SuppressWarnings("squid:S106")
public final class EFactoryBackoffice extends EFactoryBaseApplication {

    /**
     * avoid instantiation of this class.
     */
    private EFactoryBackoffice() {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {

        AuthzRegistry.configure(PersistenceContext.repositories().users(),
                new FactoryPasswordPolicy(), new PlainTextEncoder());

        new EFactoryBackoffice().run(args);
    }

    @Override
    protected void doMain(final String[] args) {
        // login and go to main menu
        if (new LoginUI().show()) {
            // go to main menu
            final MainMenu menu = new MainMenu();
            menu.mainLoop();
        }
    }

    @Override
    protected String appTitle() {
        return "eFactory Back Office";
    }

    @Override
    protected String appGoodbye() {
        return "eFactory Back Office";
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doSetupEventHandlers(final EventDispatcher dispatcher) {
        dispatcher.subscribe(new NewUserRegisteredFromSignupWatchDog(),
                NewUserRegisteredFromSignupEvent.class);
        dispatcher.subscribe(new SignupAcceptedWatchDog(),
                SignupAcceptedEvent.class);
    }
}
