/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import java.util.Iterator;
import java.util.Set;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.productionmanagement.domain.Notification;

/**
 *
 * @author mxlsa
 */
@SuppressWarnings("squid:S106")
public class ProductionLinePrinter implements Visitor<ProductionLine> {

    private final MachineRepository machineRepository = PersistenceContext.repositories().machines();

    @Override
    public void visit(final ProductionLine visitee) {
        Set<Machine> itMac = visitee.getMachines();
        Set<Message> itMes = visitee.getMessages();

        Iterator<Machine> itMacit = itMac.iterator();
        Iterator<Message> itMesit = itMes.iterator();

        Machine a;
        Message b;

        System.out.println("\n\n\n====================================");
        System.out.println("Production Line ID:            " + visitee.getId());
        System.out.println("Production Line Code:          " + visitee.getCode());
        System.out.println("Availability:                  " + visitee.isAvailability());
        System.out.println("====================================");
        System.out.println("====================================");
        System.out.println("Machines\n");
        System.out.println("Number of machine in line: " + itMac.size() + "\n");
        for (int i = 0; i < itMac.size(); i++) {
            a = itMacit.next();

            System.out.println("---------------------------------------");
            System.out.println("MachineID:            " + a.getId());
            System.out.println("Series Number:        " + a.getNumSerie());
            System.out.println("Description:          " + a.getDesc().getDesc());
            System.out.println("Installation Date:    " + a.getInstallationDate().toString());
            System.out.println("Brand:                " + a.getBrand().getBrandName());
            System.out.println("Model:                " + a.getModel().getModel());
            System.out.println("Manufacturer:         " + a.getMan().getName());
            System.out.println("Protocol:             " + a.getProt().getProtocol());
            if (a.getFile() != null) {
                System.out.println("Configuration File:   " + a.getFile().getFile());
            } else {
                System.out.println("No Configuration File ");
            }
            System.out.println("---------------------------------------");
        }

        System.out.println("====================================");
        System.out.println("====================================");
        System.out.println("\nMessages");
        Message mmm;
        for (int i = 0; i < itMes.size(); i++) {
            mmm= itMesit.next();
            System.out.println("\n\n\n---------------------------------------");
            System.out.println("MessageID:          " + mmm.getId());
            System.out.println("Description:        " + mmm.getDesc().getDesc());
            System.out.println("Type:               " + mmm.getType());
            System.out.println("---------------------------------------");
        }
        System.out.println("====================================");

    }

}
