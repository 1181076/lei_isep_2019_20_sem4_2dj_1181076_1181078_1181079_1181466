/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.factorymachines.application.ListMachineController;
import lapr4.factorymachines.domain.Machine;

/**
 *
 * @author vrmpe
 */
public class ListMachineUI extends AbstractListUI<Machine> {
    
    private final ListMachineController theController = new ListMachineController();

    @Override
    protected Iterable<Machine> elements() {
        return this.theController.allMachines();
    }

    @Override
    protected Visitor<Machine> elementPrinter() {
        return new MachinePrinter();
    }

    @Override
    protected String elementName() {
        return "Machine";
    }

    @Override
    protected String listHeader() {
        return "MACHINES";
    }

    @Override
    protected String emptyMessage() {
        return "A lista está vazia";
    }

    @Override
    public String headline() {
        return "Machines";
    }   
}
