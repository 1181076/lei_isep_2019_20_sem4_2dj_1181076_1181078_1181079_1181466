/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.productmanagement.application.ListProductionSheetsController;
import lapr4.productmanagement.application.ListProductsController;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductionSheet;

/**
 *
 * @author migue
 */
public class ListProductsUI extends AbstractListUI<Product> {

    private final ListProductsController theController = new ListProductsController();

    @Override
    protected Iterable<Product> elements() {
        return this.theController.allProducts();
    }

    @Override
    protected Visitor<Product> elementPrinter() {
        return new ProductPrinter();
    }

    @Override
    protected String elementName() {
        return "Product";
    }

    @Override
    protected String listHeader() {
        return "PRODUCTS";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    public String headline() {
        return "List Products";
    }
}
