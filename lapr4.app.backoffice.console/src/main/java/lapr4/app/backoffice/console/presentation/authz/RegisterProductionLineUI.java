/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lapr4.factorymachines.application.ListMachineController;
import lapr4.factorymachines.application.RegisterProductionLineController;
import lapr4.factorymachines.domain.Machine;
import lapr4.messagemanagement.domain.Message;

/**
 *
 * @author mxlsa
 */
public class RegisterProductionLineUI extends AbstractUI {

    private final ListMachineController listController = new ListMachineController();
    private final RegisterProductionLineController theController = new RegisterProductionLineController();

    @Override
    protected boolean doShow() {
        final long code = Console.readLong("CODE");
         try{
             this.theController.addProductionLine(code,new HashSet(),new HashSet());
             } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Production Line which already exists in the database.");
        }
        return false;
    }

    @Override

    public String headline() {
        return "Register Production Line";
    }

}
