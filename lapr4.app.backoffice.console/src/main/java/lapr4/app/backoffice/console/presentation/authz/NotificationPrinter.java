/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import java.util.Iterator;
import java.util.Map;
import lapr4.productionmanagement.domain.Notification;

/**
 *
 * @author vrmpe
 */
public class NotificationPrinter implements Visitor<Notification> {

    @Override
    public void visit(Notification visitee) {

        System.out.println("\n\n\n==========================================");
        System.out.println("ID:                   " + visitee.getId());
        System.out.println("Descrição:            " + visitee.getDesc());
        System.out.println("Tipo:                 " + visitee.getType());
        System.out.println("===========================================");
    } 
}
