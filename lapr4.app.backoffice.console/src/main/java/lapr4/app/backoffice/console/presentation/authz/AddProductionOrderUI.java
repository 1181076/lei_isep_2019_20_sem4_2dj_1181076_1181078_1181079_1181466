/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.application.AddProductionOrderController;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productmanagement.application.AddProductController;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductLot;
import lapr4.productmanagement.domain.repositories.ProductRepository;

/**
 *
 * @author Leticia
 */
public class AddProductionOrderUI extends AbstractUI {

    private final AddProductionOrderController poController = new AddProductionOrderController();
    private final ProductionLineRepository plRepo = PersistenceContext.repositories().productionLines();
    private final ProductRepository pRepo = PersistenceContext.repositories().products();

    @Override
    protected boolean doShow() {
        
        final Long ID = Console.readLong("ID da nova Ordem?");
        
        final Date orderEmissionDate = new Date();

        final String poIdEncomenda = Console.readLine("Id Encomenda");

        System.out.println("Add Product");
        String a = "Y";
        Product prod = new Product();
        while (a.equalsIgnoreCase("Y")) {

            final Iterable<Product> prods = this.pRepo.findAll();
            final SelectWidget<Product> selector2 = new SelectWidget<>("Product:", prods, new ProductPrinter());
            selector2.show();
            prod = selector2.selectedElement();

            a = Console.readLine("More Products? (Y/N)");
        }

        int amount = Console.readInteger("Quantidade?");

        Date previewFinalDate = new Date();
        final String conf = Console.readLine("Adicionar Preview Final Date? (Y/N)");
        if (conf.equalsIgnoreCase("Y")) {
            final String date = Console.readLine("Preview Date");
            previewFinalDate = new Date(date);

        }
        ProductionLine pl = new ProductionLine();
        final String conf3 = Console.readLine("Add Production Line? (Y/N)");

        if (conf3.equalsIgnoreCase("Y")) {

            final Iterable<ProductionLine> productionLines = this.plRepo.findAll();

            final SelectWidget<ProductionLine> selector = new SelectWidget<>("ProductionLine:", productionLines, new ProductionLinePrinter());

            selector.show();

            pl = selector.selectedElement();
        }

        Set<ProductLot> pls = new HashSet();

        String conf4 = "Y";

        System.out.println("Adicionar Lote de Produtos");

        do {
            String id = Console.readLine("ID do novo lote?");
            
            pls.add(new ProductLot(id, new HashMap()));

            conf4 = Console.readLine("Adicionar Mais Lotes? (Y/N)");
            
        } while (conf4.equalsIgnoreCase("y"));

        try {
            if (conf.equalsIgnoreCase("Y") && conf3.equalsIgnoreCase("Y")) {
                this.poController.addProductionOrder(ID,orderEmissionDate, previewFinalDate, prod, pl, poIdEncomenda, OrderStatus.ACCEPTED, amount, pls);
            }
            if (conf.equalsIgnoreCase("N") && conf3.equalsIgnoreCase("Y")) {
                this.poController.addProductionOrder(ID,orderEmissionDate, null, prod, pl, poIdEncomenda, OrderStatus.ACCEPTED, amount, pls);
            }
            if (conf.equalsIgnoreCase("Y") && conf3.equalsIgnoreCase("N")) {
                this.poController.addProductionOrder(ID,orderEmissionDate, previewFinalDate, prod, null, poIdEncomenda, OrderStatus.ACCEPTED, amount, pls);

            }
            if (conf.equalsIgnoreCase("N") && conf3.equalsIgnoreCase("N")) {
                this.poController.addProductionOrder(ID,orderEmissionDate, null, prod, null, poIdEncomenda, OrderStatus.ACCEPTED, amount, pls);
            }

        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Product which already exists in the database.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Add Production Order";
    }
}
