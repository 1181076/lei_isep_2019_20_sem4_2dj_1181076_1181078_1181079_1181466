/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import lapr4.productmanagement.application.AddProductCSVController;

/**
 *
 * @author mxlsa
 */
public class AddProductCSVUI extends AbstractUI {

    final AddProductCSVController controller = new AddProductCSVController();
    @Override
    protected boolean doShow() {
        final String fileName = Console.readLine("Nome/Localização do Ficheiro");
        controller.read(fileName);
        return false;
    }

    @Override
    public String headline() {
        return "Add Product CSV";
    }
    
}
