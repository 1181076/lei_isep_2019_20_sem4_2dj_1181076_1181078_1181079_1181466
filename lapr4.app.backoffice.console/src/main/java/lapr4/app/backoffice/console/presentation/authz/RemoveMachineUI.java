/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import lapr4.factorymachines.application.RemoveMachineController;

/**
 *
 * @author vrmpe
 */
public class RemoveMachineUI extends AbstractUI {

    private final RemoveMachineController theController = new RemoveMachineController();    
    private final ListMachineUI theUI = new ListMachineUI();
    
    @Override
    protected boolean doShow() {
        
        theUI.elements();
        
        final long id = Console.readInteger("ID da Máquina");
        
        try{
             this.theController.removeMachine(id);
             
             } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Machine that doesn't exist.");
        }

        return false;
    }
   
    @Override
    public String headline() {
        return "Remove Machine";
    }
}
