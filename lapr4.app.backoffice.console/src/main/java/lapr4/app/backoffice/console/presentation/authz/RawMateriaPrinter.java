/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import lapr4.productmanagement.domain.RawMateria;

/**
 *
 * @author Leticia
 */
public class RawMateriaPrinter implements Visitor<RawMateria> {

    @Override
    public void visit(final RawMateria visitee) {
        System.out.println("\n\n\n---------------------------------------");
        System.out.println("ID:            "+ visitee.getId());
        System.out.println("Internal Code: "+ visitee.getCode().getCode());
        System.out.println("Category:      "+ visitee.getCat().getCateg());
        System.out.println("Description:   "+ visitee.getDesc().getDesc());
        System.out.println("---------------------------------------");
    }
    
}
