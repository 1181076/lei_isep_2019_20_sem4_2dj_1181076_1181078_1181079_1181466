/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import lapr4.productmanagement.application.DefineRawMateriaCategoryController;

/**
 *
 * @author Leticia
 */
public class DefineRawMateriaCategoryUI extends AbstractUI {

    private final DefineRawMateriaCategoryController theController = new DefineRawMateriaCategoryController();

    @Override
    protected boolean doShow() {
        final String cat = Console.readLine("Category:");

        try {
            this.theController.defineRawMateriaCategory(cat);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("That category is already in use.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Define Raw Materia Category";
    }
}
