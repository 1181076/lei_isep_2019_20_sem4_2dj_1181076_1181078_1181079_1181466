/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import lapr4.productionmanagement.application.ArchiveNotificationsController;
import lapr4.productionmanagement.application.RemoveNotificationsController;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productmanagement.domain.Product;

/**
 *
 * @author vrmpe
 */
public class ArchiveNotificationUI extends AbstractUI {

    private final ArchiveNotificationsController theController = new ArchiveNotificationsController();
    private final ListAllNotificationsUI theUI = new ListAllNotificationsUI();

    @Override
    protected boolean doShow() {

        String yn = "yes";

        do {

            final Iterable<Notification> prods = this.theUI.elements();
            final SelectWidget<Notification> selector2 = new SelectWidget<>("Notifications:", prods, new NotificationPrinter());
            selector2.show();
            Notification n = selector2.selectedElement();

            try {

                theController.ArchiveNotifications(n);

            } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
                System.out.println("You tried to enter a Notification that doesn't exist.");
            }

            yn = Console.readLine("Do you want do archive another notification (Write 'yes' if you do)");

        } while ("yes".equalsIgnoreCase(yn) || "y".equalsIgnoreCase(yn));

        return false;
    }

    @Override
    public String headline() {
        return "Archive Notification";
    }

}
