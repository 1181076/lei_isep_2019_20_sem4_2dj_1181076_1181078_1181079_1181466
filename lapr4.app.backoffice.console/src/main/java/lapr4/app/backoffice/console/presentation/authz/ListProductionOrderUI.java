/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.productionmanagement.application.ListProductionOrderController;
import lapr4.productionmanagement.domain.ProductionOrder;

/**
 *
 * @author Leticia
 */
public class ListProductionOrderUI extends AbstractListUI<ProductionOrder>{
  private final ListProductionOrderController theController = new ListProductionOrderController();

    @Override
    protected Iterable<ProductionOrder> elements() {
        return this.theController.listProductionOrders();
    }

    @Override
    protected Visitor<ProductionOrder> elementPrinter() {
        return new ProductionOrderPrinter();
    }

    @Override
    protected String elementName() {
        return "Production Order";
    }

    @Override
    protected String listHeader() {
        return "PRODUCTION ORDERS";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    public String headline() {
        return "List Production Orders";
    }   
}
