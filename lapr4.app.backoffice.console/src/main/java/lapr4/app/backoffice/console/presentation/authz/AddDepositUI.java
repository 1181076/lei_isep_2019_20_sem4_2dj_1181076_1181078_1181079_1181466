/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.util.HashMap;
import java.util.Map;
import lapr4.productmanagement.application.AddDepositController;
import lapr4.productmanagement.application.ListProductsController;
import lapr4.productmanagement.domain.DepositType;
import lapr4.productmanagement.domain.Product;

/**
 *
 * @author Leticia
 */
public class AddDepositUI extends AbstractUI {

    private final AddDepositController theController = new AddDepositController();
    private final ListProductsController pController = new ListProductsController();
    private Map<Product,Integer> mP = new HashMap();

    @Override
    protected boolean doShow() {
        final String id = Console.readLine("ID of Deposit");
        final String dt = Console.readLine("DepositType (ENTRY ou EXIT)");
        DepositType dpt = DepositType.valueOf(dt);

        final String desc = Console.readLine("Description");        

        
        String conf2 = Console.readLine("Add products to deposit? (Y/N)");
        
        while(!conf2.equalsIgnoreCase(("N"))){
           addProduct();           
           conf2 = Console.readLine("Add products to deposit? (Y/N)");
        
        }
        
        
        
        try {
            this.theController.addDeposit(id, desc,dpt, mP);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a deposit which already exists in the database.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Add Deposit";
    }

    private void addProduct() {
        final Iterable<Product> products = this.pController.allProducts();

        final SelectWidget<Product> selector = new SelectWidget<>("Select Product:", products,
                new ProductPrinter());
        selector.show();
        final Product chosen = selector.selectedElement();
        
        final int am = Console.readInteger("Amount?");
        
        if (mP.containsKey(chosen)) {
                mP.replace(chosen, am + mP.get(chosen));
            } else {
                mP.put(chosen, am);
            }
        
    }

}
