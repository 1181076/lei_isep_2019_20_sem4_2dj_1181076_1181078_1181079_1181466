/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import lapr4.messagemanagement.domain.application.RemoveMessageController;

/**
 *
 * @author migue
 */
public class RemoveMessageUI extends AbstractUI {

    private final RemoveMessageController theController = new RemoveMessageController();    
    private final ListMessageUI theUI = new ListMessageUI();
    
    @Override
    protected boolean doShow() {
        
        theUI.elements();
        
        final long id = Console.readInteger("ID da Message");
        
        try{
             this.theController.removeMessage(id);
             
             } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Message that doesn't exist.");
        }

        return false;
    }
   
    @Override
    public String headline() {
        return "Remove Message";
    }
}
