/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.repositories.ProductRepository;

/**
 *
 * @author migue
 */
public class ProductionSheetPrinter implements Visitor<ProductionSheet> {

    private final ProductRepository materialRepository = PersistenceContext.repositories().products();
    private Map<Product, Integer> set = new HashMap();
    private Entry<Product, Integer> ma;

    @Override
    public void visit(final ProductionSheet visitee) {
        set = visitee.getAmount();
        Iterator<Entry<Product, Integer>> it = set.entrySet().iterator();

        System.out.println("\n\n\n==========================================");
        System.out.println("ID:            " + visitee.getId());
        for (int i = 0; i < set.size(); i++) {
            System.out.println("-------------------------------------------");
            ma = it.next();
            System.out.println("MaterialID:  " + ma.getKey().getId());
            System.out.println("Amount:      " + ma.getValue());
            System.out.println("-------------------------------------------");
        }
        System.out.println("===========================================");

    }

}
