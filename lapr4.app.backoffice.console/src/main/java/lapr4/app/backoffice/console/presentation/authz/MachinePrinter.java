/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import lapr4.factorymachines.domain.Machine;

/**
 *
 * @author vrmpe
 */
@SuppressWarnings("squid:S106")
public class MachinePrinter implements Visitor<Machine> {

    @Override
    public void visit(final Machine visitee) {
        
        System.out.println("\n\n\n---------------------------------------");
        System.out.println("MachineID:                " + visitee.getId());
        System.out.println("Series Number:            " + visitee.getNumSerie());
        System.out.println("Description:              " + visitee.getDesc().getDesc());
        System.out.println("Installation Date:        " + visitee.getInstallationDate().toString());
        System.out.println("Brand:                    " + visitee.getBrand().getBrandName());
        System.out.println("Model:                    " + visitee.getModel().getModel());
        System.out.println("Manufacturer:             " + visitee.getMan().getName());
        System.out.println("Protocol:                 " + visitee.getProt().getProtocol());
        if(visitee.getFile()!= null){
            System.out.println("Configuration File:       " + visitee.getFile().getFile());
        }else{
            System.out.println("No Configuration File ");
        }
        System.out.println("---------------------------------------");
    }

}
