/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import lapr4.productmanagement.domain.Product;

/**
 *
 * @author vrmpe
 */
public class ProductPrinter implements Visitor<Product> {

    public ProductPrinter() {
    }

    @Override
    public void visit(Product visitee) {
        System.out.println("\n\n\n---------------------------------------");
        System.out.println("ProductID:                       "+ visitee.getId());
        System.out.println("Product Commercial Code:         "+ visitee.getProductCommercialCode().getCode());
        System.out.println("Product Description Brief:       "+ visitee.getProductDescriptionBrief().getDesc());
        System.out.println("Product Description Complete:    "+ visitee.getProductDescriptionComplete().getDesc());
        System.out.println("Product Manufacture Code:        "+ visitee.getProductManufactureCode().getCode());
        if(visitee.getRawMaterial()!=null){
        System.out.println("Equivelent Raw Material ID:      "+ visitee.getRawMaterial().getId());
        }else{
        System.out.println("No Raw Material associated");
        }
        if(visitee.getProductSheet()!=null){
        System.out.println("Product Sheet ID:                "+ visitee.getProductSheet().getId());
        }else{
        System.out.println("No Production Sheet associated");
        }
        System.out.println("---------------------------------------");
    }
    
}
