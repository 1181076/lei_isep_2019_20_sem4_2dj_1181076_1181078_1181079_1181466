/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.util.Console;
import eapli.framework.visitor.Visitor;
import java.util.ArrayList;
import java.util.List;
import lapr4.productionmanagement.application.ConsultNotificationsWithTreatmentController;
import lapr4.productionmanagement.domain.ErrorType;
import lapr4.productionmanagement.domain.Notification;

/**
 *
 * @author migue
 */
public class ConsultNotificationsWithTreatmentUI extends AbstractListUI<Notification> {

    private final ConsultNotificationsWithTreatmentController theController = new ConsultNotificationsWithTreatmentController();

    @Override
    protected Iterable<Notification> elements() {
        String ar = Console.readLine("List Archived Notifications:"
                + "1 - regarding non-existing products in messages?\n"
                + "2 - regarding non-existing deposits in messages?\n"
                + "3 - regarding non-existing product lots in messages?\n"
                + "4 - regarding shortage of products?\n"
                + "5 - list all\n");

        switch (ar) {
            case "1":
                return this.theController.consultNotificationsWithTreatment(ErrorType.NON_EXISTENT_PRODUCT);
            case "2":
                return this.theController.consultNotificationsWithTreatment(ErrorType.NON_EXISTENT_DEPOSIT);
            case "3":
                return this.theController.consultNotificationsWithTreatment(ErrorType.NON_EXISTENT_LOT);
            case "4":
                return this.theController.consultNotificationsWithTreatment(ErrorType.NOT_ENOUGH_PRODUCT);
            case "5":
                return this.theController.consultNotificationsWithTreatment();

        }
        return this.theController.consultNotificationsWithTreatment();
    }

    @Override
    protected Visitor<Notification> elementPrinter() {
        return new NotificationPrinter();
    }

    @Override
    protected String elementName() {
        return "Notification With Treatment";
    }

    @Override
    protected String listHeader() {
        return "NOTIFICATIONS WITH TREATMENT";
    }

    @Override
    protected String emptyMessage() {
        return "A lista está vazia";
    }

    @Override
    public String headline() {
        return "Notification With Treatment";
    }
}
