/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.productionmanagement.application.ListAllNotificationsController;
import lapr4.productionmanagement.domain.Notification;

/**
 *
 * @author vrmpe
 */
public class ListAllNotificationsUI extends AbstractListUI<Notification>{
    
    private final ListAllNotificationsController theController = new ListAllNotificationsController();

    @Override
    protected Iterable<Notification> elements() {
        return this.theController.listAllNotifications();
    }

    @Override
    protected Visitor<Notification> elementPrinter() {
        return new NotificationPrinter();
    }

    @Override
    protected String elementName() {
        return "Notification";
    }

    @Override
    protected String listHeader() {
        return "ALL NOTIFICATIONS";
    }

    @Override
    protected String emptyMessage() {
        return "A lista está vazia";
    }

    @Override
    public String headline() {
        return "Notification";
    }
    
}
