/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.util.Console;
import eapli.framework.visitor.Visitor;
import lapr4.productionmanagement.application.ListProductionOrderByIdEncomendaController;
import lapr4.productionmanagement.domain.ProductionOrder;

/**
 *
 * @author Leticia
 */
public class ListProductionOrderByIdEncomendaUI extends AbstractListUI<ProductionOrder> {

    private final ListProductionOrderByIdEncomendaController theController = new ListProductionOrderByIdEncomendaController();

    @Override
    protected Iterable<ProductionOrder> elements() {
        
        final String idEncomenda = Console.readLine("Id Encomenda");
        return this.theController.productionOrderByIdEncomenda(idEncomenda);
    }

    @Override
    protected Visitor<ProductionOrder> elementPrinter() {
        return new ProductionOrderPrinter();
    }

    @Override
    protected String elementName() {
        return "Production Oerder by Id Encomenda";
    }

    @Override
    protected String listHeader() {
        return "PRODUCTION ORDER BY ID ENCOMENDA";
    }

    @Override
    protected String emptyMessage() {
        return "A lista está vazia";
    }

    @Override
    public String headline() {
        return "Production Order By Id Encomenda";
    }

    

}
