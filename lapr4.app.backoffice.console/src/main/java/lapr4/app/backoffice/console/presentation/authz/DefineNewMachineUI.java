/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.util.Date;
import lapr4.factorymachines.application.DefineNewMachineController;
import lapr4.factorymachines.application.ListProductionLineController;
import lapr4.factorymachines.domain.MachineBrand;
import lapr4.factorymachines.domain.MachineDescript;
import lapr4.factorymachines.domain.MachineModel;
import lapr4.factorymachines.domain.Manufacturer;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.domain.Protocol;

/**
 *
 * @author vrmpe
 */
public class DefineNewMachineUI extends AbstractUI {

    private final DefineNewMachineController theController = new DefineNewMachineController();
    private final ListProductionLineController lController = new ListProductionLineController();

    @Override
    protected boolean doShow() {
        final String numSerie = Console.readLine("Número de Série");
        final String desc = Console.readLine("Descrição");
        MachineDescript descript = new MachineDescript(desc);
        final String data = Console.readLine("Data (AAAA/MM/DD)");
        Date data1 = new Date(data);
        final String prot = Console.readLine("Protocolo");
        Protocol protocol = new Protocol(prot);
        final String brand = Console.readLine("Machine Brand");
        MachineBrand machineBrand = new MachineBrand(brand);
        final String model = Console.readLine("Machine Model");
        MachineModel machineModel = new MachineModel(model);
        final String man = Console.readLine("Manufacturer");
        Manufacturer manufacturer = new Manufacturer(man);

        final Iterable<ProductionLine> pls = this.lController.allProductionLines();

        final SelectWidget<ProductionLine> selector = new SelectWidget<>("Select ProductionLine:", pls,
                new ProductionLinePrinter());
        selector.show();
        final ProductionLine chosen = selector.selectedElement();

        try {
            this.theController.addMachine(numSerie, protocol, descript, data1, machineBrand, machineModel, manufacturer, chosen);

        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Machine which already exists in the database.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Define Machine";
    }

}
