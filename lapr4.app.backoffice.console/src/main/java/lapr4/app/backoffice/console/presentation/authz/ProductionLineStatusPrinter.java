/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import java.util.Iterator;
import java.util.Set;
import lapr4.factorymachines.domain.Machine;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.MachineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;

/**
 *
 * @author Leticia
 */
public class ProductionLineStatusPrinter implements Visitor<ProductionLine> {

    @Override
    public void visit(final ProductionLine visitee) {

        System.out.println("\n\n\n==========================================================");
        System.out.println("Production Line ID:                     " + visitee.getId());
        System.out.println("Production Line Status:                 " + visitee.isProcesseable());
        System.out.println("Production Line last processed date:    " + visitee.getLastProcessed());
        System.out.println("==========================================================");
        System.out.println("==========================================================");
    }
}
