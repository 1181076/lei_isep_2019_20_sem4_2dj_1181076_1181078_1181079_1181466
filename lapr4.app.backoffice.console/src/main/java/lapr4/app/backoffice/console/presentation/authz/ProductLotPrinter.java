/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import java.util.Iterator;
import java.util.Map;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.Deposit;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductLot;
import lapr4.productmanagement.domain.repositories.ProductRepository;

/**
 *
 * @author migue
 */
public class ProductLotPrinter implements Visitor<ProductLot> {
    private final ProductRepository productRepository = PersistenceContext.repositories().products();
    
    @Override
    public void visit(final ProductLot visitee) {
        System.out.println("\n\n\n---------------------------------------");
        System.out.println("ProductLotID:      " + visitee.getId());
        System.out.println("---------------------------------------");
        Map<Product,Integer> mP = visitee.getAmount();
        Iterator<Map.Entry<Product, Integer>> sP = mP.entrySet().iterator();
        ProductPrinter p = new ProductPrinter();
        Product prod;
        int nP;
        int nM;
        Map.Entry<Product, Integer> pAm;

        System.out.println("===============Products====================");
        for (int i = 0; i < mP.size(); i++) {
            pAm = sP.next();
            prod = pAm.getKey();

            System.out.println("---------------------------------------");
            System.out.println("ProductID:                       " + prod.getId());
            System.out.println("Product Commercial Code:         " + prod.getProductCommercialCode().getCode());
            System.out.println("Product Description Brief:       " + prod.getProductDescriptionBrief().getDesc());
            System.out.println("Product Description Complete:    " + prod.getProductDescriptionComplete().getDesc());
            System.out.println("Product Manufacture Code:        " + prod.getProductManufactureCode().getCode());
            System.out.println("Equivelent Raw Material ID:      " + prod.getRawMaterial().getId());
            System.out.println("Product Sheet ID:                " + prod.getProductSheet());
            System.out.println("AMOUNT:                          " + pAm.getValue());
            System.out.println("---------------------------------------");
        }

        System.out.println("===========================================");


        System.out.println("===========================================");
    }

}
