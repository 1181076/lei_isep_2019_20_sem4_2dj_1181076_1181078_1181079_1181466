/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.util.HashMap;
import java.util.Map;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.application.AddProductController;
import lapr4.productmanagement.application.SpecifyProductionSheetForAProductController;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductCommercialCode;
import lapr4.productmanagement.domain.ProductDescriptionBrief;
import lapr4.productmanagement.domain.ProductDescriptionComplete;
import lapr4.productmanagement.domain.ProductManufactureCode;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.RawMateria;
import lapr4.productmanagement.domain.repositories.ProductRepository;

/**
 *
 * @author mxlsa
 */
public class AddProductUI extends AbstractUI {

    private final AddProductController controller = new AddProductController();
    private final SpecifyProductionSheetForAProductController theController = new SpecifyProductionSheetForAProductController();
    private final ProductRepository productRepository = PersistenceContext.repositories().products();

    @Override
    protected boolean doShow() {
        final String pManufacturerCode = Console.readLine("Código Fabricante");
        final String pComercialCode = Console.readLine("Código Comercial");
        final String pDescriptionBrief = Console.readLine("Descrição Breve");
        final String pDescriptionComplete = Console.readLine("Descrição Completa");

        RawMateria rawMat = new RawMateria();

        final String conf2 = Console.readLine("Adicionar Raw Materia? (Y/N)");

        if (conf2.equalsIgnoreCase("Y")) {

            final Iterable<RawMateria> rawMats = this.controller.rawMaterialsWithoutProducts();
            final SelectWidget<RawMateria> selector2 = new SelectWidget<>("Raw Material:", rawMats, new RawMateriaPrinter());
            selector2.show();
            rawMat = selector2.selectedElement();

        }

        final String conf = Console.readLine("Adicionar Production Sheet? (Y/N)");        
        final String ids = Console.readLine("ID da Production Sheet");
        ProductionSheet productionSheet = new ProductionSheet();
        if (conf.equalsIgnoreCase("Y")) {
            Map<Product, Integer> maSet = new HashMap();

            String a = "Y";

            while (a.equalsIgnoreCase("Y")) {
                System.out.println("Material da Ficha de Produção");

                final Iterable<Product> materials = this.productRepository.findAll();

                final SelectWidget<Product> selector = new SelectWidget<>("Material:", materials, new ProductPrinter());

                selector.show();

                final Product material = selector.selectedElement();

                final int sheet2 = Console.readInteger("Quantidade da Ficha de Produção");

                if (maSet.containsKey(material)) {
                    maSet.replace(material, sheet2 + maSet.get(material));
                } else {
                    maSet.put(material, sheet2);
                }

                a = Console.readLine("Mais Materiais? (Y/N)");
            }

            productionSheet = new ProductionSheet(ids, maSet);
        }

        try {
            if (conf.equalsIgnoreCase("Y") && conf2.equalsIgnoreCase("Y")) {
                this.theController.specifyProductionSheetForAProduct(productionSheet);
                this.controller.addProduct(new ProductManufactureCode(pManufacturerCode), new ProductCommercialCode(pComercialCode), new ProductDescriptionBrief(pDescriptionBrief), new ProductDescriptionComplete(pDescriptionComplete), productionSheet, rawMat);
            }
            if (conf.equalsIgnoreCase("N") && conf2.equalsIgnoreCase("Y")) {
                this.controller.addProduct(new ProductManufactureCode(pManufacturerCode), new ProductCommercialCode(pComercialCode), new ProductDescriptionBrief(pDescriptionBrief), new ProductDescriptionComplete(pDescriptionComplete), null, rawMat);
            }
            if (conf.equalsIgnoreCase("Y") && conf2.equalsIgnoreCase("N")) {
                this.theController.specifyProductionSheetForAProduct(productionSheet);
                this.controller.addProduct(new ProductManufactureCode(pManufacturerCode), new ProductCommercialCode(pComercialCode), new ProductDescriptionBrief(pDescriptionBrief), new ProductDescriptionComplete(pDescriptionComplete), productionSheet, null);
            }
            if (conf.equalsIgnoreCase("N") && conf2.equalsIgnoreCase("N")) {
                this.controller.addProduct(new ProductManufactureCode(pManufacturerCode), new ProductCommercialCode(pComercialCode), new ProductDescriptionBrief(pDescriptionBrief), new ProductDescriptionComplete(pDescriptionComplete), null, null);
            }

        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Product which already exists in the database.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Add Product";
    }

}
