/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.productmanagement.application.ListDepositController;
import lapr4.productmanagement.domain.Deposit;

/**
 *
 * @author Leticia
 */
public class ListDepositUI extends AbstractListUI<Deposit> {
    
    private final ListDepositController theController = new ListDepositController();

    @Override
    protected Iterable<Deposit> elements() {
        return this.theController.listDeposit();
    }

    @Override
    protected Visitor<Deposit> elementPrinter() {
        return new DepositPrinter();
    }

    @Override
    protected String elementName() {
        return "Deposit";
    }

    @Override
    protected String listHeader() {
        return "DEPOSITS";
    }

    @Override
    protected String emptyMessage() {
        return "A lista está vazia";
    }

    @Override
    public String headline() {
        return "Deposits";
    }   
    
}
