/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import eapli.framework.visitor.Visitor;
import lapr4.productmanagement.application.AddRawMateriaController;
import lapr4.productmanagement.application.ListRawMateriaController;
import lapr4.productmanagement.application.RemoveRawMateriaController;
import lapr4.app.backoffice.console.presentation.authz.ListRawMateriaUI;
import lapr4.productmanagement.domain.RawMateria;

/**
 *
 * @author Leticia
 */
public class RemoveRawMateriaUI extends AbstractUI {

    private final RemoveRawMateriaController theController = new RemoveRawMateriaController();
    private final ListRawMateriaUI lu = new ListRawMateriaUI();

    @Override
    protected boolean doShow() {

        lu.show();

        final long id = Console.readLong("ID");
        try {
            this.theController.removeRawMateria(id);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to remove a raw materia which not exists in the database.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Remove Raw Materia";
    }
}
