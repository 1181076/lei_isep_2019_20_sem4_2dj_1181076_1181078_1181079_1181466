/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import lapr4.productmanagement.domain.RawMateriaCategory;

/**
 *
 * @author Leticia
 */
public class RawMateriaCategoryPrinter implements Visitor<RawMateriaCategory> {

    @Override
    public void visit(final RawMateriaCategory visitee) {
        System.out.println("\n\n\n---------------------------------------");
        System.out.println("ID:            "+ visitee.getId());
        System.out.println("Category:      "+ visitee.getCateg());
        System.out.println("---------------------------------------");
    }
    
}
