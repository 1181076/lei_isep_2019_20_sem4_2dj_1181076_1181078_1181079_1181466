package lapr4.app.backoffice.console.presentation;

import lapr4.app.backoffice.console.presentation.authz.ListProductLotsAction;
import lapr4.app.backoffice.console.presentation.authz.ConsultOrderStateAction;
import lapr4.app.backoffice.console.presentation.authz.ExportToXMLAction;
import lapr4.app.common.console.presentation.authz.MyUserMenu;
import lapr4.Application;
import lapr4.app.backoffice.console.presentation.authz.AddUserUI;
import lapr4.app.backoffice.console.presentation.authz.DeactivateUserAction;
import lapr4.app.backoffice.console.presentation.authz.ListUsersAction;
import lapr4.usermanagement.domain.FactoryRoles;
import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import eapli.framework.presentation.console.menu.VerticalMenuRenderer;
import lapr4.app.backoffice.console.presentation.authz.AddDepositUI;
import lapr4.app.backoffice.console.presentation.authz.AddOrdersCSVAction;
import lapr4.app.backoffice.console.presentation.authz.AddProductCSVUI;
import lapr4.app.backoffice.console.presentation.authz.AddProductUI;
import lapr4.app.backoffice.console.presentation.authz.AddProductionOrderAction;
import lapr4.app.backoffice.console.presentation.authz.AddRawMateriaUI;
import lapr4.app.backoffice.console.presentation.authz.ArchiveNotificationAction;
import lapr4.app.backoffice.console.presentation.authz.ConsultNotificationsWithTreatmentAction;
import lapr4.app.backoffice.console.presentation.authz.ConsultNotificationsWithoutTreatmentAction;
import lapr4.app.backoffice.console.presentation.authz.ConsultProductsWithoutProductionSheetAction;
import lapr4.app.backoffice.console.presentation.authz.DefineNewMachineUI;
import lapr4.app.backoffice.console.presentation.authz.DefineRawMateriaCategoryUI;
import lapr4.app.backoffice.console.presentation.authz.ListAllNotificationsAction;
import lapr4.app.backoffice.console.presentation.authz.ListDepositAction;
import lapr4.app.backoffice.console.presentation.authz.ListMachineAction;
import lapr4.app.backoffice.console.presentation.authz.ListMessageAction;
import lapr4.app.backoffice.console.presentation.authz.ListProductionLineAction;
import lapr4.app.backoffice.console.presentation.authz.ListProductionOrderAction;
import lapr4.app.backoffice.console.presentation.authz.ListProductionOrderByIdEncomendaAction;
import lapr4.app.backoffice.console.presentation.authz.ListProductionSheetsAction;
import lapr4.app.backoffice.console.presentation.authz.ListProductsAction;
import lapr4.app.backoffice.console.presentation.authz.ListRawMateriaAction;
import lapr4.app.backoffice.console.presentation.authz.ListRawMateriaCategoryAction;
import lapr4.app.backoffice.console.presentation.authz.RegisterProductionLineUI;
import lapr4.app.backoffice.console.presentation.authz.RemoveDepositAction;
import lapr4.app.backoffice.console.presentation.authz.RemoveMachineAction;
import lapr4.app.backoffice.console.presentation.authz.RemoveMessageAction;
import lapr4.app.backoffice.console.presentation.authz.RemoveNotificationsAction;
import lapr4.app.backoffice.console.presentation.authz.RemoveProductionLineAction;
import lapr4.app.backoffice.console.presentation.authz.RemoveProductionOrderAction;
import lapr4.app.backoffice.console.presentation.authz.RemoveProductsAction;
import lapr4.app.backoffice.console.presentation.authz.RemoveRawMateriaAction;
import lapr4.app.backoffice.console.presentation.authz.RemoveRawMateriaCategoryAction;
import lapr4.app.backoffice.console.presentation.authz.SendConfigurationAction;
import lapr4.app.backoffice.console.presentation.authz.SetConfigFilesAction;
import lapr4.app.backoffice.console.presentation.authz.SetProcessingStatusAction;
import lapr4.app.backoffice.console.presentation.authz.SpecifyProductionSheetForAProductUI;

public class MainMenu extends AbstractUI {

    private static final String RETURN_LABEL = "Return ";

    private static final int EXIT_OPTION = 0;

    // USERS
    private static final int ADD_USER_OPTION = 1;
    private static final int LIST_USERS_OPTION = 2;
    private static final int DEACTIVATE_USER_OPTION = 3;

    // SETTINGS
    private static final int TO_BE_ADDED = 1;

    // RAW-MATERIA-CATEGORY
    private static final int RAW_MATERIA_CATEGORY_REGISTER_OPTION = 1;
    private static final int RAW_MATERIA_CATEGORY_LIST_OPTION = 2;
    private static final int RAW_MATERIA_CATEGORY_REMOVE_OPTION = 3;

    // RAW_MATERIA
    private static final int RAW_MATERIA_REGISTER_OPTION = 1;
    private static final int RAW_MATERIA_LIST_OPTION = 2;
    private static final int RAW_MATERIA_REMOVE_OPTION = 3;

    // PRODUCTION_SHEET
    private static final int PRODUCTION_SHEET_REGISTER_OPTION = 1;
    private static final int PRODUCTION_SHEET_LIST_OPTION = 2;
    private static final int PRODUCTION_SHEET_REMOVE_OPTION = 3;

    // PRODUCT
    private static final int PRODUCT_REGISTER_OPTION = 1;
    private static final int PRODUCT_LIST_OPTION = 2;
    private static final int PRODUCT_LIST_WITHOUT_SHEET_OPTION = 3;
    private static final int PRODUCT_REMOVE_OPTION = 4;
    private static final int PRODUCT_IMPORT_CSV_OPTION = 5;

    // MATERIALS
    private static final int PRODUCT_LOT_LIST_OPTION = 1;
    private static final int PRODUCT_LOT_ADD_CONTENT_OPTION = 2;
    private static final int PRODUCT_LOT_REMOVE_CONTENT_OPTION = 3;

    // MACHINE
    private static final int MACHINE_REGISTER_OPTION = 1;
    private static final int MACHINE_LIST_OPTION = 2;
    private static final int MACHINE_REMOVE_OPTION = 3;
    private static final int MACHINE_ADD_CONFIGURATION_FILE_OPTION = 4;
    private static final int OPEN_MACHINE_CONFIG_SERVER = 5;

    // PRODUCTION_LINE
    private static final int PRODUCTION_LINE_REGISTER_OPTION = 1;
    private static final int PRODUCTION_LINE_LIST_OPTION = 2;
    private static final int PRODUCTION_LINE_PROCESSABLE_OPTION = 3;
    private static final int PRODUCTION_LINE_REMOVE_OPTION = 4;

    // DEPOSIT
    private static final int DEPOSIT_REGISTER_OPTION = 1;
    private static final int DEPOSIT_LIST_OPTION = 2;
    private static final int DEPOSIT_REMOVE_OPTION = 3;

    // PRODUCTION_ORDER
    private static final int PRODUCTION_ORDER_REGISTER_OPTION = 1;
    private static final int PRODUCTION_ORDER_IMPORT_CSV_OPTION = 2;
    private static final int PRODUCTION_ORDER_LIST_OPTION = 3;
    private static final int PRODUCTION_ORDER_LIST_BY_STATE_OPTION = 4;
    private static final int PRODUCTION_ORDER_LIST_BY_COMISSION_OPTION = 5;
    private static final int PRODUCTION_ORDER_REMOVE_OPTION = 6;

    // NOTIFICATION
    private static final int NOTIFICATION_LIST_OPTION = 1;
    private static final int NOTIFICATION_BY_ORDER_LIST_OPTION = 2;
    private static final int NOTIFICATION_TREATED_LIST_OPTION = 3;
    private static final int NOTIFICATION_TBD_LIST_OPTION = 4;
    private static final int NOTIFICATION_SET_AS_TREATED_OPTION = 5;
    private static final int NOTIFICATION_REMOVE_OPTION = 6;

    // MESSAGE
    private static final int MESSAGE_LIST_OPTION = 1;
    private static final int MESSAGE_REMOVE_OPTION = 6;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int USERS_OPTION = 2;
    private static final int SETTINGS_OPTION = 3;
    private static final int RAW_MATERIA_OPTION = 4;
    private static final int RAW_MATERIA_CATEGORY_OPTION = 5;
    private static final int PRODUCTION_SHEET_OPTION = 6;
    private static final int PRODUCT_OPTION = 7;
    private static final int PRODUCT_LOT_OPTION = 8;
    private static final int MACHINE_OPTION = 9;
    private static final int PRODUCTION_LINE_OPTION = 10;
    private static final int DEPOSIT_OPTION = 11;
    private static final int PRODUCTION_ORDER_OPTION = 12;
    private static final int NOTIFICATION_OPTION = 13;
    private static final int MESSAGE_OPTION = 14;
    private static final int MESSAGE_TYPE_OPTION = 15;
    private static final int XML_OPTION = 20;

    private static final String SEPARATOR_LABEL = "--------------";

    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (Application.settings().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        } else {
            renderer = new VerticalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        }
        return renderer.render();
    }

    @Override
    public String headline() {

        return authz.session().map(s -> "eFACTORY [ @" + s.authenticatedUser().identity() + " ]")
                .orElse("eFACTORY [ ==Anonymous== ]");
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.addSubMenu(MY_USER_OPTION, myUserMenu);

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        }

        if (authz.isAuthenticatedUserAuthorizedTo(FactoryRoles.POWER_USER,
                FactoryRoles.ADMIN)) {
            final Menu usersMenu = buildUsersMenu();
            mainMenu.addSubMenu(USERS_OPTION, usersMenu);
            final Menu settingsMenu = buildAdminSettingsMenu();
            mainMenu.addSubMenu(SETTINGS_OPTION, settingsMenu);
        }

        if (authz.isAuthenticatedUserAuthorizedTo(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_PRODUCAO)) {
            final Menu rawMateriaMenu = buildRawMateriaMenu();
            mainMenu.addSubMenu(RAW_MATERIA_OPTION, rawMateriaMenu);
            final Menu rawMateriaCategoryMenu = buildRawMateriaCategoryMenu();
            mainMenu.addSubMenu(RAW_MATERIA_CATEGORY_OPTION, rawMateriaCategoryMenu);
            final Menu productionSheetMenu = buildProductionSheetMenu();
            mainMenu.addSubMenu(PRODUCTION_SHEET_OPTION, productionSheetMenu);
            final Menu productMenu = buildProductMenu();
            mainMenu.addSubMenu(PRODUCT_OPTION, productMenu);
            final Menu productLotMenu = buildProductLotMenu();
            mainMenu.addSubMenu(PRODUCT_LOT_OPTION, productLotMenu);
            final Menu pOMenu = buildProductionOrderMenu();
            mainMenu.addSubMenu(PRODUCTION_ORDER_OPTION, pOMenu);
            final Menu settingsMenu = buildAdminSettingsMenu();
        }

        if (authz.isAuthenticatedUserAuthorizedTo(FactoryRoles.POWER_USER,
                FactoryRoles.GESTOR_CHAO)) {
            final Menu machineMenu = buildMachineMenu();
            mainMenu.addSubMenu(MACHINE_OPTION, machineMenu);
            final Menu productionLineMenu = buildProductionLineMenu();
            mainMenu.addSubMenu(PRODUCTION_LINE_OPTION, productionLineMenu);
            final Menu depositMenu = buildDepositMenu();
            mainMenu.addSubMenu(DEPOSIT_OPTION, depositMenu);
            final Menu notifiMenu = buildNotificationsMenu();
            mainMenu.addSubMenu(NOTIFICATION_OPTION, notifiMenu);
            final Menu messMenu = buildMessageMenu();
            mainMenu.addSubMenu(MESSAGE_OPTION, messMenu);
            final Menu settingsMenu = buildAdminSettingsMenu();
        }

        final Menu xmlMenu = buildXMLMenu();
        mainMenu.addSubMenu(XML_OPTION, xmlMenu);

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        }

        mainMenu.addItem(EXIT_OPTION, "Exit", new ExitWithMessageAction("Bye, Bye"));

        return mainMenu;
    }

    private Menu buildAdminSettingsMenu() {
        final Menu menu = new Menu("Settings >");

        menu.addItem(TO_BE_ADDED, "TBA",
                new ShowMessageAction("Not implemented yet"));
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildUsersMenu() {
        final Menu menu = new Menu("Users >");

        menu.addItem(ADD_USER_OPTION, "Add User", new AddUserUI()::show);
        menu.addItem(LIST_USERS_OPTION, "List all Users", new ListUsersAction());
        menu.addItem(DEACTIVATE_USER_OPTION, "Deactivate User", new DeactivateUserAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildMachineMenu() {

        final Menu menu = new Menu("Machines >");

        menu.addItem(MACHINE_REGISTER_OPTION, "Register Machine", new DefineNewMachineUI()::show);
        menu.addItem(MACHINE_LIST_OPTION, "List all Machines", new ListMachineAction());
        menu.addItem(MACHINE_REMOVE_OPTION, "Remove Machine", new RemoveMachineAction());
        menu.addItem(MACHINE_ADD_CONFIGURATION_FILE_OPTION, "Add Configuration File to Machine", new SetConfigFilesAction());
        menu.addItem(OPEN_MACHINE_CONFIG_SERVER, "Open Server for Machine Configuration", new SendConfigurationAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildProductionLineMenu() {

        final Menu menu = new Menu("Production Lines >");

        menu.addItem(PRODUCTION_LINE_REGISTER_OPTION, "Register Production Line", new RegisterProductionLineUI()::show);
        menu.addItem(PRODUCTION_LINE_LIST_OPTION, "List all Production Line", new ListProductionLineAction());
        menu.addItem(PRODUCTION_LINE_PROCESSABLE_OPTION, "Set Production Line Processing Status as active/inactive", new SetProcessingStatusAction());
        menu.addItem(PRODUCTION_LINE_REMOVE_OPTION, "Remove Production Line", new RemoveProductionLineAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildDepositMenu() {

        final Menu menu = new Menu("Deposits >");

        menu.addItem(DEPOSIT_REGISTER_OPTION, "Register Deposit", new AddDepositUI()::show);
        menu.addItem(DEPOSIT_LIST_OPTION, "List all Deposit", new ListDepositAction());
        menu.addItem(DEPOSIT_REMOVE_OPTION, "Remove Deposit", new RemoveDepositAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildRawMateriaMenu() {

        final Menu menu = new Menu("Raw Materia >");

        menu.addItem(RAW_MATERIA_REGISTER_OPTION, "Register Raw Materia", new AddRawMateriaUI()::show);
        menu.addItem(RAW_MATERIA_LIST_OPTION, "List all Raw Materia", new ListRawMateriaAction());
        menu.addItem(RAW_MATERIA_REMOVE_OPTION, "Remove Raw Materia", new RemoveRawMateriaAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildRawMateriaCategoryMenu() {

        final Menu menu = new Menu("Raw Materia Category >");

        menu.addItem(RAW_MATERIA_CATEGORY_REGISTER_OPTION, "Register Raw Materia Category", new DefineRawMateriaCategoryUI()::show);
        menu.addItem(RAW_MATERIA_CATEGORY_LIST_OPTION, "List all Raw Materia Category", new ListRawMateriaCategoryAction());
        menu.addItem(RAW_MATERIA_CATEGORY_REMOVE_OPTION, "Remove Raw Materia Category", new RemoveRawMateriaCategoryAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildProductionSheetMenu() {

        final Menu menu = new Menu("Production Sheet >");

        menu.addItem(PRODUCTION_SHEET_REGISTER_OPTION, "Specify Production Sheet for Product", new SpecifyProductionSheetForAProductUI()::show);
        menu.addItem(PRODUCTION_SHEET_LIST_OPTION, "List all Production Sheets", new ListProductionSheetsAction());
        menu.addItem(PRODUCTION_SHEET_REMOVE_OPTION, "Remove Production Sheet",
                Actions.SUCCESS /**
         * new RemoveProductionSheetAction()*
         */
        );
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildProductMenu() {

        final Menu menu = new Menu("Product >");

        menu.addItem(PRODUCT_REGISTER_OPTION, "Register Product", new AddProductUI()::show);
        menu.addItem(PRODUCT_LIST_OPTION, "List all Products", new ListProductsAction());
        menu.addItem(PRODUCT_LIST_WITHOUT_SHEET_OPTION, "List Products without Production Sheet", new ConsultProductsWithoutProductionSheetAction());
        menu.addItem(PRODUCT_REMOVE_OPTION, "Remove Product", new RemoveProductsAction());
        menu.addItem(PRODUCT_IMPORT_CSV_OPTION, "Import Products from file", new AddProductCSVUI()::show);
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildProductLotMenu() {

        final Menu menu = new Menu("Materials >");

        menu.addItem(PRODUCT_LOT_REMOVE_CONTENT_OPTION, "List all Materials", new ListProductLotsAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildProductionOrderMenu() {

        final Menu menu = new Menu("Production Order >");

        menu.addItem(PRODUCTION_ORDER_REGISTER_OPTION, "Register Production Order", new AddProductionOrderAction());
        menu.addItem(PRODUCTION_ORDER_LIST_OPTION, "List all Production Orders", new ListProductionOrderAction());
        menu.addItem(PRODUCTION_ORDER_LIST_BY_STATE_OPTION, "List all Production Order by current state", new ConsultOrderStateAction());
        menu.addItem(PRODUCTION_ORDER_LIST_BY_COMISSION_OPTION, "List all Production Orders By Comission", new ListProductionOrderByIdEncomendaAction());
        menu.addItem(PRODUCTION_ORDER_IMPORT_CSV_OPTION, "Import Production Orders By CSV File", new AddOrdersCSVAction());
        menu.addItem(PRODUCTION_ORDER_REMOVE_OPTION, "Remove Production Order", new RemoveProductionOrderAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildNotificationsMenu() {

        final Menu menu = new Menu("Notifications >");

        menu.addItem(NOTIFICATION_LIST_OPTION, "List all Notifications", new ListAllNotificationsAction());
        menu.addItem(NOTIFICATION_TREATED_LIST_OPTION, "List all Treated Notifications", new ConsultNotificationsWithTreatmentAction());
        menu.addItem(NOTIFICATION_TBD_LIST_OPTION, "List all Notifications Not Yet Treated", new ConsultNotificationsWithoutTreatmentAction());
        menu.addItem(NOTIFICATION_SET_AS_TREATED_OPTION, "Set Notification as Treated", new ArchiveNotificationAction());
        menu.addItem(NOTIFICATION_REMOVE_OPTION, "Remove Notification", new RemoveNotificationsAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildMessageMenu() {

        final Menu menu = new Menu("Messages >");

        menu.addItem(MESSAGE_LIST_OPTION, "List all Messages", new ListMessageAction());
        menu.addItem(MESSAGE_REMOVE_OPTION, "Remove Message", new RemoveMessageAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildXMLMenu() {

        final Menu menu = new Menu("XML >");

        menu.addItem(1, "Export Database to XML", new ExportToXMLAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

}
