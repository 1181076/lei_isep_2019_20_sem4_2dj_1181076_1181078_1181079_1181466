package lapr4.app.backoffice.console.presentation.authz;


import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import lapr4.app.backoffice.console.presentation.authz.ListMachineUI;
import lapr4.app.backoffice.console.presentation.authz.ListProductionLineUI;
import lapr4.factorymachines.application.RemoveMachineController;
import lapr4.factorymachines.application.RemoveProductionLineController;

/**
 *
 * @author mxlsa
 */
public class RemoveProductionLineUI extends AbstractUI {

    private final RemoveProductionLineController theController = new RemoveProductionLineController();    
    private final ListProductionLineUI theUI = new ListProductionLineUI();
    
    @Override
    protected boolean doShow() {
        
        theUI.elements();
        
        final long id = Console.readInteger("ID da Linha de Produção");
        
        try{
             this.theController.removeMachine(id);
             
             } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Production Line that doesn't exist.");
        }

        return false;
    }
   
    @Override
    public String headline() {
        return "Remove Production Line";
    }
    
}
