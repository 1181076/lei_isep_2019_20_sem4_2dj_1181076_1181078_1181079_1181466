/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.domain.application.ListMessagesController;

/**
 *
 * @author migue
 */
public class ListMessageUI extends AbstractListUI<Message> {

    private final ListMessagesController theController = new ListMessagesController();

    @Override
    protected Iterable<Message> elements() {
        return this.theController.listAllMessages();
    }

    @Override
    protected Visitor<Message> elementPrinter() {
        return new MessagePrinter();
    }

    @Override
    protected String elementName() {
        return "Message";
    }

    @Override
    protected String listHeader() {
        return "MESSAGES";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    public String headline() {
        return "List Messages";
    }
    
}
