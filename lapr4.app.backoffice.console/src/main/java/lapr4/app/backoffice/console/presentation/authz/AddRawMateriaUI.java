/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import lapr4.productmanagement.application.AddRawMateriaController;
import lapr4.productmanagement.domain.InternalRawMateriaCode;
import lapr4.productmanagement.domain.RawMateriaCategory;
import lapr4.productmanagement.domain.RawMateriaDescription;
import lapr4.productmanagement.domain.TechnicalSheet;

/**
 *
 * @author Leticia
 */
public class AddRawMateriaUI extends AbstractUI {

    private final AddRawMateriaController theController = new AddRawMateriaController();

    @Override
    protected boolean doShow() {
        final Iterable<RawMateriaCategory> rawMateriaCategories = this.theController.RawMateriaCategories();

        final SelectWidget<RawMateriaCategory> selector = new SelectWidget<>("Raw Materia Category:", rawMateriaCategories,
                new RawMateriaCategoryPrinter());
        selector.show();
        final RawMateriaCategory rawMateriaCategory = selector.selectedElement();

        final String desc = Console.readLine("Description");
        final String code = Console.readLine("Code");
        final String technicalSheet = Console.readLine("Technical Sheet");

        try {
            this.theController.addRawMateria(new RawMateriaDescription(desc), rawMateriaCategory, new InternalRawMateriaCode(code), new TechnicalSheet(technicalSheet));
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a raw materia which already exists in the database.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Add Raw Materia";
    }

}
