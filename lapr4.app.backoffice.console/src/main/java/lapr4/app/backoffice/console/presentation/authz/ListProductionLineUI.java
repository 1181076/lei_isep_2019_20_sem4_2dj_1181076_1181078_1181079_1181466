/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.factorymachines.application.ListMachineController;
import lapr4.factorymachines.application.ListProductionLineController;
import lapr4.factorymachines.domain.ProductionLine;

/**
 *
 * @author mxlsa
 */
public class ListProductionLineUI extends AbstractListUI<ProductionLine> {

    private final ListProductionLineController theController = new ListProductionLineController();

    @Override
    protected Iterable<ProductionLine> elements() {
        return this.theController.allProductionLines();
    }

    @Override
    protected Visitor<ProductionLine> elementPrinter() {
        return new ProductionLinePrinter();
    }

    @Override
    protected String elementName() {
        return "Production Line";
    }

    @Override
    protected String listHeader() {
        return "PRODUCTION LINES";
    }

    @Override
    protected String emptyMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String headline() {
        return "Production Lines";
    }

}
