/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.factorymachines.application.ListProductionLineController;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.domain.ProductLot;
import lapr4.productmanagement.domain.repositories.ProductLotRepository;

/**
 *
 * @author migue
 */
public class ListProductLotsUI extends AbstractListUI<ProductLot> {

    private final ProductLotRepository repo = PersistenceContext.repositories().productLots();

    @Override
    protected Iterable<ProductLot> elements() {
        return this.repo.findAll();
    }

    @Override
    protected Visitor<ProductLot> elementPrinter() {
        return new ProductLotPrinter();
    }

    @Override
    protected String elementName() {
        return "Product Lot";
    }

    @Override
    protected String listHeader() {
        return "PRODUCT LOTS";
    }

    @Override
    protected String emptyMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String headline() {
        return "Product Lots";
    }
}
