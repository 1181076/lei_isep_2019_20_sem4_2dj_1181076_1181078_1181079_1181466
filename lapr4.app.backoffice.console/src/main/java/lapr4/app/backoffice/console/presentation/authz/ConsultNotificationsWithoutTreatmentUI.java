/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.util.Console;
import eapli.framework.visitor.Visitor;
import lapr4.productionmanagement.application.ConsultNotificationsWithoutTreatmentController;
import lapr4.productionmanagement.domain.ErrorType;
import lapr4.productionmanagement.domain.Notification;

/**
 *
 * @author vrmpe
 */
public class ConsultNotificationsWithoutTreatmentUI extends AbstractListUI<Notification> {

    private final ConsultNotificationsWithoutTreatmentController theController = new ConsultNotificationsWithoutTreatmentController();

    @Override
    protected Iterable<Notification> elements() {
        String ar = Console.readLine("List Non-Archived Notifications:"
                + "1 - regarding non-existing products in messages?\n"
                + "2 - regarding non-existing deposits in messages?\n"
                + "3 - regarding non-existing product lots in messages?\n"
                + "4 - regarding shortage of products?\n"
                + "5 - list all\n");

        switch (ar) {
            case "1":
                return this.theController.consultNotificationsWithoutTreatment(ErrorType.NON_EXISTENT_PRODUCT);
            case "2":
                return this.theController.consultNotificationsWithoutTreatment(ErrorType.NON_EXISTENT_DEPOSIT);
            case "3":
                return this.theController.consultNotificationsWithoutTreatment(ErrorType.NON_EXISTENT_LOT);
            case "4":
                return this.theController.consultNotificationsWithoutTreatment(ErrorType.NOT_ENOUGH_PRODUCT);
            case "5":
                return this.theController.consultNotificationsWithoutTreatment();

        }
        return this.theController.consultNotificationsWithoutTreatment();   
    }

    @Override
    protected Visitor<Notification> elementPrinter() {
        return new NotificationPrinter();
            }

    @Override
    protected String elementName() {
        return "Notification Without Treatment";
    }

    @Override
    protected String listHeader() {
        return "NOTIFICATIONS WITHOUT TREATMENT";    }

    @Override
    protected String emptyMessage() {
        return "A lista está vazia";
    }

    @Override
    public String headline() {
        return "Notification Without Treatment";
    }
  

    
}
