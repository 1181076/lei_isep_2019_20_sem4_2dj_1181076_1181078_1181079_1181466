/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import lapr4.productmanagement.application.RemoveDepositController;

/**
 *
 * @author Leticia
 */
public class RemoveDepositUI extends AbstractUI {

    private final RemoveDepositController theController = new RemoveDepositController();    
    private final ListDepositUI theUI = new ListDepositUI();
    
    @Override
    protected boolean doShow() {
        
        theUI.elements();
        
        final String id = Console.readLine("ID do Depósito");
        
        try{
             this.theController.removeDeposit(id);
             
             } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Deposit that doesn't exist.");
        }

        return false;
    }
   
    @Override
    public String headline() {
        return "Remove Deposit";
    }
    
}
