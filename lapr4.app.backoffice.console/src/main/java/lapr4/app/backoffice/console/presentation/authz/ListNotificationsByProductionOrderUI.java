/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.visitor.Visitor;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productionmanagement.application.ConsultNotificationsWithTreatmentController;
import lapr4.productionmanagement.application.ListAllNotificationsController;
import lapr4.productionmanagement.application.ListNotificationsByProductionOrderController;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;

/**
 *
 * @author vrmpe
 */
public class ListNotificationsByProductionOrderUI extends AbstractListUI<Notification>{

    private final ProductionOrderRepository theRepo = PersistenceContext.repositories().productionOrders();
    private final ListNotificationsByProductionOrderController theController = new ListNotificationsByProductionOrderController();

    @Override
    protected Iterable<Notification> elements() {
        
       final Iterable<ProductionOrder> proOrd = this.theController.getProductionOrderList();
       final SelectWidget<ProductionOrder> selector = new SelectWidget<>("Production Order:", proOrd, new ProductionOrderPrinter());
       selector.show();
       final ProductionOrder proOrd2 = selector.selectedElement();
       
       return proOrd2.getNotifications();    }

    @Override
    protected Visitor<Notification> elementPrinter() {
        return new NotificationPrinter();
    }

    @Override
    protected String elementName() {
        return "Notification";
    }

    @Override
    protected String listHeader() {
        return "NOTIFICATIONS BY PRODUCTION ORDER";    }

    @Override
    protected String emptyMessage() {
        return "A lista está vazia";
    }

    @Override
    public String headline() {
        return "Notification";
    }
    
}
