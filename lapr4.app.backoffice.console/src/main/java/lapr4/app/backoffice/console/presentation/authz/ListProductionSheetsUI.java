/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.factorymachines.application.ListProductionLineController;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.productmanagement.application.ListProductionSheetsController;
import lapr4.productmanagement.domain.ProductionSheet;

/**
 *
 * @author migue
 */
public class ListProductionSheetsUI extends AbstractListUI<ProductionSheet> {

    private final ListProductionSheetsController theController = new ListProductionSheetsController();

    @Override
    protected Iterable<ProductionSheet> elements() {
        return this.theController.allSheets();
    }

    @Override
    protected Visitor<ProductionSheet> elementPrinter() {
        return new ProductionSheetPrinter();
    }

    @Override
    protected String elementName() {
        return "Production Sheet";
    }

    @Override
    protected String listHeader() {
        return "PRODUCTION SHEETS";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    public String headline() {
        return "Production Sheets";
    }

}
