/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import lapr4.ExportToXMLController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author migue
 */
public class ExportToXMLUI extends AbstractUI {

    private final ExportToXMLController theController = new ExportToXMLController();

    @Override
    protected boolean doShow() {
        String[] list = new String[100];

        Date s, e;

        String filterConf = Console.readLine("Apply Temporal Filter? (Y/N)");
        if (filterConf.equalsIgnoreCase("y")) {
            String start = Console.readLine("Start Date: (hh:mm:ss;dd:mm:yyyy)");
            String end = Console.readLine("End Date: (hh:mm:ss;dd/mm/yyyy)");
            Calendar startDate = Calendar.getInstance();
            Calendar endDate = Calendar.getInstance();
            startDate.set(Calendar.HOUR_OF_DAY, Integer.parseInt(start.split(";")[0].split(":")[0]));
            startDate.set(Calendar.MINUTE, Integer.parseInt(start.split(";")[0].split(":")[1]));
            startDate.set(Calendar.SECOND, Integer.parseInt(start.split(";")[0].split(":")[2]));
            startDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(start.split(";")[1].split("/")[0]));
            startDate.set(Calendar.MONTH, Integer.parseInt(start.split(";")[1].split("/")[1]));
            startDate.set(Calendar.YEAR, Integer.parseInt(start.split(";")[1].split("/")[2]));
            endDate.set(Calendar.HOUR_OF_DAY, Integer.parseInt(start.split(";")[0].split(":")[0]));
            endDate.set(Calendar.MINUTE, Integer.parseInt(start.split(";")[0].split(":")[1]));
            endDate.set(Calendar.SECOND, Integer.parseInt(start.split(";")[0].split(":")[2]));
            endDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(start.split(";")[1].split("/")[0]));
            endDate.set(Calendar.MONTH, Integer.parseInt(start.split(";")[1].split("/")[1]));
            endDate.set(Calendar.YEAR, Integer.parseInt(start.split(";")[1].split("/")[2]));
            s = startDate.getTime();
            e = endDate.getTime();
        } else {
            s = null;
            e = null;
        }

        String name = Console.readLine("Export Products? (Y/N):");
        list[0] = name;
        name = Console.readLine("Export RawMaterials? (Y/N):");
        list[1] = name;
        name = Console.readLine("Export RawMaterialCategories? (Y/N):");
        list[2] = name;
        name = Console.readLine("Export Deposits? (Y/N):");
        list[3] = name;
        name = Console.readLine("Export ProductionSheets? (Y/N):");
        list[4] = name;
        name = Console.readLine("Export Notifications? (Y/N):");
        list[5] = name;
        name = Console.readLine("Export ProductionOrders? (Y/N):");
        list[6] = name;
        name = Console.readLine("Export Product Lots? (Y/N):");
        list[7] = name;
        name = Console.readLine("Export ProductionLines? (Y/N):");
        list[8] = name;
        name = Console.readLine("Export Machines? (Y/N):");
        list[9] = name;
        name = Console.readLine("Export Messages? (Y/N):");
        list[10] = name;

        final String name2 = Console.readLine("Name of File to be exported to:");

        this.theController.exportTo(name2, list, s, e);

        return false;
    }

    @Override
    public String headline() {
        return "Export to XML";
    }
}
