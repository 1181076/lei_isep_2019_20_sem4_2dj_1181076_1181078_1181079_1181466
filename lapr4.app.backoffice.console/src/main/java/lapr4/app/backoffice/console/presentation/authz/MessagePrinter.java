/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.visitor.Visitor;
import lapr4.messagemanagement.domain.Message;

/**
 *
 * @author migue
 */
public class MessagePrinter implements Visitor<Message> {

    @Override
    public void visit(final Message visitee) {
        System.out.println("\n\n\n---------------------------------------");
        System.out.println("MessageID:          "+ visitee.getId());
        System.out.println("Description:        "+ visitee.getDesc().getDesc());
        System.out.println("Type:               "+ visitee.getType());
        System.out.println("Processed:          "+ visitee.isProcessed());
        System.out.println("---------------------------------------");
    }
    
}
