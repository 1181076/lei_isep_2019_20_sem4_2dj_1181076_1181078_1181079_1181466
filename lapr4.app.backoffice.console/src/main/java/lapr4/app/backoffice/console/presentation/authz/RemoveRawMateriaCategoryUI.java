/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import lapr4.productmanagement.application.RemoveRawMateriaCategoryController;

/**
 *
 * @author Leticia
 */
public class RemoveRawMateriaCategoryUI extends AbstractUI {

    private final RemoveRawMateriaCategoryController theController = new RemoveRawMateriaCategoryController();
    private final ListRawMateriaCategoryUI lu = new ListRawMateriaCategoryUI();

    @Override
    protected boolean doShow() {

        lu.show();

        final long id = Console.readLong("ID");
        try {
            this.theController.removeRawMateriaCategory(id);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to remove a raw materia category which not exists in the database.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Remove Raw Materia Category";
    }
}
