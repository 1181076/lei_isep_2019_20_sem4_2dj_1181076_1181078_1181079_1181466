/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;
import java.util.HashMap;
import java.util.Map;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.productmanagement.application.ConsultProductsWithoutProductionSheetController;
import lapr4.productmanagement.application.SpecifyProductionSheetForAProductController;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductionSheet;
import lapr4.productmanagement.domain.repositories.ProductRepository;

/**
 *
 * @author vrmpe
 */
public class SpecifyProductionSheetForAProductUI extends AbstractUI {

    private final SpecifyProductionSheetForAProductController theController = new SpecifyProductionSheetForAProductController();
    private final ConsultProductsWithoutProductionSheetUI theUI = new ConsultProductsWithoutProductionSheetUI();
    private final ConsultProductsWithoutProductionSheetController pCont = new ConsultProductsWithoutProductionSheetController();
    private final ProductRepository pdRepo = PersistenceContext.repositories().products();

    @Override
    protected boolean doShow() {
        
        String id = Console.readLine("Nome da Ficha de Produção");
        
        Map<Product, Integer> maSet = new HashMap();

        System.out.println("Material da Ficha de Produção");

        final Iterable<Product> products = this.pCont.productsWithoutProductionSheet();

        final SelectWidget<Product> selectorr = new SelectWidget<>("Product:", products,
                new ProductPrinter());
        selectorr.show();
        final Product prod = selectorr.selectedElement();

        String a = "Y";

        while (a.equalsIgnoreCase("Y")) {
            System.out.println("Material da Ficha de Produção");

            final Iterable<Product> materials = this.pdRepo.findAll();

            final SelectWidget<Product> selector = new SelectWidget<>("Material:", materials,
                    new ProductPrinter());
            selector.show();
            final Product material = selector.selectedElement();

            final int sheet2 = Console.readInteger("Quantidade da Ficha de Produção");

            if (maSet.containsKey(material)) {
                maSet.replace(material, sheet2 + maSet.get(material));
            } else {
                maSet.put(material, sheet2);
            }
            a = Console.readLine("Mais Materiais? (Y/N)");
        }

        ProductionSheet productionSheet = new ProductionSheet(id, maSet);
        prod.setProductSheet(productionSheet);

        try {
            this.theController.specifyProductionSheetForAProduct(productionSheet);
            this.pdRepo.save(prod);

        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Product that doesn't exist in the database.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Specify Production Sheet For A Product";
    }

}
