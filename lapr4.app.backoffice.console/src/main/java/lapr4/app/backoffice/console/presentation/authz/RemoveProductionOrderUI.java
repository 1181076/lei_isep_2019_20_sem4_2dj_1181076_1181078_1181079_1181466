/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import lapr4.productionmanagement.application.RemoveProductionOrderController;

/**
 *
 * @author Leticia
 */
public class RemoveProductionOrderUI extends AbstractUI {

    private final RemoveProductionOrderController theController = new RemoveProductionOrderController();    
    private final ListProductsUI theUI = new ListProductsUI();
    
    @Override
    protected boolean doShow() {
        
        theUI.elements();
        
        final long id = Console.readInteger("Production Order's ID ");
        
        try{
             this.theController.removeProductionOrder(id);
             
             } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Product that doesn't exist.");
        }

        return false;
    }
   
    @Override
    public String headline() {
        return "Remove Production Order";
    }
    
}
