/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import lapr4.factorymachines.application.RemoveMachineController;
import lapr4.productmanagement.application.RemoveProductController;

/**
 *
 * @author migue
 */
public class RemoveProductsUI extends AbstractUI {

    private final RemoveProductController theController = new RemoveProductController();    
    private final ListProductsUI theUI = new ListProductsUI();
    
    @Override
    protected boolean doShow() {
        
        theUI.elements();
        
        final long id = Console.readInteger("ID do Produto");
        
        try{
             this.theController.removeProduct(id);
             
             } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("You tried to enter a Product that doesn't exist.");
        }

        return false;
    }
   
    @Override
    public String headline() {
        return "Remove Product";
    }
    
}
