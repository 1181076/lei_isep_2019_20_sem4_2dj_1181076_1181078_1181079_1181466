/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.util.Console;
import eapli.framework.visitor.Visitor;
import lapr4.productionmanagement.application.ConsultOrderStateController;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;

/**
 *
 * @author mxlsa
 */
public class ConsultOrderStateUI extends AbstractListUI<ProductionOrder> {

    private final ConsultOrderStateController theController = new ConsultOrderStateController();
    
    @Override
    protected Iterable<ProductionOrder> elements() {
        String state = Console.readLine("Digite uma destas opções ACCEPTED, PROCESSING, DELIVERING, FINISHED ");
        switch(state) {
            case "ACCEPTED":
                 return this.theController.consultOrdersByState(OrderStatus.ACCEPTED);
            case "PROCESSING":
                return this.theController.consultOrdersByState(OrderStatus.PROCESSING);
            case "DELIVERING":
                return this.theController.consultOrdersByState(OrderStatus.DELIVERING);
            case "FINISHED":
                return this.theController.consultOrdersByState(OrderStatus.FINISHED);
            default:
                state = Console.readLine("Digite uma destas opções ACCEPTED, PROCESSING, DELIVERING, FINISHED ");
        }   
        return null;
    }

    @Override
    protected Visitor<ProductionOrder> elementPrinter() {
        return new ProductionOrderPrinter();
    }

    @Override
    protected String elementName() {
        return "Ordem de Produção";
    }

    @Override
    protected String listHeader() {
        return "Ordens de Produção";
    }

    @Override
    protected String emptyMessage() {
         return "A lista está vazia";
    }

    @Override
    public String headline() {
        return "Consultar Ordens por Estado";
    }
    
}
