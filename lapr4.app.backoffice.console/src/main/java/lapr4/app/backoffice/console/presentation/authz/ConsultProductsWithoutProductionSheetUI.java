/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.factorymachines.domain.Machine;
import lapr4.productmanagement.application.ConsultProductsWithoutProductionSheetController;
import lapr4.productmanagement.domain.Product;

/**
 *
 * @author vrmpe
 */
public class ConsultProductsWithoutProductionSheetUI extends AbstractListUI<Product>{
    
    private final ConsultProductsWithoutProductionSheetController theController = new ConsultProductsWithoutProductionSheetController();

    @Override
    protected Iterable<Product> elements() {
        return this.theController.productsWithoutProductionSheet();
    }

    @Override
    protected Visitor<Product> elementPrinter() {
        return new ProductPrinter();
    }

    @Override
    protected String elementName() {
        return "Product Without Production Sheet";
    }

    @Override
    protected String listHeader() {
        return "PRODUCTS WITHOUT PRODUCTION SHEET";
    }

    @Override
    protected String emptyMessage() {
        return "A lista está vazia";
    }

    @Override
    public String headline() {
        return "Product Without Production Sheet";
    }

    
}
