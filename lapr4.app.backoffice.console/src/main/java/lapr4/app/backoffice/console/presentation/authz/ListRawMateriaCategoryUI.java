/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.productmanagement.application.ListRawMateriaCategoryController;
import lapr4.productmanagement.domain.RawMateriaCategory;

/**
 *
 * @author Leticia
 */
public class ListRawMateriaCategoryUI extends AbstractListUI<RawMateriaCategory> {

    private final ListRawMateriaCategoryController theController = new ListRawMateriaCategoryController();

    @Override
    protected Iterable<RawMateriaCategory> elements() {
        return this.theController.listRawMateriaCategories();
    }

    @Override
    protected Visitor<RawMateriaCategory> elementPrinter() {
        return new RawMateriaCategoryPrinter();
    }

    @Override
    protected String elementName() {
        return "Raw Materia Category";
    }

    @Override
    protected String listHeader() {
        return "RAW MATERIA CATEGORIES";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    public String headline() {
        return "List Raw Materia Categories";
    }
    
}
