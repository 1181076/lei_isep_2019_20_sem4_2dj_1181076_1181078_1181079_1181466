/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.backoffice.console.presentation.authz;

import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;
import lapr4.productmanagement.application.ListRawMateriaController;
import lapr4.productmanagement.domain.RawMateria;

/**
 *
 * @author Leticia
 */
public class ListRawMateriaUI extends AbstractListUI<RawMateria> {

    private final ListRawMateriaController theController = new ListRawMateriaController();

    @Override
    protected Iterable<RawMateria> elements() {
        return this.theController.listRawMaterias();
    }

    @Override
    protected Visitor<RawMateria> elementPrinter() {
        return new RawMateriaPrinter();
    }

    @Override
    protected String elementName() {
        return "Raw Materia";
    }

    @Override
    protected String listHeader() {
        return "RAW MATERIAS";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    public String headline() {
        return "List raw materias";
    }
    
}
