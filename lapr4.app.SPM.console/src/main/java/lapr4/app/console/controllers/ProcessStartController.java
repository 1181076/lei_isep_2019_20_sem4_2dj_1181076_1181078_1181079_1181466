package lapr4.app.console.controllers;

import static java.lang.Thread.sleep;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Timer;
import lapr4.app.spm.console.runnables.LineRunner;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;

public class ProcessStartController {

    private final ProductionLineRepository pdRepository = PersistenceContext.repositories().productionLines();

    int mode;
    Calendar dateStart;
    Calendar dateEnd;
    int interval;
    int duration;

    public ProcessStartController(int mode, Calendar dateStart, Calendar dateEnd, int interval, int duration) {
        this.mode = mode;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.interval = interval;
        this.duration = duration;
    }

    public void setUp() throws InterruptedException {


        if (mode == 1) {
            
            
        int num = 0;
        for (ProductionLine pddd : pdRepository.findAll()) {
            num++;
        }

        Iterator<ProductionLine> pd = pdRepository.findAll().iterator();

        Thread[] tThreads = new Thread[num];
        for (int j = 0; j < num; j++) {
            LineRunner ot = new LineRunner(new MessageProcesserController(pd.next()));
            tThreads[j] = new Thread(ot);
        }
            setUpOneBlock(tThreads, num);
        }

        if (mode == 2) {
            
            
        int num = 0;
        for (ProductionLine pddd : pdRepository.findToBeProcessed()) {
            num++;
        }

        Iterator<ProductionLine> pd = pdRepository.findToBeProcessed().iterator();

        Thread[] tThreads = new Thread[num];
        for (int j = 0; j < num; j++) {
            LineRunner ot = new LineRunner(new MessageProcesserController(pd.next()));
            tThreads[j] = new Thread(ot);
        }
        
            System.out.println("Waiting for Start Date");
            sleep(dateStart.getTimeInMillis() - System.currentTimeMillis());
            System.out.println("It's Time");
            setUpUnlimitedBlocks(tThreads, num);

        }

    }

    public void setUpOneBlock(Thread[] t, int num) throws InterruptedException {
        Calendar cal = Calendar.getInstance();
        System.out.println("Waiting for Start Date");
        System.out.println(dateStart.getTimeInMillis() - System.currentTimeMillis());
        sleep(dateStart.getTimeInMillis() - System.currentTimeMillis());
        System.out.println("It's Time");

        for (int j = 0; j < num; j++) {
            t[j].start();
        }

        sleep(dateEnd.getTimeInMillis() - dateStart.getTimeInMillis());

        System.out.println("Interrupting");
        for (Thread tOrderTaker : t) {
            tOrderTaker.interrupt();
            tOrderTaker.join();
        }
    }

    private void setUpUnlimitedBlocks(Thread[] t, int num) throws InterruptedException {

        for (int j = 0; j < num; j++) {
            t[j].start();
        }

        sleep(duration * 100000);

        for (Thread tOrderTaker : t) {
            tOrderTaker.interrupt();
        }

        System.out.println("Waiting...");

        sleep(interval * 100000);

        setUpAgain(num);
    }

    public void setUpAgain(int num) throws InterruptedException {

        Iterator<ProductionLine> pd = pdRepository.findAll().iterator();

        Thread[] tThreads = new Thread[num];
        for (int j = 0; j < num; j++) {
            LineRunner ot = new LineRunner(new MessageProcesserController(pd.next()));
            tThreads[j] = new Thread(ot);
        }

        System.out.println("Resuming...");
        setUpUnlimitedBlocks(tThreads, num);

    }
}
