/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.console.controllers;

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lapr4.factorymachines.domain.ProductionLine;
import lapr4.factorymachines.repositories.ProductionLineRepository;
import lapr4.infrastructure.persistence.PersistenceContext;
import lapr4.messagemanagement.domain.Message;
import lapr4.messagemanagement.repositories.MessageRepository;
import lapr4.productionmanagement.domain.ErrorType;
import lapr4.productionmanagement.domain.Notification;
import lapr4.productionmanagement.domain.NotificationDescription;
import lapr4.productionmanagement.domain.NotificationType;
import lapr4.productionmanagement.domain.OrderStatus;
import lapr4.productionmanagement.domain.ProductionOrder;
import lapr4.productionmanagement.repositories.ProductionOrderRepository;
import lapr4.productmanagement.domain.Deposit;
import lapr4.productmanagement.domain.Product;
import lapr4.productmanagement.domain.ProductLot;
import lapr4.productmanagement.domain.repositories.DepositRepository;
import lapr4.productmanagement.domain.repositories.ProductLotRepository;
import lapr4.productmanagement.domain.repositories.ProductRepository;

/**
 *
 * @author migue
 */
public class MessageProcesserController {

    private final DepositRepository depositRepository = PersistenceContext.repositories().deposits();
    private final ProductionLineRepository pdRepository = PersistenceContext.repositories().productionLines();
    private final ProductRepository productRepository = PersistenceContext.repositories().products();
    private final MessageRepository mRepository = PersistenceContext.repositories().messages();
    private final ProductLotRepository repo = PersistenceContext.repositories().productLots();
    private final ProductionOrderRepository productionOrderRepository = PersistenceContext.repositories().productionOrders();

    ProductionLine pd;
    Set<Message> lst;
    List<Message> unprocessed = new ArrayList();

    public MessageProcesserController(ProductionLine pd) {
        this.pd = pd;
        this.lst = pd.getMessages();
    }

    public synchronized void process() {
        
        pd.setLastProcessed(Calendar.getInstance().getTime());

        Comparator<Message> c = new Comparator() {
            @Override
            public int compare(Object arg0, Object arg1) {
                Message m1 = (Message) arg0;
                Message m2 = (Message) arg1;
                return m1.getData().compareTo(m2.getData());
            }
        };

        System.out.println("Processing Started");
        checkUnprocessed();

        unprocessed.sort(c);

        try {
            for (Message ms : unprocessed) {
                System.out.println("Processing Message:" + ms.getDesc());
                process(ms);
                System.out.println("Sleeping for 5 seconds");
                sleep(5000);
            }
        } catch (InterruptedException e) {
        }
    }

    private void checkUnprocessed() {
        for (Message m : lst) {
            if (!m.isProcessed()) {
                unprocessed.add(m);
            }
        }
    }

    private void process(Message ms) {
        String[] desc = ms.getDesc().getDesc().split(";");
        String type = desc[1];

        switch (type) {
            case "C0":
                processC0(ms, ms.getData(), desc[3], desc[4], desc[5], desc[6]);
                break;
            case "C9":
                processC9(ms, ms.getData(), desc[3], desc[4], desc[5], desc[6]);
                break;
            case "P1":
                processP1(ms, ms.getData(), desc[3], desc[4], desc[5], desc[6]);
                break;
            case "P2":
                processP2(ms, ms.getData(), desc[3], desc[4], desc[5], desc[6]);
                break;
            case "S0":
                processS0(ms, ms.getData(), desc[3]);
                break;
            case "S1":
                processS1(ms, ms.getData(), desc[3], desc[4]);
                break;
            case "S8":
                processS8(ms, ms.getData(), desc[4]);
                break;
            case "S9":
                processS9(ms, ms.getData(), desc[3]);
                break;
        }
//        C0 -> Máquina;TipoMsg;DataHora;Produto;Quantidade;Depósito
//        C9 -> Máquina;TipoMsg;DataHora;Produto;Quantidade;Depósito
//        P1 -> Máquina;TipoMsg;DataHora;Produto;Quantidade;Lote
//        P2 -> Máquina;TipoMsg;DataHora;Produto;Quantidade;Depósito
//
//        S0 -> Máquina;TipoMsg;DataHora;OrdemProducao
//        S1 -> Máquina;TipoMsg;DataHora;Erro
//        S8 -> Máquina;TipoMsg;DataHora
//        S9 -> Máquina;TipoMsg;DataHora;OrdemProducao
    }

    private void processC0(Message ms, Date data, String string1, String string2, String string3, String string4) {
        ProductionOrder po = productionOrderRepository.findByID(Long.parseLong(string4)).get();
        Optional<Deposit> od = depositRepository.findByID(string3);
        Optional<Product> op = productRepository.findByCode(string1);
        if (od.isPresent()) {
            Deposit d = od.get();
            if (op.isPresent()) {
                Product p = op.get();
                po.getMov().setUsedMateria(po.getMov().getUsedMateria() + Integer.parseInt(string2));
                if (d.getProducts().containsKey(p)) {
                    d.getProducts().replace(p, d.getProducts().get(p) - Integer.parseInt(string2));
                } else {
                    Notification n = new Notification(new NotificationDescription(ms.getDesc().getDesc()), NotificationType.TBD);
                    n.setErrorType(ErrorType.NOT_ENOUGH_PRODUCT);
                    po.getNotifications().add(n);
                }
            } else {
                Notification n = new Notification(new NotificationDescription(ms.getDesc().getDesc()), NotificationType.TBD);
                n.setErrorType(ErrorType.NON_EXISTENT_PRODUCT);
                po.getNotifications().add(n);
            }

            depositRepository.save(d);

        } else {
            Notification n = new Notification(new NotificationDescription(ms.getDesc().getDesc()), NotificationType.TBD);
            n.setErrorType(ErrorType.NON_EXISTENT_DEPOSIT);
            po.getNotifications().add(n);
        }
        productionOrderRepository.save(po);
        ms.setProcessed(true);
        mRepository.save(ms);
    }

    private void processC9(Message ms, Date data, String string1, String string2, String string3, String string4) {
        ProductionOrder po = productionOrderRepository.findByID(Long.parseLong(string4)).get();
        Optional<Deposit> od = depositRepository.findByID(string3);
        Optional<Product> op = productRepository.findByCode(string1);
        if (od.isPresent()) {
            Deposit d = od.get();
            if (op.isPresent()) {
                Product p = op.get();
                po.getMov().setNewProduct(po.getMov().getNewProduct() + Integer.parseInt(string2));
                if (d.getProducts().containsKey(p)) {
                    d.getProducts().replace(p, d.getProducts().get(p) + Integer.parseInt(string2));
                } else {
                    d.getProducts().put(p, Integer.parseInt(string2));
                }
            } else {
                Notification n = new Notification(new NotificationDescription(ms.getDesc().getDesc()), NotificationType.TBD);
                n.setErrorType(ErrorType.NON_EXISTENT_PRODUCT);
                po.getNotifications().add(n);
            }

            depositRepository.save(d);

        } else {
            Notification n = new Notification(new NotificationDescription(ms.getDesc().getDesc()), NotificationType.TBD);
            n.setErrorType(ErrorType.NON_EXISTENT_DEPOSIT);
            po.getNotifications().add(n);
        }
        productionOrderRepository.save(po);
        ms.setProcessed(true);
        mRepository.save(ms);
    }

    private void processP1(Message ms, Date data, String string1, String string2, String string3, String string4) {
        ProductionOrder po = productionOrderRepository.findByID(Long.parseLong(string4)).get();
        Optional<ProductLot> od = repo.findByCode(string3);
        Optional<Product> op = productRepository.findByCode(string1);
        if (od.isPresent()) {
            ProductLot d = od.get();
            if (op.isPresent()) {
                Product p = op.get();
                po.getMov().setNewProduct(po.getMov().getNewProduct() + Integer.parseInt(string2));
                if (d.getAmount().containsKey(p)) {
                    d.getAmount().replace(p, d.getAmount().get(p) + Integer.parseInt(string2));
                } else {
                    d.getAmount().put(p, Integer.parseInt(string2));
                }
            } else {
                Notification n = new Notification(new NotificationDescription(ms.getDesc().getDesc()), NotificationType.TBD);
                n.setErrorType(ErrorType.NON_EXISTENT_PRODUCT);
                po.getNotifications().add(n);
            }
            repo.save(d);
        } else {
            Notification n = new Notification(new NotificationDescription(ms.getDesc().getDesc()), NotificationType.TBD);
            n.setErrorType(ErrorType.NON_EXISTENT_LOT);
            po.getNotifications().add(n);
        }
        productionOrderRepository.save(po);
        ms.setProcessed(true);
        mRepository.save(ms);
    }

    private void processP2(Message ms, Date data, String string1, String string2, String string3, String string4) {
        ProductionOrder po = productionOrderRepository.findByID(Long.parseLong(string4)).get();
        Optional<Deposit> od = depositRepository.findByID(string3);
        Optional<Product> op = productRepository.findByCode(string1);
        if (od.isPresent()) {
            Deposit d = od.get();
            if (op.isPresent()) {
                Product p = op.get();
                po.getMov().setNewProduct(po.getMov().getNewProduct() + Integer.parseInt(string2));
                if (d.getProducts().containsKey(p)) {
                    d.getProducts().replace(p, d.getProducts().get(p) + Integer.parseInt(string2));
                } else {
                    d.getProducts().put(p, Integer.parseInt(string2));
                }
            } else {
                Notification n = new Notification(new NotificationDescription(ms.getDesc().getDesc()), NotificationType.TBD);
                n.setErrorType(ErrorType.NON_EXISTENT_PRODUCT);
                po.getNotifications().add(n);
            }

            depositRepository.save(d);

        } else {
            Notification n = new Notification(new NotificationDescription(ms.getDesc().getDesc()), NotificationType.TBD);
            n.setErrorType(ErrorType.NON_EXISTENT_DEPOSIT);
            po.getNotifications().add(n);
        }
        productionOrderRepository.save(po);
        ms.setProcessed(true);
        mRepository.save(ms);
    }

    private void processS0(Message ms, Date data, String string1) {
        ProductionOrder po = productionOrderRepository.findByID(Long.parseLong(string1)).get();
        po.setOrderStatus(OrderStatus.PROCESSING);
        po.setLine(pd);
        productionOrderRepository.save(po);
        pd.setAvailability(false);
        pdRepository.save(pd);
        ms.setProcessed(true);
        mRepository.save(ms);
    }

    private void processS1(Message ms, Date data, String string1, String string2) {
        ProductionOrder po = productionOrderRepository.findByID(Long.parseLong(string2)).get();
        po.setOrderStatus(OrderStatus.PROCESSING);
        productionOrderRepository.save(po);
        //notify error here
        ms.setProcessed(true);
        mRepository.save(ms);
    }

    private void processS8(Message ms, Date data, String string1) {
        ProductionOrder po = productionOrderRepository.findByID(Long.parseLong(string1)).get();
        po.setOrderStatus(OrderStatus.STOPPED);
        productionOrderRepository.save(po);
        ms.setProcessed(true);
        mRepository.save(ms);
    }

    private void processS9(Message ms, Date data, String string1) {
        ProductionOrder po = productionOrderRepository.findByID(Long.parseLong(string1)).get();
        po.setOrderStatus(OrderStatus.DELIVERING);
        po.setLine(null);
        productionOrderRepository.save(po);
        ms.setProcessed(true);
        mRepository.save(ms);
    }

}
