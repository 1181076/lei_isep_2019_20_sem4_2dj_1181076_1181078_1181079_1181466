/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.spm.console.presentation.authz;

import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr4.app.console.controllers.ProcessStartController;

/**
 *
 * @author migue
 */
public class MessageProcessingUI extends AbstractUI {

    int mode;
    Calendar dateStart;
    Calendar dateEnd;
    int interval=0;
    int duration=0;
    String read;

    @Override
    protected boolean doShow() {
        while (mode > 2 || mode < 1) {
            mode = Console.readInteger("What Processing mode would you prefer?\n"
                    + "1 - Specify a specific time interval (processes one block)\n"
                    + "2 - Specify a start time, with processing occurring regularly until cancelled");

        }
        
        if(mode == 1){
            read = Console.readLine("What is the Start Time (format hh:mm)");
            String ar[] = read.split(":");
            System.out.println(ar[0]);
            System.out.println(ar[1]);
            dateStart = Calendar.getInstance();
            dateStart.set(Calendar.HOUR_OF_DAY, Integer.parseInt(ar[0]));
            dateStart.set(Calendar.MINUTE, Integer.parseInt(ar[1]));
            dateStart.set(Calendar.SECOND, 0);
            dateStart.set(Calendar.MILLISECOND, 0);
            System.out.println("Start Date set as " + dateStart.getTime().toString());
            
            read = Console.readLine("What is the End Time (format hh:mm)");
            ar = read.split(":");
            System.out.println(ar[0]);
            System.out.println(ar[1]);
            dateEnd = Calendar.getInstance();
            dateEnd.set(Calendar.HOUR_OF_DAY, Integer.parseInt(ar[0]));
            dateEnd.set(Calendar.MINUTE, Integer.parseInt(ar[1]));
            dateEnd.set(Calendar.SECOND, 0);
            dateEnd.set(Calendar.MILLISECOND, 0);
            System.out.println("Start Date set as " + dateEnd.getTime().toString());
        }else{
            read = Console.readLine("What is the Start Time (format hh:mm)");
            String ar[] = read.split(":");
            System.out.println(ar[0]);
            System.out.println(ar[1]);
            dateStart = Calendar.getInstance();
            dateStart.set(Calendar.HOUR_OF_DAY, Integer.parseInt(ar[0]));
            dateStart.set(Calendar.MINUTE, Integer.parseInt(ar[1]));
            dateStart.set(Calendar.SECOND, 0);
            dateStart.set(Calendar.MILLISECOND, 0);
            System.out.println("Start Date set as " + dateStart.getTime().toString());
            
            duration = Console.readInteger("What is the duration of the processing blocks in minutes?");
            interval = Console.readInteger("What is the interval between  processing blocks in minutes?");
            
        }
        
        String y = Console.readLine("Start Processing? (Y/N)");
        
        while(!y.equalsIgnoreCase("Y")){
            y = Console.readLine("Start Processing? (Y/N)");
        }
        
        ProcessStartController cont = new ProcessStartController(mode, dateStart, dateEnd, interval,  duration);
        
        try {
            cont.setUp();
        } catch (InterruptedException ex) {
            Logger.getLogger(MessageProcessingUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;

    }
    

    @Override
    public String headline() {
        return "Message Processment Setup";
    }

}
