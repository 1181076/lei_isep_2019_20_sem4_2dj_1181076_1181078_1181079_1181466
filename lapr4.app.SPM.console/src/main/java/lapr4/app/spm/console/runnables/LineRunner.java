/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr4.app.spm.console.runnables;

import lapr4.app.console.controllers.MessageProcesserController;

/**
 *
 * @author migue
 */
public class LineRunner implements Runnable {

    private final MessageProcesserController objec;

    public LineRunner(MessageProcesserController obj) {
        this.objec = obj;
    }

    public void run() {

        objec.process();
    }
}
