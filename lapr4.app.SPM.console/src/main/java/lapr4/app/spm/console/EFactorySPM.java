package lapr4.app.spm.console;

import eapli.framework.infrastructure.eventpubsub.EventDispatcher;
import lapr4.app.common.console.EFactoryBaseApplication;
import lapr4.app.spm.console.presentation.MainMenu;


@SuppressWarnings("squid:S106")
public class EFactorySPM extends EFactoryBaseApplication {

    public EFactorySPM() {
    }

    public static void main(String[] args) {
        new EFactorySPM().run(args);
    }

    @Override
    protected void doMain(String[] args) {
        final MainMenu menu = new MainMenu();
        menu.mainLoop();
    }

    @Override
    protected String appTitle() {
        return "eFactory Message Processing Service (SPM)";
    }

    @Override
    protected String appGoodbye() {
        return "eFactory Message Processing Service (SPM)";
    }

    @Override
    protected void doSetupEventHandlers(EventDispatcher dispatcher) {
        
    }
}