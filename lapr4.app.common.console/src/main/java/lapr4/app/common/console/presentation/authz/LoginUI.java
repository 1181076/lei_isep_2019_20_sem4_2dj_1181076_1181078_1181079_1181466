package lapr4.app.common.console.presentation.authz;

import eapli.framework.infrastructure.authz.application.AuthenticationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.infrastructure.authz.domain.model.Role;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

@SuppressWarnings("squid:S106")
public class LoginUI extends AbstractUI {

    private final AuthenticationService authenticationService = AuthzRegistry
            .authenticationService();

    private Role onlyWithThis;
    private static final int DEFAULT_MAX_ATTEMPTS = 3;
    private final int maxAttempts;

    public LoginUI() {
        maxAttempts = DEFAULT_MAX_ATTEMPTS;
    }

    public LoginUI(final Role onlyWithThis) {
        this.onlyWithThis = onlyWithThis;
        maxAttempts = DEFAULT_MAX_ATTEMPTS;
    }

    public LoginUI(final Role onlyWithThis, final int maxAttempts) {
        this.onlyWithThis = onlyWithThis;
        this.maxAttempts = maxAttempts;
    }

    public LoginUI(final int maxAttempts) {
        this.maxAttempts = maxAttempts;
    }

    @Override
    protected boolean doShow() {
        int attempt = 1;
        while (attempt <= maxAttempts) {
            final String userName = Console.readNonEmptyLine("Username:",
                    "Please provide a username");
            final String password = Console.readLine("Password:");

            if (authenticationService.authenticate(userName, password, onlyWithThis).isPresent()) {
                return true;
            }
            System.out.printf(
                    "Wrong username or password. You have %d attempts left.%n%n»»»»»»»»»%n",
                    maxAttempts - attempt);
            attempt++;
        }
        System.out.println(
                "Sorry, we are unable to authenticate you. Please contact your system admnistrator.");
        return false;
    }

    @Override
    public String headline() {
        return "Login";
    }
}
